FROM eclipse-temurin:17-jre-alpine
RUN addgroup -S schedyt && adduser -S kageyama -G schedyt
USER kageyama
RUN mkdir /home/kageyama/logs
RUN chown --recursive kageyama:schedyt /home/kageyama/logs
COPY target/schedyt-1.0.1.jar .
ENTRYPOINT ["sh","-c","java ${JAVA_OPTS} -jar schedyt-1.0.1.jar"]