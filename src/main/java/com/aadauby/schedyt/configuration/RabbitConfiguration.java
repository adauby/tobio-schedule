package com.aadauby.schedyt.configuration;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("quick-start")
@Configuration
public class RabbitConfiguration {
    @Bean
    public DirectExchange direct() {
        return new DirectExchange("quick_exchange");
    }

    @Bean
    public Queue autoDeleteQueue() {
        return new AnonymousQueue();
    }


    @Bean
    public Binding binding(DirectExchange direct,
                           Queue autoDeleteQueue) {
        return BindingBuilder.bind(autoDeleteQueue)
                .to(direct)
                .with("quick_start");
    }




}
