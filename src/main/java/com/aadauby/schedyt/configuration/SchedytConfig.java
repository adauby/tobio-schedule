package com.aadauby.schedyt.configuration;

import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.application.repository.ApplicationData;
import com.aadauby.schedyt.schedule.controller.ScheduleRequest;
import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.EventScheduleMapper;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.constants.Values;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.exception.InvalidDataException;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import com.aadauby.schedyt.utils.generator.TobioGenerator;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import com.aadauby.schedyt.utils.page.PageBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_DATE_FORMAT;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_FORMATTER;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_WITHOUT_SECONDS_FORMATTER;

@Configuration
public class SchedytConfig {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private final Function<ScheduleRequest, EventSchedule> scheduleRequestMapper = req -> {
        var triggerDate =  DateUtilities.convertStringToDate(req.triggerDate(), DATE_TIME_FORMATTER, req.zoneId());
        var schedule = new EventSchedule(null, req.zoneId(),triggerDate
                , req.frequency(), req.duration(), JsonUtilities.fromMapToJsonString(req.event()));
        schedule.setEventName(req.eventName());
        schedule.setRoutingKey(req.routingKey());
        if(schedule.getTriggerDate() == null)
            schedule.setTriggerFirstAt(DateUtilities.convertStringToDate(req.triggerFirstAt()
                    , DATE_TIME_WITHOUT_SECONDS_FORMATTER,  req.zoneId()));
        if(req.zoneId()!=null && !req.zoneId().isBlank()) {
            var creationTime = ZonedDateTime.now(ZoneId.of(req.zoneId()));
            schedule.setCreatedAt(creationTime
                    .withZoneSameInstant(ZoneId.of("UTC"))
                    .toLocalDateTime());
        }
        return schedule;
    };

    @Bean("date-validator")
    public Consumer<String> dateFormatValidator() {
        return (date) -> {
            if (DateUtilities.convertStringToDate(date, DateUtilities.DATE_TIME_FORMATTER, Values.DEFAULT_ZONE_ID) == null)
                throw new InvalidDataException(Messages.INVALID_DATETIME_FORMAT, CODE_INVALID_DATE_FORMAT);
        };
    }

    @Bean("application-mapper")
    public Function<Application, ApplicationData> applicationMapper() {

        return app -> new ApplicationData(app.getId(), app.getName()
                    , app.getDescription(), app.getExchangeName()
                    ,app.getZoneId(), app.getFormattedCreatedAt());

    }

    @Bean("scheduleRequest-mapper")
    public Function<ScheduleRequest, EventSchedule> dataMapper() {
        return scheduleRequestMapper;
    }

    @Bean
    public EventScheduleMapper eventScheduleMapper() {
        return new EventScheduleMapper();
    }

    @Bean
    public BiFunction<Integer, Integer, Pageable> pageBuilder() {
        return PageBuilder::build;
    }

    @Bean
    public TobioGenerator idGenerator() {
        return CodeGenerator::generateStringId;
    }


    @Bean("json-verifier")
    public Function<String, Boolean> jsonVerifier() {
        return JsonUtilities::jsonVerifier;
    }




}
