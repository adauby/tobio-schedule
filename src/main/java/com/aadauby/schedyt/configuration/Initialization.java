package com.aadauby.schedyt.configuration;

import com.aadauby.schedyt.SchedytApplication;
import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.application.repository.ApplicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

@Profile({"prod-http-basic","prod-api-key"})
@Configuration
public class Initialization {
    @Value("${schedyt.admin.secret:Pas5w.rd}")
    private String adminSecret;
    @Bean
    public CommandLineRunner registerAppAdmin(ApplicationRepository repository){
        final Logger logger = LoggerFactory.getLogger(SchedytApplication.class);
        return args ->{
            if (adminSecret.isBlank())
                adminSecret = "Pas5w.rd";

            var adminId = "74894a2088394cb7acdc700a85f0443d";
            repository.findById(adminId).or(()-> {
                logger.info("[INITIALIZATION] - Registering application admin...");
                PasswordEncoder encoder = new BCryptPasswordEncoder();
                var appAdmin = new Application("schedyt", "Application Administrator"
                        , "admin.exchange");
                var encodedSecret = encoder.encode(adminSecret);
                var zoneId = ZoneId.systemDefault();
                appAdmin.setSecret(encodedSecret);
                appAdmin.setId(adminId);
                appAdmin.setRoles("ROLE_ADMIN");
                appAdmin.setZoneId(zoneId.getId());
                appAdmin.setCreatedAt(ZonedDateTime.now(zoneId)
                        .withZoneSameInstant(ZoneId.of("UTC"))
                        .toLocalDateTime());
                logger.info("[INITIALIZATION] - Application admin registered successfully!");
                return Optional.of(repository.save(appAdmin));
            });

        };
    }
}
