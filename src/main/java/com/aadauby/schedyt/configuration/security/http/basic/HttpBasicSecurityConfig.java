package com.aadauby.schedyt.configuration.security.http.basic;

import com.aadauby.schedyt.configuration.security.CustomAccessDeniedHandler;
import com.aadauby.schedyt.configuration.security.CustomAuthEntryPoint;
import com.aadauby.schedyt.utils.constants.Paths;
import com.aadauby.schedyt.utils.constants.Values;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
@Profile({"prod-http-basic","test-prod-http-basic"})
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
public class HttpBasicSecurityConfig {

    private final UserDetailsService userDetailsService;

    public HttpBasicSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
        Values.ACTIVE_PROFILE = "prod-http-basic";
    }

    @Bean
    public SecurityFilterChain httpBasicSecurity(HttpSecurity httpSec) throws Exception {

        return httpSec
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(HttpMethod.POST, Paths.APPLICATIONS).permitAll()
                        .requestMatchers(HttpMethod.GET, "/api-docs.html").permitAll()
                        .anyRequest()
                        .authenticated())
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .userDetailsService(this.userDetailsService)
                .httpBasic(basic -> basic.authenticationEntryPoint(new CustomAuthEntryPoint()))
                .exceptionHandling(exceptions -> exceptions.accessDeniedHandler(new CustomAccessDeniedHandler()))
                .build();
    }

}
