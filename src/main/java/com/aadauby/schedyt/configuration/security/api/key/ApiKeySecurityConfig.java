package com.aadauby.schedyt.configuration.security.api.key;

import com.aadauby.schedyt.utils.constants.Paths;
import com.aadauby.schedyt.utils.constants.Values;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Profile({"prod-api-key","test-prod-api-key"})
@Configuration
@EnableMethodSecurity(prePostEnabled = true)
public class ApiKeySecurityConfig {
    private final ApiKeyAuthFilter apiKeyFilter;

    public ApiKeySecurityConfig(ApiKeyAuthFilter apiKeyFilter) {
        this.apiKeyFilter = apiKeyFilter;
        Values.ACTIVE_PROFILE = "prod-api-key";
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(HttpMethod.POST, Paths.APPLICATIONS).permitAll()
                        .requestMatchers(HttpMethod.POST, Paths.APPLICATIONS+"/*/keys").permitAll()
                        .requestMatchers(HttpMethod.GET, Paths.APPLICATIONS+"/*/keys").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api-docs.html").permitAll()
                        .anyRequest().authenticated()
                )
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                //.addFilterBefore(new CustomApiKeyAuthenticationFilter(authenticationManager), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(apiKeyFilter, UsernamePasswordAuthenticationFilter.class)
                .build();



    }

}
