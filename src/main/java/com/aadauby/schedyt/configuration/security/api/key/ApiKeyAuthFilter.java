package com.aadauby.schedyt.configuration.security.api.key;

import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.utils.constants.Paths;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_API_KEY;
import static com.aadauby.schedyt.utils.constants.Codes.CODE_MISSING_API_KEY;
import static com.aadauby.schedyt.utils.constants.Messages.MSG_INVALID_API_KEY;
import static com.aadauby.schedyt.utils.constants.Messages.MSG_MISSING_API_KEY_HEADER;

@Profile({"prod-api-key","test-prod-api-key"})
@Component
public class ApiKeyAuthFilter extends OncePerRequestFilter {
    private final Logger logger = LoggerFactory.getLogger(ApiKeyAuthFilter.class);

    private final ApplicationRepository applicationRepo;
    private static final String API_KEY_HEADER = "X-API-KEY";
    public ApiKeyAuthFilter(ApplicationRepository applicationRepo) {
        this.applicationRepo = applicationRepo;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {


        if (request.getMethod().equalsIgnoreCase("POST")
                && request.getRequestURI().equals(Paths.APPLICATIONS)) {
            filterChain.doFilter(request, response);
            return;
        }
        if (request.getMethod().equalsIgnoreCase("POST")
                && request.getRequestURI().matches("/v1/applications/[a-zA-Z0-9]+/keys")) {
            filterChain.doFilter(request, response);
            return;
        }
        if (request.getMethod().equalsIgnoreCase("GET")
                && request.getRequestURI().matches("/v1/applications/[a-zA-Z0-9]+/keys")) {
            filterChain.doFilter(request, response);
            return;
        }
        if (request.getMethod().equalsIgnoreCase("GET")
                && request.getRequestURI().equals("/api-docs.html")) {
            filterChain.doFilter(request, response);
            return;
        }

        String apiKey = request.getHeader(API_KEY_HEADER);
        if (apiKey == null || apiKey.isBlank()){
            writeResponse(response, request.getServletPath()
                    , MSG_MISSING_API_KEY_HEADER
                    , CODE_MISSING_API_KEY);
            return;
        }
        var optionalAppli = applicationRepo.findByApiKey(apiKey);
//        optionalAppli.ifPresentOrElse();
        if(optionalAppli.isEmpty()){
            writeResponse(response, request.getServletPath()
                    , MSG_INVALID_API_KEY
                    , CODE_INVALID_API_KEY);
            return;
        }

        var application = optionalAppli.get();
        // Create authentication token with roles
        Collection<? extends GrantedAuthority> authorities = Arrays.stream(application.getRoles().split(","))
                .map(SimpleGrantedAuthority::new)
                .toList();
        logger.debug("Setting authentication with roles: {}",
                authorities.stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.joining(", "))
        );

        ApiKeyAuthenticationToken authentication =
                new ApiKeyAuthenticationToken(apiKey, authorities);
        authentication.setDetails(application);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        logger.info("[Security]-all passed");
        filterChain.doFilter(request, response);
    }


    private void writeResponse(HttpServletResponse response, String path, String message, int errorCode) throws ServletException, IOException {
        Map<String, Object> map = new HashMap<>();
        map.put("timestamp", LocalDateTime.now());
        map.put("errorCode", errorCode);
        map.put("message", message);
        map.put("path", path);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), map);
        } catch (Exception e) {
            throw new ServletException();
        }

    }
}
