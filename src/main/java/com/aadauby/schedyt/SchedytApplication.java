package com.aadauby.schedyt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchedytApplication {


	public static void main(String[] args) {
		SpringApplication.run(SchedytApplication.class, args);
	}




}
