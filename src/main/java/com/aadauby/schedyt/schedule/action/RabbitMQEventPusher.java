package com.aadauby.schedyt.schedule.action;

import com.aadauby.schedyt.application.repository.ApplicationService;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQEventPusher implements JobAction<ScheduleData> {
    private static final Logger logger = LoggerFactory.getLogger(RabbitMQEventPusher.class);
    private final RabbitTemplate rabbitTemplate;
    private final ApplicationService applicationService;



    public RabbitMQEventPusher(RabbitTemplate rabbitTemplate, ApplicationService applicationService) {
        this.rabbitTemplate = rabbitTemplate;
        this.applicationService = applicationService;
    }

    @Override
    public void execute(ScheduleData eventSchedule) {
        try {
            var appInfo = eventSchedule.application();
            var app  = applicationService.findById(appInfo.id());
            logger.info("[RABBITMQ EVENT PUSHER] - [Execute] - Sending event: {}", eventSchedule.event());
            rabbitTemplate.convertAndSend(app.exchangeName(), eventSchedule.routingKey(), JsonUtilities.fromMapToJsonString(eventSchedule.event()));
        }catch (AmqpException ex){
            logger.error("[RABBITMQ EVENT PUSHER] - [Execute] - AmqpException: ", ex);
        }
        //com.rabbitmq.client.AuthenticationFailureException
    }
}
