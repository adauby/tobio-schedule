package com.aadauby.schedyt.schedule.action;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public interface MessageParser<T> {
    final Gson objectParser = new GsonBuilder()
            .create();
    default String parse(T t){
        if(t == null)
            return null;
        return objectParser.toJson(t);
    }
}
