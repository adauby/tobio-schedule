package com.aadauby.schedyt.schedule.action;

public interface JobAction<T> {
    void execute(T t);
}
