package com.aadauby.schedyt.schedule.controller;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import java.util.Map;

public record ScheduleRequest(@NotBlank String eventName, String zoneId
        , String triggerDate, int frequency, int duration, String triggerFirstAt, @NotEmpty Map<String, String>event
        , @NotBlank String routingKey) {
    public ScheduleRequest{
        if (triggerDate == null)
            triggerDate = "";

        if(triggerFirstAt == null)
            triggerFirstAt = "";
    }
}
