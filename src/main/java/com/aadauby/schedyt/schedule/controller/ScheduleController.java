package com.aadauby.schedyt.schedule.controller;

import com.aadauby.schedyt.schedule.calendar.scheduler.ScheduleCalendar;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.schedule.repository.*;
import com.aadauby.schedyt.utils.constants.Paths;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.BiFunction;

import static com.aadauby.schedyt.utils.constants.Keys.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path= Paths.SCHEDULES)
public class ScheduleController {

    private final EventScheduleService eventScheduleService;
    private final BiFunction<Integer, Integer, Pageable>pageBuilder;
    private final SchedytScheduler eventScheduler;



    public ScheduleController(EventScheduleService eventScheduleService
            , BiFunction<Integer, Integer, Pageable> pageBuilder
            , SchedytScheduler eventScheduler){

        this.eventScheduleService = eventScheduleService;
        this.pageBuilder = pageBuilder;
        this.eventScheduler = eventScheduler;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public  ResponseEntity<List<ScheduleData>>findAll(@RequestParam(defaultValue = "10") int size
            , @RequestParam(defaultValue = "0") int page){
        var pageable = pageBuilder.apply(page, size);
        Page<ScheduleData> schedules = eventScheduleService.findAll(pageable);
        if (!schedules.hasContent())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok()
                .header(H_PAGE, String.valueOf(schedules.getNumber()))
                .header(H_TOTAL_ELEMENTS,String.valueOf( schedules.getTotalElements()))
                .header(H_TOTAL_PAGES, String.valueOf(schedules.getTotalPages()))
                .header(H_HAS_NEXT_PAGE,String.valueOf(schedules.hasNext()))
                .body(schedules.getContent());

    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
    @GetMapping(path = Paths.ID, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleData>findById(@PathVariable long id){
        var scheduleData = eventScheduleService.findById(id);
        return ResponseEntity.ok(scheduleData);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
    @GetMapping(path = Paths.NEXT_SCHEDULE_OCCURRENCES, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleCalendar>getNextScheduleOccurrences(@PathVariable long id){
        var upcoming = eventScheduler.getCalendar(id);
        return ResponseEntity.ok(upcoming);
    }



}
