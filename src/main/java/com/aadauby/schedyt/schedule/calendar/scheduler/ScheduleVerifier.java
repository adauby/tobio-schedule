package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.constants.Values;
import com.aadauby.schedyt.utils.exception.InvalidScheduleException;
import org.springframework.stereotype.Component;

import static com.aadauby.schedyt.utils.constants.Codes.*;

@Component
public class ScheduleVerifier {
    public void verify(EventSchedule schedule) throws InvalidScheduleException {
        if(schedule.getFrequency() < Values.FREQUENCY_MIN)
            throw new InvalidScheduleException(Messages.INVALID_FREQUENCY,CODE_INVALID_FREQUENCY);
        if(schedule.getDuration() < Values.DURATION_FOREVER )
            throw new InvalidScheduleException(Messages.INVALID_DURATION, CODE_INVALID_DURATION);
        if( schedule.getDuration() > Values.DURATION_FOREVER  && schedule.getFrequency() > schedule.getDuration())
            throw new InvalidScheduleException(Messages.FREQUENCY_GREATER_THAN_DURATION, CODE_FREQUENCY_GREATER_THAN_DURATION);

    }
}
