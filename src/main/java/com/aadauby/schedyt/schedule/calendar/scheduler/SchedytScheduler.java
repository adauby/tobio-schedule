package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import org.quartz.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;

public abstract class SchedytScheduler {
    protected final Scheduler scheduler;
    private static final int TWENTY_FOUR_HOURS = 24
            , THOUSAND_MILLISECONDS = 1000
            , ONE_MINUTE_IN_SECONDS = 60
            , ONE_HOUR_IN_MINUTES = 60;


    protected SchedytScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public abstract EventSchedule schedule(EventSchedule scheduleData);

    abstract JobDetail buildJobDetail(EventSchedule event);

    abstract Trigger buildTrigger(JobDetail jobDetail, ZonedDateTime startDate, ScheduleBuilder<?> scheduleBuilder);

    protected ScheduleBuilder<?> getScheduleBuilder(EventSchedule schedule) {

        // Just once
        if(schedule.scheduleOnce())
            return SimpleScheduleBuilder
                    .simpleSchedule()
                    .withIntervalInHours(TWENTY_FOUR_HOURS * schedule.getFrequency());

        if(schedule.isMonthly()){
            if(schedule.isForever())
                return CalendarIntervalScheduleBuilder
                        .calendarIntervalSchedule()
                        .withIntervalInMonths(schedule.frequencyInMonths());
            else return SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInHours(schedule.getFrequency() * TWENTY_FOUR_HOURS )
                    .withRepeatCount(schedule.count());
        }else{
            if (schedule.isForever())
                return CalendarIntervalScheduleBuilder
                        .calendarIntervalSchedule()
                        .withIntervalInDays(schedule.getFrequency());
            else return SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(TWENTY_FOUR_HOURS * schedule.getFrequency())
                    .withRepeatCount(schedule.count());
        }

    }

    public int calculateOffsetInHour(String zoneId) {
        TimeZone timeZone = TimeZone.getTimeZone(zoneId);
        var currentOffset = timeZone.getOffset(System.currentTimeMillis());
        return currentOffset / (THOUSAND_MILLISECONDS
                * ONE_MINUTE_IN_SECONDS * ONE_HOUR_IN_MINUTES );
    }

    List<String> getJobGroupNames() throws SchedulerException {
        return scheduler.getJobGroupNames();
    }

    void deleteJobs(List<JobKey> jobKeys) throws SchedulerException {
        scheduler.deleteJobs(jobKeys);
    }

    public abstract ScheduleCalendar getCalendar(Long scheduleId);

    public abstract ScheduleData updateSchedule(EventSchedule schedule);

    public abstract void cancelScheduleEvent(EventSchedule schedule);
}