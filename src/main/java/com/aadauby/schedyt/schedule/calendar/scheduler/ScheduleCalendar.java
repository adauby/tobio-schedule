package com.aadauby.schedyt.schedule.calendar.scheduler;

import java.util.List;
import java.util.Map;

public record ScheduleCalendar(String eventName, long scheduleId, String zoneId
        ,Map<String,String>event, ScheduleState state,List<NextTrigger>upcoming) {
}
