package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.EventScheduleMapper;
import com.aadauby.schedyt.schedule.repository.EventScheduleService;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import com.aadauby.schedyt.utils.constants.Keys;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.InvalidScheduleException;
import com.aadauby.schedyt.utils.exception.ScheduleUpdateException;
import com.aadauby.schedyt.utils.generator.TobioGenerator;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_DATE;
import static com.aadauby.schedyt.utils.constants.Messages.INVALID_SCHEDULE_DATE;
import static com.aadauby.schedyt.utils.constants.Messages.JOB_DETAIL_DESCRIPTION;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_FORMATTER_WITH_ZONE;


@Service
public class EventScheduler extends SchedytScheduler {
    private static final Logger logger = LoggerFactory.getLogger(SchedytScheduler.class);
    private final ScheduleVerifier scheduleVerifier;
    private final EventScheduleService scheduleService;
    private final Consumer<String> datetimeValidator;

    private final Function<String, Boolean> eventDataVerifier;

    private final TobioGenerator keyGenerator;
    private final EventScheduleMapper scheduleMapper;

    public EventScheduler(Scheduler scheduler, ScheduleVerifier scheduleVerifier
            , EventScheduleService scheduleService
            , @Qualifier("date-validator") Consumer<String> datetimeValidator
            , @Qualifier("json-verifier") Function<String, Boolean> eventDataVerifier,
                          TobioGenerator keyGenerator, EventScheduleMapper scheduleMapper) {
        super(scheduler);
        this.scheduleVerifier = scheduleVerifier;
        this.scheduleService = scheduleService;
        this.datetimeValidator = datetimeValidator;
        this.eventDataVerifier = eventDataVerifier;
        this.keyGenerator = keyGenerator;
        this.scheduleMapper = scheduleMapper;
    }

    @Override
    public EventSchedule schedule(EventSchedule eventSchedule) {
        try {

            eventDataVerifier.apply(eventSchedule.getEvent());
            ZonedDateTime eventDate = null;

            if (eventSchedule.scheduleOnce()) {
                eventSchedule.validateTriggerDate();
                eventDate = eventSchedule.getTriggerDate();
                eventSchedule.setFrequency(0);
                eventSchedule.setDuration(0);
            } else {
                eventSchedule.validateTriggerFirstAt();
                scheduleVerifier.verify(eventSchedule);
                eventDate = eventSchedule.getTriggerFirstAt();
            }

            if (eventDate == null)
                throw new InvalidScheduleException(INVALID_SCHEDULE_DATE, CODE_INVALID_DATE);

            eventSchedule.setJobKey(keyGenerator.generate());
            eventSchedule = scheduleService.save(eventSchedule);
            JobDetail jobDetail = buildJobDetail(eventSchedule);
            Trigger trigger = buildTrigger(jobDetail, eventDate
                    , getScheduleBuilder(eventSchedule));

            scheduler.scheduleJob(jobDetail, trigger);

        } catch (SchedulerException e) {
            logger.error("schedule() - SchedulerException: {}", e.getMessage());
        }

        return eventSchedule;
    }


    @Override
    JobDetail buildJobDetail(EventSchedule eventSchedule) {
        var jobDataMap = new JobDataMap();
        jobDataMap.put(Keys.DESC, eventSchedule.obtainDescription());
        jobDataMap.put(Keys.SCHED_ID, eventSchedule.getId());
        jobDataMap.put(Keys.JOB_KEY, eventSchedule.getJobKey());
        return JobBuilder
                .newJob(EventScheduleJob.class)
                .withIdentity(eventSchedule.getApplicationId(), eventSchedule.getJobKey())
                .withDescription(String.format(JOB_DETAIL_DESCRIPTION
                        , eventSchedule.getEventName(), eventSchedule.getFrequency(), eventSchedule.getDuration()))
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Override
    Trigger buildTrigger(JobDetail jobDetail, ZonedDateTime startDate, ScheduleBuilder<?> scheduleBuilder) {
        var jobDetailKey = jobDetail.getKey();
        logger.info("[SCHEDULING] - StartDate: {} --------------------", startDate);

        return TriggerBuilder
                .newTrigger()
                .forJob(jobDetail)
                .withDescription(jobDetail.getDescription())
                .withIdentity(jobDetailKey.getName(), jobDetailKey.getGroup())
                .startAt(Date.from(startDate.toInstant()))
                .withSchedule(scheduleBuilder)
                .build();
    }

    @Override
    public ScheduleCalendar getCalendar(Long scheduleId) {
        var schedule = this.scheduleService.findOne(scheduleId);
        if (schedule.scheduleOnce())
            return getNextTriggerDate(schedule);
        if (schedule.isForever())
            return getNextDateIfForever(schedule);
        return getNextDateIfOnPeriod(schedule);
    }

    private ScheduleCalendar getNextTriggerDate(EventSchedule schedule) {

        if (schedule.neverTriggered()) {
            return new ScheduleCalendar(schedule.getEventName(), schedule.getId(), schedule.getZoneId()
                    , JsonUtilities.fromJsonToMap(schedule.getEvent()), ScheduleState.ACTIVE,

                    List.of(new NextTrigger(schedule.getFormattedTriggerDate())));
        } else {
            return new ScheduleCalendar(schedule.getEventName(), schedule.getId(), schedule.getZoneId()
                    , JsonUtilities.fromJsonToMap(schedule.getEvent()), ScheduleState.TERMINATED,
                    new ArrayList<>());
        }
    }

//    private ScheduleCalendar getNextTriggerDates(EventSchedule schedule) {
//        List<NextTrigger> nextDates = new ArrayList<>();
//        int count = 0;
//        ZonedDateTime today = ZonedDateTime.now(ZoneId.of(schedule.getZoneId()));
//        var start = getStartDate(schedule);
//        ZonedDateTime nextDate = start;
//        if (today.isBefore(start))
//            nextDates.add(new NextTrigger(start.format(DATE_TIME_FORMATTER_WITH_ZONE)));
//
//        var state = ScheduleState.ACTIVE;
//        if (schedule.isForever()) {
//            do {
//                nextDate = nextDate.plusDays(schedule.getFrequency());
//                if (nextDate.isEqual(today) || nextDate.isAfter(today)) {
////                    logger.info("[Getting Next Trigger Dates] - Next {} is equals or after today {}", nextDate, today);
//                    var nextTrigger = new NextTrigger(nextDate.format(DATE_TIME_FORMATTER_WITH_ZONE));
//                    nextDates.add(nextTrigger);
//                    count++;
//                }
//            } while (count < 12);
//        } else {
//
//            ZonedDateTime endDate = start.plusDays(schedule.getDuration());
//            while (count < 12
//                    && (endDate.isAfter(nextDate.plusDays(schedule.getFrequency()))
//                    || endDate.isEqual(nextDate.plusDays(schedule.getFrequency())))) {
//                boolean endDateOvertaken = endDate.isAfter(nextDate.plusDays(schedule.getFrequency()));
//                logger.info("[Getting Next Trigger Dates] - End date is overtaken: {}", endDateOvertaken);
//                nextDate = nextDate.plusDays(schedule.getFrequency());
//                if (nextDate.isEqual(today) || nextDate.isAfter(today)) {
//                    var nextTrigger = new NextTrigger(nextDate.format(DATE_TIME_FORMATTER_WITH_ZONE));
//                    nextDates.add(nextTrigger);
//                    count++;
//                }
//            }
//        }
//        if (nextDates.isEmpty())
//            state = ScheduleState.TERMINATED;
//
//        return new ScheduleCalendar(schedule.getEventName(), schedule.getId(), schedule.getZoneId()
//                , JsonUtilities.fromJsonToMap(schedule.getEvent()), state,
//                nextDates);
//
//    }

    public ScheduleCalendar getNextDateIfForever(EventSchedule schedule) {
        List<NextTrigger> nextDates = new ArrayList<>();
        int count = 0;
        var start = getStartDate(schedule);
        ZonedDateTime nexTriggerDate = start;
        ZonedDateTime today = ZonedDateTime.now(ZoneId.of(schedule.getZoneId()));
        if (start.isAfter(today))
            nextDates.add(new NextTrigger(start.format(DATE_TIME_FORMATTER_WITH_ZONE)));
        do {
            nexTriggerDate = nexTriggerDate.plusDays(schedule.getFrequency());
            if (nexTriggerDate.isEqual(today) || nexTriggerDate.isAfter(today)) {
                var nextTrigger = new NextTrigger(nexTriggerDate.format(DATE_TIME_FORMATTER_WITH_ZONE));
                nextDates.add(nextTrigger);
                count++;
            }
        } while (count < 12);

        return new ScheduleCalendar(schedule.getEventName(), schedule.getId(), schedule.getZoneId()
                , JsonUtilities.fromJsonToMap(schedule.getEvent()), ScheduleState.ACTIVE,
                nextDates);
    }

    public ScheduleCalendar getNextDateIfOnPeriod(EventSchedule schedule) {
        List<NextTrigger> nextDates = new ArrayList<>();
        int count = 0;
        var start = getStartDate(schedule);
        ZonedDateTime nexTriggerDate = start;
        ZonedDateTime endPeriod = start.plusDays(schedule.getDuration());
        ZonedDateTime today = ZonedDateTime.now(ZoneId.of(schedule.getZoneId()));
        if (start.isAfter(today))
            nextDates.add(new NextTrigger(start.format(DATE_TIME_FORMATTER_WITH_ZONE)));
        while (count < 12
                && (endPeriod.isAfter(nexTriggerDate.plusDays(schedule.getFrequency()))
                || endPeriod.isEqual(nexTriggerDate.plusDays(schedule.getFrequency())))) {
            nexTriggerDate = nexTriggerDate.plusDays(schedule.getFrequency());
            if (nexTriggerDate.isEqual(today) || nexTriggerDate.isAfter(today)) {
                var nextTrigger = new NextTrigger(nexTriggerDate.format(DATE_TIME_FORMATTER_WITH_ZONE));
                nextDates.add(nextTrigger);
                count++;
            }
        }

        return new ScheduleCalendar(schedule.getEventName(), schedule.getId(), schedule.getZoneId()
                , JsonUtilities.fromJsonToMap(schedule.getEvent()), ScheduleState.ACTIVE,
                nextDates);
    }


    private ZonedDateTime getStartDate(EventSchedule schedule) {
        if (schedule.isForever()) {
            if (schedule.neverTriggered())
                return schedule.getTriggerFirstAt();
            return schedule.getTriggeredLastAt();
        }
        return schedule.getTriggerFirstAt();

    }

    @Override
    public ScheduleData updateSchedule(EventSchedule schedule) {
        EventSchedule update = null;
        try {
            scheduler.deleteJob(new JobKey(schedule.getApplicationId(), schedule.getJobKey()));
            update = schedule(schedule);
        } catch (SchedulerException e) {
            logger.error("[Update Schedule] - SchedulerException: ", e);
            throw new ScheduleUpdateException(Messages.EXCEPTION_ON_UPDATE);
        }

        return scheduleMapper.toScheduleData(update);
    }

    @Override
    public void cancelScheduleEvent(EventSchedule schedule) {
        try {
            scheduler.deleteJob(new JobKey(schedule.getApplicationId(), schedule.getJobKey()));
        } catch (Exception e) {
            logger.error("[cancelling Schedule Event] - Exception: ", e);
            throw new InternalError(Messages.CANT_DELETE_SCHEDULE);
        }
    }
}
