package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.schedule.action.JobAction;
import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.EventScheduleMapper;
import com.aadauby.schedyt.schedule.repository.EventScheduleService;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import com.aadauby.schedyt.utils.constants.Keys;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.ZoneId;
import java.time.ZonedDateTime;


public class EventScheduleJob extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(EventScheduleJob.class);
    private final JobAction<ScheduleData> eventPusher;
    private final EventScheduleMapper eventScheduleMapper;
    private final EventScheduleService eventScheduleService;


    public EventScheduleJob(JobAction<ScheduleData> eventPusher, EventScheduleMapper eventScheduleMapper, EventScheduleService eventScheduleService) {
        this.eventPusher = eventPusher;
        this.eventScheduleMapper = eventScheduleMapper;
        this.eventScheduleService = eventScheduleService;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        EventSchedule schedule = null;
        String jobKey = "";
        try{
            var jobMap = context.getMergedJobDataMap();
            logger.info("[EVENT SCHEDULE JOB] - [Execute Internal] - Event {} has been triggered", jobMap.getString(Keys.DESC));
            schedule = eventScheduleService.findRawById(jobMap.getLongValue(Keys.SCHED_ID));
            jobKey = jobMap.getString(Keys.JOB_KEY);
            eventPusher.execute(eventScheduleMapper.toScheduleData(schedule));
        }catch (Exception e){
            logger.error("[EVENT SCHEDULE JOB] - [Execute Internal] - Exception: ",e);
        }finally {
            if(schedule!=null){
                ZoneId timeZone = ZoneId.of(schedule.getZoneId());
                ZonedDateTime lastTriggerDate = ZonedDateTime.now(timeZone);
                schedule.setTriggeredLastAt(lastTriggerDate
                        .withZoneSameInstant(timeZone)
                        .toLocalDateTime());
                schedule.setJobKey(jobKey);
                var scheduleData = eventScheduleService.update(schedule);
                logger.info("[EVENT SCHEDULE JOB] - [Execute Internal] - Event ' {} ' has been Updated", scheduleData);
            }
        }

    }
}
