package com.aadauby.schedyt.schedule.repository;

import java.util.Map;

public record ScheduleInfo(Long scheduleId, String eventName, Map<String
        ,String>event, String createdAt, String zoneId, String frequency
        , String period, String triggeredLastAt, String nextTrigger
        , String routingKey) {
}
