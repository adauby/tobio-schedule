package com.aadauby.schedyt.schedule.repository;

import java.util.Map;

public record ScheduleData(Long scheduleId, String eventName, String zoneId, String createdAt, String triggerDate
        , int frequency, int duration, String triggerFirstAt
        , Map<String,String>event, ApplicationInfo application, String routingKey) {

}
