package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.utils.constants.Constants;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.DataNotFoundException;
import com.aadauby.schedyt.utils.exception.DuplicatedDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_DUPLICATE_SCHEDULE;
import static com.aadauby.schedyt.utils.constants.Codes.CODE_SCHEDULE_NOT_FOUND;
import static com.aadauby.schedyt.utils.constants.Messages.dataNotFoundForId;
import static com.aadauby.schedyt.utils.constants.Values.NO_ID;

@Service
public class EventScheduleService {
    private static final Logger logger = LoggerFactory.getLogger(SchedytScheduler.class);
    private final EventScheduleRepository eventScheduleRepository;
    private final EventScheduleMapper eventScheduleMapper;

    public EventScheduleService(EventScheduleRepository eventScheduleRepository
            , EventScheduleMapper eventScheduleMapper) {
        this.eventScheduleRepository = eventScheduleRepository;
        this.eventScheduleMapper = eventScheduleMapper;
    }

    public EventSchedule save(EventSchedule eventSchedule) {
        return eventScheduleRepository.save(eventSchedule);
    }

    public ScheduleData findById(Long id) {
        return eventScheduleRepository.findById(id)
                .map(eventScheduleMapper::toScheduleData)
                .orElseThrow(()-> new DataNotFoundException(dataNotFoundForId("Schedule", String.valueOf(id))
                        , CODE_SCHEDULE_NOT_FOUND));
    }

    public EventSchedule findRawById(Long id){
        return eventScheduleRepository.findById(id)
                .orElseThrow(()-> new DataNotFoundException(dataNotFoundForId("Schedule", String.valueOf(id))
                        , CODE_SCHEDULE_NOT_FOUND));
    }

    public EventSchedule findOne(Long id){
        return eventScheduleRepository.findById(id)
                .orElseThrow(()-> new DataNotFoundException(dataNotFoundForId("Schedule"
                        , String.valueOf(id)), CODE_SCHEDULE_NOT_FOUND));
    }

    public void deleteAll(){
        eventScheduleRepository.deleteAll();
    }

    public Page<ScheduleData> findAll(Pageable page) {
        var pageFound = eventScheduleRepository.findAll(page);
        var mapped = pageFound.getContent()
                .stream()
                .map(eventScheduleMapper::toScheduleData)
                .toList();
        return new PageImpl<>(mapped, PageRequest.of(pageFound.getNumber(),pageFound.getSize()),pageFound.getTotalElements());
    }

    public ScheduleData update(EventSchedule schedule) {
        ScheduleData data = null;
        try{
            var eventSchedule = eventScheduleRepository.save(schedule);
            data = eventScheduleMapper.toScheduleData(eventSchedule);
        }catch (Exception e){
            logger.error("[REPOSITORY]- [Update] - Exception: ",e);
        }
        return data;
    }

    public void delete(EventSchedule schedule){
        try{
            eventScheduleRepository.delete(schedule);
        }catch (Exception e) {
            logger.error("[REPOSITORY]- [Delete] - Exception: ",e);
            //throw new Intern
        }
    }

    public boolean verifyEventNameIsNotUsed(String eventName, long excludedId) {
        var query = QEventSchedule.eventSchedule;

        if(identifierNotCreatedYet(excludedId)){// schedule not created yet
            if(eventScheduleRepository.exists(query.eventName.eq(eventName)))
                throw new DuplicatedDataException(Messages.duplicatedData(eventName, Constants.ENTITY_SCHEDULE), CODE_DUPLICATE_SCHEDULE);
        }else{
            if(eventScheduleRepository.exists(query
                    .id.ne(excludedId)
                    .and(query.eventName.eq(eventName))))
                throw new DuplicatedDataException(Messages.duplicatedData(eventName, Constants.ENTITY_SCHEDULE), CODE_DUPLICATE_SCHEDULE);
        }

        return true;
    }

    //TODO: this function must be isolated and injected
    private boolean identifierNotCreatedYet(long excludedId) {
        return excludedId <= NO_ID;
    }
}
