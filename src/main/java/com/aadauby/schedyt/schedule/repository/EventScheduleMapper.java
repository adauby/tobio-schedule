package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.utils.json.JsonUtilities;

import java.util.Map;

import static com.aadauby.schedyt.utils.constants.Values.NOT_APPLICABLE;

public class EventScheduleMapper {
    public ScheduleData toScheduleData(EventSchedule schedule){
        var app = schedule.getApplication();
        Map event = JsonUtilities.fromJsonToMap(schedule.getEvent());

        return new ScheduleData(schedule.getId(), schedule.getEventName(), schedule.getZoneId()
                , schedule.getFormattedCreatedAt(), schedule.getFormattedTriggerDate()

                , schedule.getFrequency(), schedule.getDuration(), schedule.getFormattedTriggerFirstAt(), event
                , new ApplicationInfo(app.getId(), app.getName()), schedule.getRoutingKey());
    }

    public ScheduleInfo toScheduleInfo(EventSchedule schedule){
        String createdAt = schedule.getFormattedCreatedAt();
        Map eventData = JsonUtilities.fromJsonToMap(schedule.getEvent());
        if(schedule.scheduleOnce())
            return new ScheduleInfo(schedule.getId(), schedule.getEventName(), eventData,createdAt ,schedule.getZoneId(),NOT_APPLICABLE, NOT_APPLICABLE
                    , schedule.generateLastTriggerInfo(), schedule.determineNextOccurrence(),schedule.getRoutingKey());


        String frequency = schedule.generateFrequencyInfo();
        String period = schedule.generatePeriodInfo();
        String next = schedule.determineNextOccurrence();

        return new ScheduleInfo(schedule.getId(),schedule.getEventName(),  eventData, createdAt, schedule.getZoneId(),frequency,period
                , schedule.generateLastTriggerInfo(), next,schedule.getRoutingKey());
    }
}
