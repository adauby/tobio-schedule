package com.aadauby.schedyt.schedule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface EventScheduleRepository extends JpaRepository<EventSchedule, Long>, QuerydslPredicateExecutor<EventSchedule> {


}
