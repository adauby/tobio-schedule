package com.aadauby.schedyt.schedule.repository;

public record ApplicationInfo(String id, String name) {
}
