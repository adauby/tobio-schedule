package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.utils.constants.Codes;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.exception.InvalidDataException;
import com.aadauby.schedyt.utils.exception.InvalidScheduleException;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TimeZone;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.aadauby.schedyt.utils.constants.Messages.INVALID_FIRST_TRIGGER_DATE;
import static com.aadauby.schedyt.utils.constants.Messages.INVALID_SCHEDULE_DATE;
import static com.aadauby.schedyt.utils.constants.Values.*;
import static com.aadauby.schedyt.utils.date.DateUtilities.*;
import static java.time.LocalDateTime.now;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "schedule")
public class EventSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "application_id")
    private Application application;
    @Column(name = "zone_id")
    private String zoneId;
    @Column(name = "event_name", unique = true)
    private String eventName;
    @Column(name = "trigger_date")
    private LocalDateTime triggerDate;
    @Column(name = "trigger_first_at")
    private LocalDateTime triggerFirstAt;
    @Column(name = "triggered_last_at")
    private LocalDateTime triggeredLastAt;
    private int frequency;
    private int duration;
    private  String event;
    @Column(name = "routing_key")
    private String routingKey;
    @Column(name = "job_key")
    private String jobKey;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public EventSchedule(String zoneId){
        if(zoneId == null || zoneId.isBlank())
            zoneId = DEFAULT_ZONE_ID;
        this.zoneId = zoneId;
        this.createdAt = ZonedDateTime.now(ZoneId.of(zoneId))
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime();

    }


    public EventSchedule(String applicationId, String zoneId, LocalDateTime triggerDate
            , int frequency, int duration, String event) {
        this(zoneId);
        this.application = new Application(applicationId,"");
        this.zoneId = zoneId;
        this.triggerDate = triggerDate;
        this.frequency = frequency;
        this.duration = duration;
        this.event = event;
    }

    public EventSchedule(String applicationId, String zoneId, LocalDateTime triggerDate
            , int frequency, int duration, String event,String jobKey) {
        this(zoneId);
        this.application = new Application(applicationId,"");
        this.zoneId = zoneId;
        this.triggerDate = triggerDate;
        this.frequency = frequency;
        this.duration = duration;
        this.event = event;
        this.jobKey = jobKey;
    }

    public EventSchedule(Application application, String zoneId, String eventName
            , LocalDateTime triggerDate, int frequency, int duration, String event) {
        this(zoneId);
        this.application = application;
        this.zoneId = zoneId;
        this.eventName = eventName;
        this.triggerDate = triggerDate;
        this.frequency = frequency;
        this.duration = duration;
        this.event = event;
    }

    public EventSchedule(Long id, Application application, String zoneId, LocalDateTime triggerDate
            , int frequency, int duration, String event, String jobKey) {
        this(zoneId);
        this.id = id;
        this.application = application;
        this.zoneId = zoneId;
        this.triggerDate = triggerDate;
        this.frequency = frequency;
        this.duration = duration;
        this.event = event;
        this.jobKey = jobKey;
    }

    public static EventSchedule build(ScheduleData scheduleData, Function<Map,String>fromMapToJsonString) {
        var app = new Application();
        var appInfo = scheduleData.application();
        var zid = ZoneId.of(scheduleData.zoneId());
        app.setId(appInfo.id());
        app.setName(appInfo.name());
        var triggerDate = convertStringToZonedDatetime(scheduleData.triggerDate(),ZoneId.of(scheduleData.zoneId()));

        LocalDateTime triggerDatetime =  null;
        if(triggerDate != null)
            triggerDatetime = triggerDate.
                    withZoneSameInstant(ZoneId.of("UTC"))
                    .toLocalDateTime();
        EventSchedule schedule = new EventSchedule(app, scheduleData.zoneId(), scheduleData.eventName(),triggerDatetime
                , scheduleData.frequency(), scheduleData.duration(), fromMapToJsonString.apply(scheduleData.event()));
        if(schedule.getCreatedAt() == null){
            var creationTime = ZonedDateTime.now(zid)
                    .withZoneSameInstant(ZoneId.of("UTC"))
                    .toLocalDateTime();
            schedule.setCreatedAt(creationTime);
        }

        schedule.setRoutingKey(scheduleData.routingKey());
        schedule.setId(scheduleData.scheduleId());
        return schedule;
    }

    public boolean scheduleOnce() {
        return triggerDate!=null;
    }

    public boolean isForever() {
        return duration == DURATION_FOREVER;
    }

    public boolean isMonthly() {
        return frequency > FREQUENCY_MIN && (frequency % ONE_MONTH) == 0;
    }

    public int frequencyInMonths() {
        return frequency / ONE_MONTH;
    }

    public int count() {
        return duration/frequency;
    }

    public String getApplicationId(){
        return this.application.getId();
    }

    @Override
    public String toString() {
        return "EventSchedule{" +
                "scheduleId='" + id + '\'' +
                ", application='" + application.getId() + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", triggerDate='" + triggerDate + '\'' +
                ", frequency=" + frequency +
                ", duration=" + duration +
                ", event='" + event + '\'' +
                '}';
    }

    public String obtainDescription() {
        return String.format("Event %s from service %s from %s.",id, application.getId(), zoneId);
    }

    public String generateFrequencyInfo() {
        if(frequency == 1)
            return "Everyday";
        return String.format("Every %d day(s)",frequency);
    }

    public String generatePeriodInfo() {
        if(isForever())
            return "Forever";
//        if(isMonthly())
//            return String.format("%d month(s)",this.obtainTotalMonths());
        return String.format("%d day(s)", this.duration);
    }

    public String getExchangeName() {
        return this.application.getExchangeName();
    }

    public String generateLastTriggerInfo() {
        if(this.triggeredLastAt == null)
            return "Never triggered";
        return getFormattedTriggeredLastAt();
    }

    public boolean neverTriggered(){
        return triggeredLastAt == null;
    }

    public String determineNextOccurrence(){
        if(scheduleOnce() && !neverTriggered())
            return "Finished";
        if(scheduleOnce() && neverTriggered())
            return getFormattedTriggerDate();

        ZonedDateTime nextOccurrence = null;
        if(neverTriggered()) {
            nextOccurrence = getTriggerFirstAt();
        }else
            nextOccurrence = getTriggeredLastAt()
                    .plusDays(frequency);

        if(!isForever()){
          ZonedDateTime endOfPeriod = getTriggerFirstAt().plusDays(duration);
          if(endOfPeriod.isBefore(nextOccurrence))
              return "Finished";
        }
        return nextOccurrence.format(DATE_TIME_FORMATTER_WITH_ZONE);
    }

    public void update(EventSchedule update) {
        this.eventName = update.getEventName();
        this.zoneId = update.getZoneId();

        this.triggerDate = update.triggerDate;

        this.frequency = update.getFrequency();
        this.duration = update.getDuration();
        this.event = update.getEvent();
        this.triggerFirstAt = update.triggerFirstAt;
        this.routingKey = update.getRoutingKey();
        this.updatedAt = ZonedDateTime.now(ZoneId.of(this.zoneId))
                .withZoneSameInstant(ZoneId.of(this.zoneId))
                .toLocalDateTime() ;
    }

    public boolean keepCalendar(LocalDateTime newTriggerDate, int newFrequency
            , int newDuration, LocalDateTime newTriggerFirstAt, String zoneId) {

        return triggerDate == newTriggerDate && frequency == newFrequency
                && duration == newDuration && triggerFirstAt == newTriggerFirstAt
                && this.zoneId.equals(zoneId);
    }


    public boolean hasFirstTriggerDateDefined() {
        return triggerFirstAt != null;
    }

    public void validateTriggerDate() {
        if(triggerDate!=null)
            validateTriggerDatetime(getTriggerDate(),INVALID_SCHEDULE_DATE);
    }

    public void validateTriggerFirstAt() {
        if(this.triggerFirstAt!=null )
            validateTriggerDatetime(this.getTriggerFirstAt(), INVALID_FIRST_TRIGGER_DATE);

    }

    public void setTriggerFirstDateIfUnset(){
        if(!this.scheduleOnce() && this.createdAt!=null && this.triggerFirstAt == null)
            this.triggerFirstAt = this.createdAt.plusDays(this.frequency);
    }
    private void validateTriggerDatetime(ZonedDateTime triggerDatetime, String exceptionMsg){
        ZoneId zid = ZoneId.of(this.zoneId);
        ZonedDateTime zonedNow = ZonedDateTime.now(zid);
        if (triggerDatetime.isBefore(zonedNow)) {
            throw new InvalidScheduleException(exceptionMsg
                    , Codes.CODE_INVALID_DATE);
        }
    }


    public String getFormattedCreatedAt() {
        return getCreatedAt()
                .format(DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss z"));
    }
    public ZonedDateTime getCreatedAt(){
        return createdAt.atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }

    public ZonedDateTime getTriggeredLastAt(){
        if(triggeredLastAt == null)
            return null;

        return triggeredLastAt.atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }
     public ZonedDateTime getTriggerFirstAt(){
         if(triggerFirstAt == null)
             return getCreatedAt().plusDays(this.frequency);

        return triggerFirstAt.atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }
    public ZonedDateTime getTriggerDate(){
        if(triggerDate == null)
            return null;
        return triggerDate.atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }


    public String getFormattedTriggeredLastAt() {
        if(triggeredLastAt == null)
            return null;
        return getTriggeredLastAt()
                .format(DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss z"));
    }

    public String getFormattedTriggerFirstAt() {
        if(triggerFirstAt == null)
            return null;
        return getTriggerFirstAt()
                .format(DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss z"));
    }
    public String getFormattedTriggerDate() {
        if(triggerDate == null)
            return null;
        return getTriggerDate()
                .format(DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss z"));
    }
}
