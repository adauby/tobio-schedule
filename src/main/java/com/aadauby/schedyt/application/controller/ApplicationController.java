package com.aadauby.schedyt.application.controller;

import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.application.repository.ApplicationData;
import com.aadauby.schedyt.application.repository.ApplicationService;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.schedule.controller.ScheduleRequest;
import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.EventScheduleService;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import com.aadauby.schedyt.utils.constants.Paths;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.aadauby.schedyt.utils.constants.Keys.*;
import static com.aadauby.schedyt.utils.constants.Paths.APPLICATIONS;
import static com.aadauby.schedyt.utils.constants.Paths.APPLICATION_SCHEDULE;
import static com.aadauby.schedyt.utils.date.DateUtilities.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = APPLICATIONS)
public class ApplicationController {

    private final ApplicationService appService;
    private final BiFunction<Integer, Integer, Pageable> pageBuilder;
    private final SchedytScheduler eventScheduler;
    private final EventScheduleService scheduleService;

    private final Function<RegisterRequest, Application> registerRequestMapper = (request) -> {
        var app = new Application(request.name(), request.description(), request.exchangeName());
        app.setSecret(request.secret());
        app.setZoneId(request.zoneId());
        return app;
    };
    //    private final Function<PasswordRequest, Application> registerRequestMapper = (request) -> {
//        var app = new Application(request.name(), request.description(), request.exchangeName());
//        app.setSecret(request.secret());
//        return app;
//    };
    private final Function<ApplicationRequest, Application> applicationRequestMapper = (request) -> {
        var app = new Application(request.name(), request.description()
                , request.exchangeName());
        app.setZoneId(request.zoneId());
        return app;
    };

    private final Function<ScheduleRequest, EventSchedule> scheduleRequestMapper;

    public ApplicationController(ApplicationService appService
            , BiFunction<Integer, Integer, Pageable> pageBuilder, SchedytScheduler eventScheduler, EventScheduleService scheduleService
            , @Qualifier("scheduleRequest-mapper") Function<ScheduleRequest, EventSchedule> scheduleRequestMapper) {
        this.appService = appService;
        this.pageBuilder = pageBuilder;
        this.eventScheduler = eventScheduler;
        this.scheduleService = scheduleService;
        this.scheduleRequestMapper = scheduleRequestMapper;
    }

    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping(path = Paths.APPLICATION_SCHEDULES, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationSchedules> getSchedulesByApplication(@PathVariable(name = "application_id") String applicationId, @RequestParam(defaultValue = "10") int size
            , @RequestParam(defaultValue = "0") int page) {
        var pageable = pageBuilder.apply(page, size);
        var schedulePage = appService.findAllSchedules(applicationId, pageable);
        if (!schedulePage.hasContent())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok()
                .header(H_PAGE, String.valueOf(schedulePage.getNumber()))
                .header(H_TOTAL_ELEMENTS, String.valueOf(schedulePage.getTotalElements()))
                .header(H_TOTAL_PAGES, String.valueOf(schedulePage.getTotalPages()))
                .header(H_HAS_NEXT_PAGE, String.valueOf(schedulePage.hasNext()))
                .body(new ApplicationSchedules(applicationId, schedulePage.getContent()));
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationData> registerApplication(@RequestBody @Validated RegisterRequest request) {

        var registered = appService.register(registerRequestMapper.apply(request));
        return ResponseEntity
                .status(CREATED)
                .body(registered);
    }
    @PreAuthorize("hasRole('CLIENT')")
    @PostMapping(path = Paths.APPLICATION_SCHEDULES, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleData> scheduleEvent(@PathVariable(name = "application_id") String applicationId
            , @RequestBody @Validated ScheduleRequest request) {
        var application = appService.findById(applicationId);
        var scheduleToBeCreate = scheduleRequestMapper.apply(request);
        if(request.zoneId() == null || request.zoneId().isBlank()) {
            scheduleToBeCreate.setZoneId(application.zoneId());
            var creationTime = ZonedDateTime.now(ZoneId.of(scheduleToBeCreate.getZoneId()))
                    .withZoneSameInstant(ZoneId.of("UTC"))
                    .toLocalDateTime();
            scheduleToBeCreate.setCreatedAt(creationTime);
        }
        scheduleToBeCreate.setTriggerFirstDateIfUnset();
        scheduleService.verifyEventNameIsNotUsed(request.eventName(), 0);
        scheduleToBeCreate.setApplication(new Application(application.id(), application.name()
                , application.description(), application.exchangeName()));
        var createdSchedule = eventScheduler.schedule(scheduleToBeCreate);
        return ResponseEntity
                .status(CREATED)
                .body(appService.toScheduleData(createdSchedule));
    }
    @PreAuthorize("hasRole('CLIENT')")
    @PutMapping(path = APPLICATION_SCHEDULE, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleData> updateScheduleEvent(@PathVariable(name = "application_id") String appId
            , @PathVariable(name = "schedule_id") long scheduleId
            , @RequestBody @Validated ScheduleRequest request) {
        var appConcerned = appService.findById(appId);
        var scheduleFound = appService.findApplicationSchedule(appConcerned.id(), scheduleId);

        String zoneId = (request.zoneId() == null || request.zoneId().isBlank()) ?
                appConcerned.zoneId() : request.zoneId() ;
        scheduleService.verifyEventNameIsNotUsed(request.eventName(), scheduleFound.getId());
        ScheduleData updated;
        var triggerDate = convertStringToDate(request.triggerDate(), DATE_TIME_FORMATTER, zoneId);
        var triggerFirstAt = convertStringToDate(request.triggerFirstAt(), DATE_TIME_WITHOUT_SECONDS_FORMATTER, zoneId);
        var scheduleUpdate = scheduleRequestMapper.apply(request);
        scheduleUpdate.setZoneId(zoneId);
        if (scheduleFound.keepCalendar(triggerDate, request.frequency()
                , request.duration(), triggerFirstAt, zoneId)) {
            scheduleFound.update(scheduleUpdate);
            scheduleFound = scheduleService.save(scheduleFound);
            updated = appService.toScheduleData(scheduleFound);
        } else {
            //TODO: create a transaction
            scheduleFound.update(scheduleUpdate);
            updated = eventScheduler.updateSchedule(scheduleFound);
        }
        return ResponseEntity.ok(updated);
    }
    @PreAuthorize("hasRole('CLIENT')")
    @PutMapping(path = Paths.ID, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationData> updateApplication(@PathVariable String id, @RequestBody @Validated ApplicationRequest request) {
        var appUpdate = applicationRequestMapper.apply(request);
        appUpdate.setId(id);
        var appUpdated = appService.update(appUpdate);
        return ResponseEntity.ok(appUpdated);
    }

    @PreAuthorize("hasRole('CLIENT')")
    @PutMapping(path = Paths.APP_PASSWORD, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationData> updateApplicationSecret(@PathVariable String id, @RequestBody @Validated PasswordRequest request) {
        var appUpdated = appService.updatePassword(id, request.currentSecret(), request.newSecret());
        return ResponseEntity.ok(appUpdated);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
    @DeleteMapping(path = APPLICATION_SCHEDULE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleData> cancelScheduleEvent(@PathVariable(name = "application_id") String appId
            , @PathVariable(name = "schedule_id") long scheduleId) {
        var appConcerned = appService.findById(appId);
        var scheduleFound = appService.findApplicationSchedule(appConcerned.id(), scheduleId);
        eventScheduler.cancelScheduleEvent(scheduleFound);
//        scheduleFound.setCancelled(true);
        scheduleService.delete(scheduleFound);
        return ResponseEntity.noContent()
                .build();
    }

    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping(path = Paths.ID, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ApplicationData> findOneApplication(@PathVariable("id") String id) {
        var found = appService.findById(id);
        return ResponseEntity.ok(found);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ApplicationData>> findApplications(@RequestParam(defaultValue = "10") int size
            , @RequestParam(defaultValue = "0") int page) {
        var pageable = pageBuilder.apply(page, size);
        var appsPage = appService.findAll(pageable);
        if (!appsPage.hasContent())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok()
                .header(H_PAGE, String.valueOf(appsPage.getNumber()))
                .header(H_TOTAL_ELEMENTS, String.valueOf(appsPage.getTotalElements()))
                .header(H_TOTAL_PAGES, String.valueOf(appsPage.getTotalPages()))
                .header(H_HAS_NEXT_PAGE, String.valueOf(appsPage.hasNext()))
                .body(appsPage.getContent());
    }

}
