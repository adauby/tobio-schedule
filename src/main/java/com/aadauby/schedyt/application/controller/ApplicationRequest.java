package com.aadauby.schedyt.application.controller;


import jakarta.validation.constraints.NotBlank;

public record ApplicationRequest(@NotBlank String name, String description
        , @NotBlank String exchangeName, @NotBlank String zoneId) {
}
