package com.aadauby.schedyt.application.controller;

import com.aadauby.schedyt.schedule.repository.ScheduleInfo;

import java.util.List;

public record ApplicationSchedules(String applicationId, List<ScheduleInfo>schedules) {
}
