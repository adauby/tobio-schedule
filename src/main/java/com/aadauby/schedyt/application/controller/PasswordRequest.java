package com.aadauby.schedyt.application.controller;

import jakarta.validation.constraints.NotBlank;

public record PasswordRequest(@NotBlank String currentSecret, @NotBlank String newSecret) {
}
