package com.aadauby.schedyt.application.controller;


import jakarta.validation.constraints.NotBlank;

public record RegisterRequest(@NotBlank String name, String description, @NotBlank String exchangeName
        , @NotBlank String secret, @NotBlank String zoneId) {
}
