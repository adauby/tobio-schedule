package com.aadauby.schedyt.application.repository;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

//@Profile({"default","test-prod-api-key"})
public interface ApplicationRepository extends JpaRepository<Application, String>, QuerydslPredicateExecutor<EventSchedule> {
    public Optional<Application> findByName(String applicationName);
    public Optional<Application>findByApiKey(String apiKey);
}
