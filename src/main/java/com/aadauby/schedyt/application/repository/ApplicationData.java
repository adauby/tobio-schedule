package com.aadauby.schedyt.application.repository;

public record ApplicationData(String id, String name, String description,String exchangeName
        , String zoneId, String createdAt) {
}
