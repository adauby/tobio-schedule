package com.aadauby.schedyt.application.repository;

import com.aadauby.schedyt.schedule.repository.*;
import com.aadauby.schedyt.utils.constants.Codes;
import com.aadauby.schedyt.utils.constants.Constants;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.constants.Values;
import com.aadauby.schedyt.utils.exception.AuthException;
import com.aadauby.schedyt.utils.exception.DataNotFoundException;
import com.aadauby.schedyt.utils.exception.DuplicatedDataException;
import com.aadauby.schedyt.utils.exception.InvalidDataException;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import com.aadauby.schedyt.utils.generator.TobioGenerator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.function.Function;

import static com.aadauby.schedyt.utils.constants.Codes.*;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_APP;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_SCHEDULE;
import static com.aadauby.schedyt.utils.constants.Messages.dataNotFoundForId;
import static com.aadauby.schedyt.utils.constants.Values.ACTIVE_PROFILE;

@Service
public class ApplicationService {

    private final ApplicationRepository applicationRepository;
    private final Function<Application, ApplicationData> applicationMapper;
    private final EventScheduleRepository eventScheduleRepository;
    private final TobioGenerator idGenerator;
    private final EventScheduleMapper eventScheduleMapper;
    private final PasswordEncoder passwordEncoder;

    public ApplicationService(ApplicationRepository applicationRepository
            , @Qualifier("application-mapper") Function<Application, ApplicationData> applicationMapper, EventScheduleRepository eventScheduleRepository
            , TobioGenerator idGenerator, EventScheduleMapper eventScheduleMapper, PasswordEncoder passwordEncoder) {
        this.applicationRepository = applicationRepository;
        this.applicationMapper = applicationMapper;
        this.eventScheduleRepository = eventScheduleRepository;
        this.idGenerator = idGenerator;
        this.eventScheduleMapper = eventScheduleMapper;
        this.passwordEncoder = passwordEncoder;
    }

    public void checkIfApplicationExists(String applicationId) {
        if (!applicationRepository.existsById(applicationId))
            throw new DataNotFoundException(dataNotFoundForId(ENTITY_APP, applicationId), CODE_APPLICATION_NOT_FOUND);
    }

    public ApplicationData register(Application application) {
        verifyIfNameIsAlreadyUsed(application.getName(), null);
        application.setCreatedAt(ZonedDateTime.now(ZoneId.of(application.getZoneId()))
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var encodedSecret = passwordEncoder.encode(application.getSecret());
        application.setSecret(encodedSecret);
        if (ACTIVE_PROFILE != null && ACTIVE_PROFILE.equals("prod-api-key")) {
            application.generateNewApiKey();
        }

        application.setId(idGenerator.generate());

        var created = applicationRepository.save(application);
        return applicationMapper.apply(created);
    }

    public Page<ScheduleInfo> findAllSchedules(String applicationId, Pageable page) {
        checkIfApplicationExists(applicationId);
        QEventSchedule query = QEventSchedule.eventSchedule;
        var PageOfSchedules = eventScheduleRepository.findAll(query.application.id.eq(applicationId), page);
        var listOfInfo = PageOfSchedules.getContent()
                .stream()
                .map(eventScheduleMapper::toScheduleInfo)
                .toList();
        return new PageImpl<>(listOfInfo, PageRequest.of(PageOfSchedules.getNumber(), PageOfSchedules.getSize())
                , PageOfSchedules.getTotalElements());

    }

    public ApplicationData findById(String applicationId) {
        return applicationRepository.findById(applicationId)
                .map(applicationMapper)
                .orElseThrow(() -> new DataNotFoundException(dataNotFoundForId(ENTITY_APP, applicationId), CODE_APPLICATION_NOT_FOUND));
    }

    public ScheduleData toScheduleData(EventSchedule schedule) {
        return eventScheduleMapper.toScheduleData(schedule);
    }

    public EventSchedule findApplicationSchedule(String appId, long scheduleId) {
        var query = QEventSchedule.eventSchedule;
        return eventScheduleRepository.findOne(query
                        .id.eq(scheduleId)
                        .and(query.application.id.eq(appId)))
                .orElseThrow(() -> new DataNotFoundException(
                        dataNotFoundForId(ENTITY_SCHEDULE, String.valueOf(scheduleId)), CODE_SCHEDULE_NOT_FOUND)
                );
    }


    public Page<ApplicationData> findAll(Pageable page) {
        var applications = applicationRepository.findAll(page);
        var listOfAppData = applications.getContent()
                .stream()
                .map(applicationMapper)
                .toList();
        return new PageImpl<>(listOfAppData, PageRequest.of(applications.getNumber(), applications.getSize())
                , applications.getTotalElements());

    }

    public ApplicationData update(Application appUpdate) {
        var appFound = applicationRepository.findById(appUpdate.getId())
                .orElseThrow(() -> new DataNotFoundException(dataNotFoundForId(ENTITY_APP, appUpdate.getId())
                        , CODE_APPLICATION_NOT_FOUND));
        this.verifyIfNameIsAlreadyUsed(appUpdate.getName(), appUpdate.getId());
        appFound.update(appUpdate);
        return applicationMapper.apply(applicationRepository.save(appFound));
    }

    public boolean verifyIfNameIsAlreadyUsed(String appName, String excludedId) {
        var query = QApplication.application;

        if (identifierNotCreatedYet(excludedId)) {
            if (applicationRepository.exists(query.name.eq(appName)))
                throw new DuplicatedDataException(Messages.duplicatedData(appName, ENTITY_APP), CODE_DUPLICATE_APPLICATION_NAME);
        } else {
            if (applicationRepository.exists(query
                    .id.ne(excludedId)
                    .and(query.name.eq(appName))))
                throw new DuplicatedDataException(Messages.duplicatedData(appName, Constants.ENTITY_APP), CODE_DUPLICATE_APPLICATION_NAME);
        }

        return true;
    }

    //TODO: this function must be isolated and injected
    private boolean identifierNotCreatedYet(String excludedId) {
        return excludedId == null || excludedId.isBlank();
    }


    public ApplicationData updatePassword(String id, String currentPassword, String newPassword) {
        var appFound = applicationRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(dataNotFoundForId(ENTITY_APP, id)
                        , CODE_APPLICATION_NOT_FOUND));
        if (passwordEncoder.matches(currentPassword, appFound.getSecret()))
            throw new AuthException(Messages.INVALID_SECRET, Codes.CODE_INVALID_SECRET);

        var newEncodedSecret = passwordEncoder.encode(newPassword);
        appFound.setSecret(newEncodedSecret);
        appFound = applicationRepository.save(appFound);
        return applicationMapper.apply(appFound);
    }

}
