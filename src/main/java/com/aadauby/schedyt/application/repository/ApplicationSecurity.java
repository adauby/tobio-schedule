package com.aadauby.schedyt.application.repository;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Arrays;
import java.util.Collection;
@Profile({"prod-http-basic"})
public class ApplicationSecurity implements UserDetails{
    @Serial
    private static final long serialVersionUID = 1L;

    private final Application app;



    public ApplicationSecurity(Application app) {
        super();
        this.app = app;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        var roles = Arrays.stream(this
                        .app.getRoles().split(","))
                .map(SimpleGrantedAuthority::new)
                .toList();
//		for(var role: roles)
//			log.info("===> role: {}",role);

        return Arrays.stream(this
                        .app.getRoles().split(","))
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    @Override
    public String getPassword() {
        return app.getSecret();
    }

    @Override
    public String getUsername() {
        return app.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true ;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true	;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }

}
