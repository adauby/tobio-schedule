package com.aadauby.schedyt.application.repository;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_FORMATTER_WITH_ZONE;
@Profile({"default","test-prod-api-key"})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "application")
public class Application {
    @Id
    private String id;
    @Column(name = "name", unique = true)
    private String name;
    private String description;
    @Column(name = "exchange_name")
    private String exchangeName;
    @OneToMany
    @JoinColumn(name = "application_id")
    private List<EventSchedule>eventSchedules=new ArrayList<>();
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    private String secret;
    @Column(name = "api_key")
    private String apiKey;
    @Column(name = "api_key_last_update")
    private LocalDateTime apiKeyLastUpdate;
    @Column(name = "secret_last_update")
    private LocalDateTime secretLastUpdate;
    @Column(name ="zone_id")
    private String zoneId;
    private String roles = "ROLE_CLIENT";
    public Application(String name) {
        this.name = name;
    }




    public Application(String id, String name, String description, String exchangeName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.exchangeName = exchangeName;
    }

    public Application(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public Application(String name, String description, String exchangeName) {
        this.name = name;
        this.description = description;
        this.exchangeName = exchangeName;
    }

    public Application(String id, String name, String description, String exchangeName, List<EventSchedule> eventSchedules) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.exchangeName = exchangeName;
        this.eventSchedules = eventSchedules;
    }

    public void update(Application appUpdate) {
        name = appUpdate.getName();
        description = appUpdate.getDescription();
        exchangeName = appUpdate.getExchangeName();
        zoneId = appUpdate.getZoneId();
    }

    public void updateSecret(String secret) {
        this.secret = secret;
        this.secretLastUpdate = LocalDateTime.now();
    }
    public void generateNewApiKey() {
        this.apiKey = UUID.randomUUID().toString();
        this.apiKeyLastUpdate = ZonedDateTime.now(ZoneId.of(zoneId))
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime();
    }

    public ZonedDateTime getCreatedAt(){
        return createdAt
                .atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }

    public ZonedDateTime getApiKeyLastUpdate(){
        if(apiKeyLastUpdate == null)
            return null;
        return apiKeyLastUpdate
                .atZone(ZoneId.of("UTC"))
                .withZoneSameInstant(ZoneId.of(zoneId));
    }
    public String getFormattedCreatedAt() {
        return getCreatedAt()
                .format(DATE_TIME_FORMATTER_WITH_ZONE);
    }
    public String getFormattedApiKeyLastUpdate() {
        return getApiKeyLastUpdate()
                .format(DATE_TIME_FORMATTER_WITH_ZONE);
    }
}


