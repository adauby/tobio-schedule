package com.aadauby.schedyt.application.repository;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
@Profile("prod-http-basic")
@Service
public class ApplicationSecurityService implements UserDetailsService {
    private final ApplicationRepository appService;

    public ApplicationSecurityService(ApplicationRepository appService) {
        super();
        this.appService = appService;
    }

    @Override
    public UserDetails loadUserByUsername(String applicationName) throws UsernameNotFoundException {
        return appService.findByName(applicationName)
                .map(ApplicationSecurity::new)
                .orElseThrow(()-> new UsernameNotFoundException("Application Not Found with name: " + applicationName));
    }

}
