package com.aadauby.schedyt.utils.json;

import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.EventFormatException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public final class JsonUtilities {
    private JsonUtilities(){}
    private static final TypeAdapter<JsonElement> strictAdapter = new Gson().getAdapter(JsonElement.class);
    private static final Gson gson = new Gson();
    public static boolean jsonVerifier(String json){
        try {
            strictAdapter.fromJson(json);
        } catch (JsonSyntaxException | IOException e) {
            throw new EventFormatException(Messages.INVALID_EVENT_FORMAT);
        }
        return true;
    }

    public static String fromMapToJsonString(Map<String, String>map){
        Type typeObject = new TypeToken<HashMap<String,String>>() {}.getType();
        return gson.toJson(map, typeObject);
    }


    public static Map fromJsonToMap(String jsonString) {
        return gson.fromJson(jsonString, Map.class);
    }
}
