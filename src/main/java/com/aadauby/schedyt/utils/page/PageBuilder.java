package com.aadauby.schedyt.utils.page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PageBuilder {
    public static final int DEFAULT_SIZE=10;
    public static final int MAX_SIZE=50;
    public static final int DEFAULT_PAGE=0;
    public static Pageable build(int page, int size) {
        if(page<0)
            page=DEFAULT_PAGE;
//        else if (page>0)
//            page =page-1;

        if(size<=0)
            size =DEFAULT_SIZE;
        else if (size>MAX_SIZE) {
            size=MAX_SIZE;
        }

        return PageRequest.of(page,size);
    }
}
