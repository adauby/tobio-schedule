package com.aadauby.schedyt.utils.exception;

import lombok.Getter;

@Getter
public class DataNotFoundException extends RuntimeException {
    private int code;
    public DataNotFoundException(String message, int code) {
        super(message);
        this.code = code;
    }
}
