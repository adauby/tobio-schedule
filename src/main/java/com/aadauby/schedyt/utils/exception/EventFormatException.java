package com.aadauby.schedyt.utils.exception;

public class EventFormatException extends RuntimeException{
    public EventFormatException(String message) {
        super(message);
    }
}
