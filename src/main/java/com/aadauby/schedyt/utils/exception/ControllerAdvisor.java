package com.aadauby.schedyt.utils.exception;


import com.aadauby.schedyt.utils.constants.Codes;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;


import jakarta.servlet.http.HttpServletRequest;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRulesException;
import java.util.List;
import java.util.Objects;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_ZONE_ID;

@RestControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(DataNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)

    public ErrorResponse handleDataNotFoundException(DataNotFoundException exception
            , WebRequest webRequest){

        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), exception.getCode()
                , request.getRequestURI());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();
        List<ObjectError> e= exception.getAllErrors();
        var objectError = e.get(0);
        String message = " " + objectError.getDefaultMessage();
        DefaultMessageSourceResolvable sourceResolvable = (DefaultMessageSourceResolvable)Objects
                .requireNonNull(objectError.getArguments())[0];
        String requestObjectField = sourceResolvable.getDefaultMessage();

        assert requestObjectField != null;
        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), requestObjectField.concat(message), Codes.CODE_INVALID_FIELD
                , request.getRequestURI());
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleAuthExceptionException(AuthException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), exception.getCode()
                , request.getRequestURI());
    }

//    @ExceptionHandler(AuthException.class)
//    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
//    @ResponseBody
//    public ErrorResponse handleDataNotFoundException(AuthException exception
//            ,WebRequest webRequest){
//        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();
//
//        return new ErrorResponse(exception.getMessage(), HttpStatus.UNAUTHORIZED.value()
//                ,request.getRequestURI());
//    }

    @ExceptionHandler(InvalidScheduleException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidScheduleException(InvalidScheduleException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), exception.getCode()
                , request.getRequestURI());

    }
    @ExceptionHandler(InvalidDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidDataException(InvalidDataException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), exception.getCode()
                , request.getRequestURI());

    }
    @ExceptionHandler(EventFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidDataException(EventFormatException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), HttpStatus.BAD_REQUEST.value()
                , request.getRequestURI());

    }
//
    @ExceptionHandler(DuplicatedDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleDuplicatedDataException(DuplicatedDataException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), exception.getCode()
                , request.getRequestURI());

    }
 @ExceptionHandler(ZoneRulesException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleZoneRulesException(ZoneRulesException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), CODE_INVALID_ZONE_ID
                , request.getRequestURI());
    }
    @ExceptionHandler(InternalError.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleInternalErrorException(InternalError exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()
                , request.getRequestURI());
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse handleDefaultHandlerExceptionResolver(MissingRequestHeaderException exception
            ,WebRequest webRequest){
        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        return new ErrorResponse(ZonedDateTime.now(ZoneId.systemDefault()), exception.getMessage(), HttpStatus.UNAUTHORIZED.value()
                , request.getRequestURI());
    }

}
