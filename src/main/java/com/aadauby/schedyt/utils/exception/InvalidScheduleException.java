package com.aadauby.schedyt.utils.exception;

import lombok.Getter;

@Getter
public class InvalidScheduleException extends RuntimeException {
    private final int code;
    public InvalidScheduleException(String message, int code) {
        super(message);
        this.code = code;
    }

}
