package com.aadauby.schedyt.utils.exception;

import lombok.Getter;

@Getter
public class DuplicatedDataException extends RuntimeException{
    private final int code;
    public DuplicatedDataException(String message, int code) {
        super(message);
        this.code = code;
    }
}
