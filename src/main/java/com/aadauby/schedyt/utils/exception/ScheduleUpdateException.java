package com.aadauby.schedyt.utils.exception;

public class ScheduleUpdateException extends RuntimeException{
    public ScheduleUpdateException(String message) {
        super(message);
    }
}
