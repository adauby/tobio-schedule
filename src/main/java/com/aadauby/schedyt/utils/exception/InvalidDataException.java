package com.aadauby.schedyt.utils.exception;

import lombok.Getter;

@Getter
public class InvalidDataException extends RuntimeException{

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    private final int code;
    public InvalidDataException(String message, int code) {
        super(message);
        this.code = code;
    }
}
