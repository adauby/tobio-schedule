package com.aadauby.schedyt.utils.exception;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public record ErrorResponse(ZonedDateTime timestamp , String message, int errorCode, String path) {
}
