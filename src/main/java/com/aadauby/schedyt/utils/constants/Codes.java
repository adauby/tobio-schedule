package com.aadauby.schedyt.utils.constants;

public final class Codes {
    public static final int CODE_APPLICATION_NOT_FOUND = 40;
    public static final int CODE_SCHEDULE_NOT_FOUND = 41;
    public static final int CODE_INVALID_DATE = 42;
    public static final int CODE_INVALID_DATE_FORMAT = 43;
    public static final int CODE_INVALID_FREQUENCY = 44;
    public static final int CODE_INVALID_DURATION = 45;
    public static final int CODE_INVALID_ZONE_ID = 46;
    public static final String MSG_INVALID_ZONE_ID = "Invalid zone ID";

    public static final int CODE_FREQUENCY_GREATER_THAN_DURATION = 46;
    //public static final int CODE_DUPLICATE_APPLICATION = 60;
    public static final int CODE_INVALID_FIELD = 60;
    public static final int CODE_DUPLICATE_APPLICATION_NAME = 61;
    public static final int CODE_DUPLICATE_SCHEDULE = 62;
    public static final int CODE_INVALID_SECRET = 70;


    public static final int CODE_MISSING_API_KEY = 90;
    public static final int CODE_INVALID_API_KEY = 91;
    public static final int CODE_INVALID_USERNAME_OR_PASSWORD = 92;
    public static final int CODE_ACCESS_DENIED = 93;



    private Codes(){}
}
