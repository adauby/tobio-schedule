package com.aadauby.schedyt.utils.constants;

public final class Keys {

    public static final String H_PAGE = "x-page";
    public static final String H_TOTAL_ELEMENTS = "x-total-count";
    public static final String H_TOTAL_PAGES = "x-pages";
    public  static final String H_HAS_NEXT_PAGE = "x-has-next-page";
    public static final String DESC = "Description";

    public static final String SCHED_ID = "scheduleId";
    public static final String JOB_KEY = "jobKey";


    private Keys(){}


}
