package com.aadauby.schedyt.utils.constants;

public final class Messages {
    public static final String INVALID_EVENT_FORMAT= "Invalid event. It may not be null or empty. It must have a valid json syntax.";
    public static final String FREQUENCY_GREATER_THAN_DURATION = "The frequency cannot be greater than the duration";
    public static final String INVALID_PERIOD_MSG = "Invalid period";
    public static final String NULL_PERIOD = INVALID_PERIOD_MSG + ": start date or end date cannot be null.";
    public static final String INVALID_FREQUENCY= "Invalid frequency";
    public static final String INVALID_DURATION = "Invalid duration";
    public static final String INVALID_DATETIME_FORMAT = "Invalid datetime format. datetime format must be 'yyyy-MM-dd HH:mm:ss'. ";
    public static final String INVALID_SCHEDULE_DATE = "Date of schedule must be later than the current date and time. Refer to the 'triggerDate' parameter";
    public static final String INVALID_FIRST_TRIGGER_DATE = "The date of the first trigger must be later than the current date and time. Refer to the 'triggerFirstAt' parameter ";
    public static final String DUPLICATED_DATA = "\"%s\" is already used by another %s";
    public static final String EXCEPTION_ON_UPDATE = "An error occurred when trying to update schedule. please retry later.";
    public static final String JOB_DETAIL_DESCRIPTION = "Schedule job for event \"%s\" that triggers every %d day(s) for a period of %d day(s)";
    public static final String CANT_DELETE_SCHEDULE = "An error occurred when trying to cancel event \"%s\". Please try later";
    public static final String MSG_INVALID_API_KEY = "Authentication failure: Invalid API Key";
    public static final String MSG_MISSING_API_KEY_HEADER = "Authentication failure: Missing API Key header";
    public static final String INVALID_SECRET = "Invalid secret";
    public static final String INVALID_USERNAME_OR_PASSWORD = "Basic Auth: Invalid username or password. Check your application name and secret";
    public static final String ACCESS_DENIED = "Access Denied: Application '%s' doesn't have required permissions";


    private Messages(){}


    public static String dataNotFoundForId(String entity, String id) {
        return String.format("%s not found for id %s", entity, id);
    }

    public static String duplicatedData(String data, String entity) {
        return String.format(DUPLICATED_DATA, data, entity);
    }
}
