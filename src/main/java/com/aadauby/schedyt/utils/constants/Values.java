package com.aadauby.schedyt.utils.constants;

import org.springframework.beans.factory.annotation.Value;

public final class Values {
    public static final String DEFAULT_ZONE_ID = "Africa/Abidjan";
    public static final int ONE_MONTH = 30;
    public static final String NOT_APPLICABLE = "N/A";

    private Values(){}
    public static int FREQUENCY_MIN=1;

    public static int DURATION_FOREVER = 0;
    public static long NO_ID = 0;
    public static String POFILE_PROD_HTTP_BASIC = "";

    @Value("${spring.profiles.active}")
    public static String ACTIVE_PROFILE;







}
