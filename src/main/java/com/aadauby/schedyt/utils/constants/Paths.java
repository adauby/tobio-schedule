package com.aadauby.schedyt.utils.constants;

public class Paths {
    public static final String VERSION="/v1";
    public static final String SCHEDULES = VERSION+"/schedules";
    public static final String APPLICATIONS = VERSION+"/applications";
    public static final String APPLICATION_ID = "/{application_id}";
    public static final String ID = "/{id}";
    public static final String NEXT_SCHEDULE_OCCURRENCES = ID + "/calendar";
    public static final String APPLICATION_SCHEDULES = APPLICATION_ID + "/schedules";
    public static final String APPLICATION_SCHEDULE = APPLICATION_ID + "/schedules/{schedule_id}";
    public static final String TOKEN = VERSION + "/tokens";
    public static final String API_KEYS = "/{id}/keys";
    public static final String APP_KEY = ID + API_KEYS;
    public static final String APP_PASSWORD = "/{id}/secrets";

    private Paths(){}


}
