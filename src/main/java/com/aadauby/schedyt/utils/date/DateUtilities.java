package com.aadauby.schedyt.utils.date;

import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.InvalidDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.TimeZone;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_DATE_FORMAT;
import static com.aadauby.schedyt.utils.constants.Messages.INVALID_DATETIME_FORMAT;


public final class DateUtilities {
    private static final Logger logger = LoggerFactory.getLogger(DateUtilities.class);

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter DATE_TIME_WITHOUT_SECONDS_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    public static DateTimeFormatter DATE_TIME_FORMATTER_WITH_ZONE = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
    private DateUtilities() {}


    public static LocalDateTime convertStringToDate(String stringDatetime
            , DateTimeFormatter formatter, String timeZone){
        ZonedDateTime zonedDatetime = null;

        try{
            if(stringDatetime==null || stringDatetime.isBlank())
                return null;

            var datetime =  LocalDateTime.parse(stringDatetime, formatter);
            zonedDatetime = datetime.atZone(ZoneId.of(timeZone));

        }catch (DateTimeParseException e){
            throw new InvalidDataException(INVALID_DATETIME_FORMAT, CODE_INVALID_DATE_FORMAT);
        }

        return zonedDatetime
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime();
    }

    private static long getOffset(String zoneId) {
        TimeZone timeZone = TimeZone.getTimeZone(zoneId);
        int currentOffset = timeZone.getOffset(System.currentTimeMillis());
        return currentOffset / (1000 * 60 * 60);
    }

    public static ZonedDateTime convertStringToZonedDatetime(String stringDatetime, String zoneTime
            , DateTimeFormatter formatter){
        LocalDateTime localDatetime = convertStringToDate(stringDatetime, formatter, zoneTime);
        ZonedDateTime zonedDateTime = null;
        try{
            if(localDatetime==null)
                return null;
            zonedDateTime =  ZonedDateTime.of(localDatetime, ZoneId.of(zoneTime));
        }catch (DateTimeParseException e){
            logger.error("[DATE UTILITIES] - [Convert String To LocalDatetime] - DateTimeParseException: ",e);
        }

        return zonedDateTime;
    }

    public static String convertDatetimeToStringFormat(LocalDateTime datetime) {
        if(datetime==null)
            return null;
        return DATE_TIME_FORMATTER.format(datetime);
    }
    public static String convertDatetimeToStringFormat(ZonedDateTime datetime) {

        if(datetime==null)
            return null;
        return DATE_TIME_FORMATTER.format(datetime);
    }

    public static void verifyPeriod(LocalDate start, LocalDate end) {
        if(start==null || end == null)
            throw new InvalidPeriodException(Messages.NULL_PERIOD);
        if(start.isAfter(end))
            throw new InvalidPeriodException(Messages.INVALID_PERIOD_MSG);
    }

    public static ZonedDateTime convertStringToZonedDatetime(String datetimeWithZone, ZoneId zoneId){
        if(datetimeWithZone == null || datetimeWithZone.isBlank() || zoneId == null)
            return null;
        var formatterWithZone = DATE_TIME_FORMATTER_WITH_ZONE.withZone(zoneId);
        return ZonedDateTime.parse(datetimeWithZone, formatterWithZone);
    }
}
