package com.aadauby.schedyt.utils.generator;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public final class CodeGenerator {
    private static final AtomicLong ID_GENERATOR = new AtomicLong();
    private CodeGenerator(){}

    public static String generateStringId(){
        UUID id =  UUID.randomUUID();
        String idStringFormat = id.toString();
        return idStringFormat.replace("-","");
    }

    public static String generateApiKey() {
        return UUID.randomUUID().toString();
    }

//    public static Long generateLongId() {
//        return ID_GENERATOR.incrementAndGet();
//    }
}
