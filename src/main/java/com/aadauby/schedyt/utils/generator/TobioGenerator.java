package com.aadauby.schedyt.utils.generator;

public interface TobioGenerator {
    String generate();
}
