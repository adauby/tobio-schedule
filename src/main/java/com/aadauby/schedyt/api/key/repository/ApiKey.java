package com.aadauby.schedyt.api.key.repository;

import org.springframework.context.annotation.Profile;

@Profile({"prod-api-key","test-prod-api-key"})
public record ApiKey(String applicationId, String applicationName,String apiKey
        , String updatedAt) {
}
