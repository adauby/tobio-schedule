package com.aadauby.schedyt.api.key.repository;

import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.utils.constants.Codes;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.AuthException;
import com.aadauby.schedyt.utils.exception.DataNotFoundException;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_APPLICATION_NOT_FOUND;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_APP;
import static com.aadauby.schedyt.utils.constants.Messages.dataNotFoundForId;
import static com.aadauby.schedyt.utils.date.DateUtilities.convertDatetimeToStringFormat;

@Profile({"prod-api-key","test-prod-api-key"})
@Service
public class ApiKeyService {
    //private final Logger logger = LoggerFactory.getLogger(ApiKeyService.class);
    private final ApplicationRepository applicationRepository;
    private final PasswordEncoder passwordEncoder;

    public ApiKeyService(ApplicationRepository applicationRepository, PasswordEncoder passwordEncoder) {
        this.applicationRepository = applicationRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public ApiKey generateApiKey(String applicationId, String secret) {
        var appFound = applicationRepository.findById(applicationId)
                .orElseThrow(()-> new DataNotFoundException(dataNotFoundForId(ENTITY_APP, applicationId)
                        , CODE_APPLICATION_NOT_FOUND));
        if(!passwordEncoder.matches(secret, appFound.getSecret()))
            throw new AuthException(Messages.INVALID_SECRET,Codes.CODE_INVALID_SECRET);
        appFound.generateNewApiKey();

        applicationRepository.save(appFound);
        var lastUpdate = appFound.getFormattedApiKeyLastUpdate();
        return new ApiKey(appFound.getId(),appFound.getName()
                ,appFound.getApiKey(),lastUpdate);
    }

    public ApiKey findApiKey(String appId, String secret) {
        var found = applicationRepository.findById(appId)
                .orElseThrow(()-> new DataNotFoundException(dataNotFoundForId(ENTITY_APP, appId)
                        , CODE_APPLICATION_NOT_FOUND));
        if(!passwordEncoder.matches(secret, found.getSecret()))
            throw new AuthException(Messages.INVALID_SECRET,Codes.CODE_INVALID_SECRET);
        var lastApiKeyUpdate = found.getFormattedApiKeyLastUpdate();
        return new ApiKey(found.getId(),found.getName(),found.getApiKey(), lastApiKeyUpdate);

    }


}
