package com.aadauby.schedyt.api.key.controller;

import com.aadauby.schedyt.api.key.repository.ApiKey;
import com.aadauby.schedyt.api.key.repository.ApiKeyService;
import com.aadauby.schedyt.utils.constants.Paths;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Profile({"prod-api-key","test-prod-api-key"})
@RequestMapping(Paths.APPLICATIONS)
@RestController
public class ApiKeyController {
    private final ApiKeyService apiKeyService;

    public ApiKeyController(ApiKeyService apiKeyService) {
        this.apiKeyService = apiKeyService;
    }

//    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
    @GetMapping(path = Paths.API_KEYS)
    public ResponseEntity<ApiKey> getApplicationKey(@RequestHeader("X-APP-SECRET")String secret
            , @PathVariable("id") String appId){
        var apiKey = apiKeyService.findApiKey(appId, secret);
        return ResponseEntity.ok(apiKey);
    }

//    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
    @PostMapping(Paths.API_KEYS)
    public ResponseEntity<ApiKey> generateApiKey(@RequestHeader("X-APP-SECRET")String secret
            ,@PathVariable("id") String appId){
        var apiKey = apiKeyService.generateApiKey(appId, secret);
        return ResponseEntity.ok(apiKey);
    }
}
