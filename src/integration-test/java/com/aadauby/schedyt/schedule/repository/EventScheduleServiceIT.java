package com.aadauby.schedyt.schedule.repository;


//@SpringBootTest
public class EventScheduleServiceIT /*implements MariadbTestContainer*/ {


//    @Autowired
//    EventScheduleService eventScheduleService;
//
//    @Autowired
//    EventScheduleRepository scheduleRepository;
//
//    @Autowired
//    ApplicationRepository appRepository;
//
//    Application app = new Application();
//
//    @BeforeEach
//    void setUp(){
//        app = appRepository.save(new Application(IdGenerator.generateStringId()
//                ,"test app","","message.direct"));
//    }
//
//    @AfterEach
//    void afterEach(){
//        appRepository.deleteAll();
//        scheduleRepository.deleteAll();
//
//    }
//
//    @Test
//    public void testSave(){
//        var toSave = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 30,"{\"event\":\"Google I/O next month\"}");
//        toSave.setRoutingKey("schedule");
//        var schedule = eventScheduleService.save(toSave);
//        assertAll("Verify event schedule data saved"
//                , ()-> assertNotNull(schedule.getId())
//                , ()-> assertTrue(schedule.getId()>0)
//                , ()-> assertEquals(app.getId(), schedule.getApplicationId())
//                , ()-> assertEquals(30, schedule.getDuration())
//                , ()-> assertEquals(10, schedule.getFrequency())
//                , ()-> assertEquals(DEFAULT_ZONE_ID, schedule.getZoneId())
//                , ()-> assertEquals("", schedule.getTriggerDate())
//                , ()-> assertEquals("{\"event\":\"Google I/O next month\"}", schedule.getEvent())
//                , ()-> assertEquals("schedule", schedule.getRoutingKey()));
//
//    }
//
//    @Test
//    public void testFindByIdIfIdExists(){
//        var scheduleToSave = new EventSchedule(app, DEFAULT_ZONE_ID,"party", "2024-05-10 15:00:00"
//                , 7, 90,"{\"event\":\"Google I/O next month\"}");
//        scheduleToSave.setEventName("My Event");
//        scheduleToSave.setRoutingKey("broker.routing.key");
//        var created = eventScheduleService.save(scheduleToSave);
//
//        assertAll("Very found data"
//                , ()-> assertTrue(created.getId()>0)
//                , ()-> assertEquals( "My Event", created.getEventName())
//                , ()-> assertEquals( 90, created.getDuration())
//                , ()-> assertEquals(7, created.getFrequency())
//                , ()-> assertEquals(DEFAULT_ZONE_ID, created.getZoneId())
//                , ()-> assertEquals("2024-05-10 15:00:00", created.getTriggerDate() )
//                , ()-> assertEquals(app.getId(), created.getApplicationId())
//                , ()-> assertEquals(app.getName(), created.getApplication().getName())
//                , ()-> assertEquals("{\"event\":\"Google I/O next month\"}", created.getEvent())
//                , ()-> assertEquals("broker.routing.key", created.getRoutingKey()));
//    }
//
//    @Test
//    void testFindByIfIfIdNotExists(){
//        var thrown = assertThrows(DataNotFoundException.class,()-> eventScheduleService.findById((long)7));
//        assertEquals("Schedule not found for id 7", thrown.getMessage());
//    }
//
//    @Test
//    void testFindAllIfPageIsThreeAndSizeTwenty(){
//        List<EventSchedule>schedules = new ArrayList<>();
//        for(var i=0; i<100;i++){
//            var sched = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, "",1
//                    ,30,"{\"event\":\"Google I/O next month\"}", generateStringId());
//            sched.setRoutingKey("tobio.key");
//            sched.setEventName("Name"+i);
//            schedules.add(sched);
//
//        }
//        scheduleRepository.saveAll(schedules);
//        var pageFound = eventScheduleService.findAll(PageRequest.of(3,20));
//        assertAll("Verify event schedule page"
//                , ()->assertEquals(3, pageFound.getNumber())
//                , ()->assertEquals(20, pageFound.getSize())
//                , ()-> assertEquals(20, pageFound.getContent().size())
//                , ()-> assertInstanceOf(ScheduleData.class, pageFound.getContent().get(0))
//                , ()-> assertEquals(100, pageFound.getTotalElements())
//                , ()-> assertEquals(5, pageFound.getTotalPages()));
//
//
//    }
//
//    @Test
//    void testVerifyEventNameIfEventNameIsUsedByAnotherSchedule(){
//        var firstSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 30,"{\"event\":\"Google I/O next month\"}");
//        firstSaved.setRoutingKey("schedule");
//        firstSaved.setEventName("exists");
//        firstSaved.setJobKey(generateStringId());
//        scheduleRepository.save(firstSaved);
//
//        var secondSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 300,"{\"event\":\"Conference\"}");
//        secondSaved.setRoutingKey("routing.key");
//        secondSaved.setEventName("new event");
//        secondSaved.setJobKey(generateStringId());
//        secondSaved = scheduleRepository.save(secondSaved);
//        secondSaved.setEventName("exists");
//        EventSchedule finalSecondSaved = secondSaved;
//        var thrown = assertThrows(DuplicatedDataException.class
//                , ()-> eventScheduleService.verifyEventNameIsNotUsed(finalSecondSaved.getEventName(), finalSecondSaved.getId()));
//        assertEquals(duplicatedData(finalSecondSaved.getEventName(), Constants.ENTITY_SCHEDULE), thrown.getMessage());
//    }
//
//    @Test
//    void testVerifyEventNameIfEventNameIsNotUsedByAnotherSchedule(){
//        var firstSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 30,"{\"event\":\"Google I/O next month\"}");
//        firstSaved.setRoutingKey("schedule");
//        firstSaved.setEventName("exists");
//        firstSaved.setJobKey(generateStringId());
//        scheduleRepository.save(firstSaved);
//
//        var secondSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 300,"{\"event\":\"Conference\"}");
//        secondSaved.setRoutingKey("routing.key");
//        secondSaved.setEventName("new event");
//        secondSaved.setJobKey(generateStringId());
//        secondSaved = scheduleRepository.save(secondSaved);
//        secondSaved.setEventName("fresh");
//        EventSchedule finalSecondSaved = secondSaved;
//        assertTrue(eventScheduleService.verifyEventNameIsNotUsed(finalSecondSaved.getEventName(), finalSecondSaved.getId()));
//    }
//
//    @Test
//    void testVerifyEventNameIfEventNameIsUsedByTheScheduleItself(){
//        var firstSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 30,"{\"event\":\"Google I/O next month\"}");
//        firstSaved.setRoutingKey("schedule");
//        firstSaved.setEventName("exists");
//        firstSaved.setJobKey(generateStringId());
//        scheduleRepository.save(firstSaved);
//
//        var secondSaved = new EventSchedule(app.getId(), DEFAULT_ZONE_ID, ""
//                , 10, 300,"{\"event\":\"Conference\"}");
//        secondSaved.setRoutingKey("routing.key");
//        secondSaved.setEventName("new event");
//        secondSaved.setJobKey(generateStringId());
//        secondSaved = scheduleRepository.save(secondSaved);
//        EventSchedule finalSecondSaved = secondSaved;
//        assertTrue(eventScheduleService.verifyEventNameIsNotUsed(finalSecondSaved.getEventName(), finalSecondSaved.getId()));
//    }

}
