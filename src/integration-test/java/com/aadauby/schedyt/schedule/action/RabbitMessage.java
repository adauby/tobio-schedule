package com.aadauby.schedyt.schedule.action;

import lombok.Data;

@Data
public class RabbitMessage {
    private String content="";

    public boolean contains(String string){
        return this.content.contains(string);
    }
}
