package com.aadauby.schedyt.schedule.calendar.scheduler;

//import com.aadauby.tobio.MariadbTestContainer;
//import com.aadauby.tobio.schedule.repository.EventSchedule;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.quartz.JobKey;
//import org.quartz.Scheduler;
//import org.quartz.SchedulerException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
//import static com.aadauby.tobio.schedule.utils.constants.Values.DEFAULT_ZONE_ID;
//import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
public class EventSchedulerIT /*implements MariadbTestContainer*/ {
//    @Autowired
//    Scheduler scheduler;
//
//    @Autowired
//    EventScheduler eventScheduler;
//
//    private final String serviceId = "ab4169f25ef458";
//
//    @BeforeEach
//    void beforeEach() throws SchedulerException {
//        List<String> groupNames = eventScheduler.getJobGroupNames();
//        if(!groupNames.isEmpty()){
//            List<JobKey>jobKeys = groupNames.stream().map(JobKey::new).toList();
//            eventScheduler.deleteJobs(jobKeys);
//        }
//
//    }
//
//    @Test
//    void shouldReturnOneJobKey() throws SchedulerException {
//        var eventSchedule = new EventSchedule(serviceId, DEFAULT_ZONE_ID, ""
//                , 10, 30,"my event");
//        eventScheduler.schedule(eventSchedule);
//        List<String>groupNames = eventScheduler.getJobGroupNames();
//        assertFalse(groupNames.isEmpty());
//        String jobKey = eventSchedule.getJobKey();
//
//        assertEquals(1, groupNames.size());
//        assertEquals(jobKey, groupNames.get(0));
//    }
//
//    @Test
//    void shouldReturnTwoJobKeys() throws SchedulerException {
//        var firstEventSchedule = new EventSchedule(serviceId, DEFAULT_ZONE_ID, ""
//                , 10, 30,"my event");
//        var secondEventSchedule = new EventSchedule(serviceId, DEFAULT_ZONE_ID, "2024-10-01 17:00:00"
//                , 10, 30,"my event");
//        eventScheduler.schedule(firstEventSchedule);
//        eventScheduler.schedule(secondEventSchedule);
//        assertFalse(scheduler.getJobGroupNames().isEmpty());
//        String jobKeyOne = firstEventSchedule.getJobKey();
//        String jobKeyTwo = secondEventSchedule.getJobKey();
//        List<String>groupNames = scheduler.getJobGroupNames();
//        assertFalse(groupNames.isEmpty());
//        assertEquals(2, groupNames.size());
//        assertTrue(groupNames.contains(jobKeyOne));
//        assertTrue(groupNames.contains(jobKeyTwo));
//    }
}
