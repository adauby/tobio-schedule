package com.aadauby.schedyt.schedule.configuration;


import com.aadauby.schedyt.schedule.action.RabbitMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

//@Profile("it-test-rabbit")
//@Component
public class RabbitMQMessageReceiver {
//    private static final Logger logger = LoggerFactory.getLogger(RabbitMQMessageReceiver.class);
//
//    private final RabbitMessage receivedMessage;
//
//    public RabbitMQMessageReceiver(RabbitMessage receivedMessage) {
//        this.receivedMessage = receivedMessage;
//    }
//
//    @RabbitListener(queues = "#{receiverQueue.name}")
//    public void receive(String message) {
//
//        try {
//            receivedMessage.setContent(message);
//            logger.info(receivedMessage.getContent());
//        } catch (Exception e) {
//            logger.error("An exception occurred: {}", e.getMessage());
//            logger.error("Exception: ",e);
//        }
//    }
}
