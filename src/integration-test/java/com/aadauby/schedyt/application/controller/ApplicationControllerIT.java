package com.aadauby.schedyt.application.controller;

import com.aadauby.schedyt.MariadbTestContainer;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationControllerIT implements MariadbTestContainer {

//    @LocalServerPort
//    private Integer port;
//
//    @Autowired
//    private ApplicationRepository repository;
//
//    @BeforeEach
//    void setUp(){
//        RestAssured.baseURI = "http://localhost:" + port;
//        //repository.deleteAll();
//    }
//
//    @Test
//    void shouldReturnCreatedApplication(){
//        RestAssured.with()
//                .body(new ApplicationRequest("my app","","message.direct"))
//                .contentType(ContentType.JSON)
//                .when()
//                .post(Paths.APPLICATIONS)
//                .then()
//                .statusCode(HttpStatus.CREATED.value())
//                .body("id", Matchers.notNullValue())
//                .body("name", Matchers.is("my app"))
//                .body("description", Matchers.is(""))
//                .body("exchangeName", Matchers.is("message.direct"));
//    }

//    @Test
//    void shouldReturnCreatedEventSchedule(){
//        //Application application, String zoneId, String eventName
//        //            , String triggerOn, int frequency, int duration, String event
//        var request = new ScheduleRequest("Conference",DEFAULT_ZONE_ID,"",3,120,"my event");
//
//        with()
//
//                .body(request)
//                .contentType(ContentType.JSON)
//                .when()
//                .post(Paths.SCHEDULES)
//                .then()
//                .statusCode(HttpStatus.CREATED.value())
//                .body("frequency", Matchers.is(3))
//                .body("duration", Matchers.is(120))
//                .body("eventName", Matchers.is("Conference"))
//                .body("event", Matchers.is("my event"))
//                .body("application.id", Matchers.is(app.getId()))
//                .body("application.name", Matchers.is(app.getName()))
//                .body("scheduleId", Matchers.greaterThan(0));
//    }
//
//    @Test
//    void shouldReturnBadRequest(){
//
//        with()
//                .body(new ScheduleRequest("Conference","","",1,10,"my event data"))
//                .contentType(ContentType.JSON)
//                .when()
//                .post(Paths.SCHEDULES)
//                .then()
//                .statusCode(HttpStatus.BAD_REQUEST.value())
//                .body("timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*"))
//                .body("message", Matchers.is("zoneId must not be blank"))
//                .body("status", Matchers.is(HttpStatus.BAD_REQUEST.value()))
//                .body("path", Matchers.is(Paths.SCHEDULES));
//
//    }

}
