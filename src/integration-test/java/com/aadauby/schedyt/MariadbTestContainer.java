package com.aadauby.schedyt;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@ActiveProfiles("it-test-db")
public interface MariadbTestContainer {
    static MariaDBContainer<?> mariadb = new MariaDBContainer<>(
            "mariadb:10.4.4"
    );

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry){
        registry.add("spring.datasource.url", mariadb::getJdbcUrl);
        registry.add("spring.datasource.username", mariadb::getUsername);
        registry.add("spring.datasource.password", mariadb::getPassword);
    }

    @BeforeAll
    static void beforeAll(){
        mariadb.start();
    }


    @AfterAll
    static void afterAll(){
        mariadb.stop();
    }
}
