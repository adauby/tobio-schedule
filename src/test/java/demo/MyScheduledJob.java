package demo;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyScheduledJob implements Job {
    Logger logger = LoggerFactory.getLogger(MyScheduledJob.class);
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("Executing scheduled job: " + System.currentTimeMillis());
        var data = context.getMergedJobDataMap();
        logger.info("Executing scheduled job: {}",data.get("message"));

    }
}
