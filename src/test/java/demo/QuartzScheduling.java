package demo;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

public class QuartzScheduling {
    public static void main(String[] args)throws SchedulerException {
        // Create a JobDetail instance
        TimeZone gooseBayTimeZone = TimeZone.getTimeZone("America/Scoresbysund");

        // Get the raw offset from UTC (in milliseconds)
        int rawOffset = gooseBayTimeZone.getRawOffset();

        // Convert milliseconds to hours
        int offsetHours = rawOffset / (1000 * 60 * 60);

        // Display the offset
        System.out.println("Raw Offset for Goose Bay: " + rawOffset + " milliseconds");
        System.out.println("Offset in Hours: " + offsetHours + " hours");

        int currentOffset = gooseBayTimeZone.getOffset(System.currentTimeMillis());
        System.out.println("Current Offset considering DST: " + (currentOffset / (1000 * 60 * 60)) + " hours");
        var datetime = LocalDateTime.now();
        System.out.println("Current LocaDatetime: " + datetime);
        System.out.println("Current LocaDatetime plus offset: " + datetime.plusHours((currentOffset / (1000 * 60 * 60))));

//
        // Step 2: Calculate the start time (17:05 today in the desired time zone)
       var timeZoneAbidjan = buildStartTime("Africa/Abidjan");
       var timeZoneParis = buildStartTime("America/Scoresbysund");
//       var timeZoneParis_2 = buildStartTime("Europe/Paris");

        // Step 3: Define the CalendarIntervalTrigger


        // Step 4: Create and start the Scheduler
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.start();

        // Step 5: Schedule the job
        scheduler.scheduleJob(buildJobDetail(1,"Africa/Abidjan"), buildTrigger(timeZoneAbidjan, "Africa/Abidjan", 1));
        scheduler.scheduleJob(buildJobDetail(2,"America/Scoresbysund"), buildTrigger(timeZoneParis, "Europe/Paris", 2));
//        scheduler.scheduleJob(buildJobDetail(3,"America/New_York"), buildTrigger(timeZoneParis_2, "America/New_York", 3));
    }

    private static Trigger buildTrigger(Date startTime, String timeZone, int count){
        return TriggerBuilder.newTrigger()
                .withIdentity("dailyTrigger"+count, "dailyGroup")
                .startAt(startTime) // Start at 17:05
                .withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule()
                        .withIntervalInMinutes(60) // Repeat every 1 day
                        .inTimeZone(TimeZone.getTimeZone(timeZone))
                        // Set time zone
                )
                .build();
    }

    private static JobDetail buildJobDetail(int count, String timeZone){
        var jobMap = new JobDataMap();
        jobMap.put("message", "message for "+timeZone);
        return JobBuilder.newJob(MyScheduledJob.class)
                .withIdentity("dailyJob"+count, "dailyGroup")
                .setJobData(jobMap)
                .build();
    }

    private static Date buildStartTime(String timeZone){
        LocalDateTime localDateTime = LocalDateTime.now();
//                .withHour(17)
//                .withMinute(50)
//                .withSecond(0)
//                .withNano(0);
        System.out.println("**********Start time from "+timeZone+" : "+Date.from(localDateTime.atZone(ZoneId.of(timeZone)).toInstant()));
        return Date.from(localDateTime.atZone(ZoneId.of(timeZone)).toInstant());
    }
}
