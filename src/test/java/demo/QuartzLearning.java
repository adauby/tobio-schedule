package demo;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Properties;

public class QuartzLearning {
    public static void main(String... args) throws SchedulerException {
        /*
         * Starting scheduler
         */
        Scheduler scheduler = initiateTheDefaultScheduler();
        // initiateSpecificSchedulerWithSpecificProperties();
        PlacingSchedulerInStandByMode(scheduler);


        /*
         * Shuting down scheduler
         */
//        waitForExecutingJobsToFinish(scheduler);
        doNotWaitForExecutionJobsToFinish(scheduler);
    }

    private static void doNotWaitForExecutionJobsToFinish(Scheduler scheduler) throws SchedulerException {
        scheduler.shutdown();
        //or
        //scheduler.shutdown(false);
    }

    private static void waitForExecutingJobsToFinish(Scheduler scheduler) throws SchedulerException {
        scheduler.shutdown(true);//shutdown() does not return until executing Jobs complete execution
    }

    private static void PlacingSchedulerInStandByMode(Scheduler scheduler) throws SchedulerException {
        scheduler.start();

        scheduler.standby();// now the scheduler will not fire triggers / execute jobs

        scheduler.start(); // now the scheduler will fire triggers and execute jobs
    }

    private static void initiateSpecificSchedulerWithSpecificProperties() throws SchedulerException {
        StdSchedulerFactory sf = new StdSchedulerFactory();
        Properties props = new Properties();
        props.put("quartz.scheduler.skipUpdateCheck", true);
        sf.initialize(props);

        Scheduler scheduler = sf.getScheduler();
        scheduler.start();

    }

    private static Scheduler initiateTheDefaultScheduler() throws SchedulerException {
        SchedulerFactory sf = new StdSchedulerFactory();
        return sf.getScheduler();

        //scheduler.start();
    }




}
