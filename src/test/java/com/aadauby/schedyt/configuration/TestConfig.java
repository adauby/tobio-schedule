package com.aadauby.schedyt.configuration;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {
    @Bean
    public CommandLineRunner mockCommandLineRunner() {
        return args -> {
            // Do nothing in tests
        };
    }
}
