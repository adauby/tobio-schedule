package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.exception.InvalidScheduleException;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static com.aadauby.schedyt.utils.constants.Messages.INVALID_FIRST_TRIGGER_DATE;
import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_WITHOUT_SECONDS_FORMATTER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ScheduleVerifierTest {
    private final ScheduleVerifier scheduleVerifier = new ScheduleVerifier();
    private final String dumbId = "a41d39ef10258f65eb";
    private final String dumbEvent = "dumb event";

    @Test
    void testVerifyIfFrequencyIsLessThanMinimumFrequency(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, null
                , 0, 30,dumbEvent );
        InvalidScheduleException thrown = assertThrows(InvalidScheduleException.class
                ,()-> scheduleVerifier.verify(sched));
        assertEquals(Messages.INVALID_FREQUENCY, thrown.getMessage());

    }

    @Test
    void testVerifyIfDurationIsLessThanMinimumDuration(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, null
                , 15, -1,dumbEvent );
        InvalidScheduleException thrown = assertThrows(InvalidScheduleException.class, ()-> scheduleVerifier.verify(sched));
        assertEquals(Messages.INVALID_DURATION, thrown.getMessage());
    }

    @Test
    void testVerifyIfFrequencyIsGreaterThanDuration(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, null
                , 31, 30,dumbEvent );
        InvalidScheduleException thrown = assertThrows(InvalidScheduleException.class, ()-> scheduleVerifier.verify(sched));
        assertEquals(Messages.FREQUENCY_GREATER_THAN_DURATION, thrown.getMessage());
    }
//    @Test
//    void testVerifyIfFirstDateOfTriggerIsBeforeCurrentDateAndTime(){
//        var currentDateAndTime = LocalDateTime.now().minusDays(1);
//
//        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, null
//                , 1, 30,dumbEvent);
//        sched.setTriggerFirstAt(currentDateAndTime);
//        InvalidScheduleException thrown = assertThrows(InvalidScheduleException.class, sched::getTriggerFirstAt);
//        assertEquals(INVALID_FIRST_TRIGGER_DATE, thrown.getMessage());
//
//    }

}
