package com.aadauby.schedyt.schedule.calendar.scheduler;

import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.schedule.repository.EventSchedule;
import com.aadauby.schedyt.schedule.repository.EventScheduleMapper;
import com.aadauby.schedyt.utils.constants.Keys;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static com.aadauby.schedyt.utils.constants.Messages.JOB_DETAIL_DESCRIPTION;
import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.constants.Values.DURATION_FOREVER;
import static com.aadauby.schedyt.utils.date.DateUtilities.*;
import static org.junit.jupiter.api.Assertions.*;

public class EventSchedulerTest {
    private final EventScheduler eventScheduler = new EventScheduler(null, null, null, null
            , JsonUtilities::jsonVerifier, CodeGenerator::generateStringId, new EventScheduleMapper());
    private final String serviceId = "a41d39ef10778f58f65ea";
    private final Long scheduleId = (long) 78003695;

    private final String dummyEvent = "dummy event";

    @Test
    void testBuildJobDetail() {
        var sched = new EventSchedule(scheduleId, new Application(serviceId,""), DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 60, DURATION_FOREVER, dummyEvent, CodeGenerator.generateStringId());
        var jobDetail = eventScheduler.buildJobDetail(sched);
        var jobDataMap = jobDetail.getJobDataMap();
        Assertions.assertAll("Verify job details"
                , () -> assertEquals(sched.getJobKey().concat(".")
                        .concat(serviceId), jobDetail.getKey().toString())
                , () -> assertEquals(String.format(JOB_DETAIL_DESCRIPTION,sched.getEventName(), sched.getFrequency()
                        , sched.getDuration()), jobDetail.getDescription())
                , () -> assertTrue(jobDetail.isDurable())
                , () -> assertEquals(sched.obtainDescription(), jobDataMap.get(Keys.DESC))
                , () -> assertEquals(sched.getId(), jobDataMap.get(Keys.SCHED_ID)));

    }

    @Test
    void testBuildTrigger() {
        var eventSchedule = new EventSchedule(scheduleId, new Application(serviceId,""), DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 60, DURATION_FOREVER, dummyEvent, CodeGenerator.generateStringId());
        var jobDetail = eventScheduler.buildJobDetail(eventSchedule);
        LocalDateTime eventDate = eventSchedule.getTriggerDate().withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        var startDate = ZonedDateTime.of(eventDate, ZoneId.of(eventSchedule.getZoneId()));
        var trigger = eventScheduler.buildTrigger(jobDetail, startDate
                , eventScheduler.getScheduleBuilder(eventSchedule));
        var jobDetailKey = jobDetail.getKey();
        assertEquals(jobDetailKey.toString(), trigger.getKey().toString());
        assertEquals(jobDetail.getDescription(), trigger.getDescription());
        assertEquals(Date.from(startDate.toInstant()), trigger.getStartTime());

    }


}
