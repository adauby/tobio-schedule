package com.aadauby.schedyt.schedule.utils.json;

import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.exception.EventFormatException;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
@Import({TestConfig.class})
public class JsonUtilitiesTest {

    @Test
    void testSimpleString(){
        String simpleString = "this is not a json";
        var thrown = assertThrows(EventFormatException.class
                , ()-> JsonUtilities.jsonVerifier(simpleString));
        assertEquals(Messages.INVALID_EVENT_FORMAT, thrown.getMessage());
    }

    @Test
    void testMalFormedJson(){
        String malformedJson = "{\"message\":\"malformed\"";
        var thrown = assertThrows(EventFormatException.class
                , ()-> JsonUtilities.jsonVerifier(malformedJson));
        assertEquals(Messages.INVALID_EVENT_FORMAT, thrown.getMessage());
    }

    @Test
    void testValidJsonFormat(){
        String validFormat = "{\"message\":\"valid json format\"}";
        assertTrue(JsonUtilities.jsonVerifier(validFormat));
    }

    @Test
    void testFromMapToJson(){
        Map<String, String>map = new HashMap<>();
        map.put("fieldOne","v1");
        map.put("fieldTwo", "v2");
        String jsonString = JsonUtilities.fromMapToJsonString(map);
        assertTrue(jsonString.contains("{"));
        assertTrue(jsonString.contains("}"));
        assertTrue(jsonString.contains("\"fieldTwo\":\"v2\""));
        assertTrue(jsonString.contains("\"fieldOne\":\"v1\""));
        assertTrue(jsonString.contains(","));

    }


}
