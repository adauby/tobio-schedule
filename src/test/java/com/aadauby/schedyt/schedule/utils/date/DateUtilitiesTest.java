package com.aadauby.schedyt.schedule.utils.date;

import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.date.InvalidPeriodException;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
@Import({TestConfig.class})
class DateUtilitiesTest {

    @Test
    void shouldThrowInvalidPeriodExceptionWhenStartDatePeriodIsNull(){
        LocalDate start = null;
        LocalDate end = LocalDate.now().plusDays(5);
        assertThrows(InvalidPeriodException.class,()->DateUtilities.verifyPeriod(start,end));
    }

    @Test
    void shouldThrowInvalidPeriodExceptionWhenEndDatePeriodIsNull(){
        LocalDate start = LocalDate.now();
        assertThrows(InvalidPeriodException.class,()-> DateUtilities.verifyPeriod(start,null));
    }

    @Test
    void shouldThrowInvalidPeriodExceptionWhenStartDateAndEndDateAreNull(){
        assertThrows(InvalidPeriodException.class,()-> DateUtilities.verifyPeriod(null,null));
    }

    @Test
    void shouldThrowInvalidPeriodExceptionWhenStartDateIsAfterEndDate(){
        LocalDate start = LocalDate.now().plusDays(5);
        LocalDate end = LocalDate.now();
        assertThrows(InvalidPeriodException.class,()-> DateUtilities.verifyPeriod(start,end));
    }

}
