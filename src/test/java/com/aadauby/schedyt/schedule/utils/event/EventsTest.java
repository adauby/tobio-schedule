package com.aadauby.schedyt.schedule.utils.event;

public class EventsTest {
//    @Test
//    void testParseEventIfMessageEventIsEmpty(){
//        var oEvent = Events.parseEvent("");
//        assertTrue(oEvent.isEmpty());
//    }
//
//    @Test
//    void testParseEventIfMessageEventIsNull(){
//        var oEvent = Events.parseEvent(null);
//        assertTrue(oEvent.isEmpty());
//    }
//
//    @Test
//    void testParseEventIfMessageEventIsInvalidJsonFormat(){
//        var oEvent = Events.parseEvent("{\n" +
//                " \"service\":\"serv001\",\n" +
//                " \"queueName\":\"q.serv001\",\n" +
//                " \"zoneId\":\"Africa/Abidjan\",\n" +
//                " \"eventDate\":\"2024-04-05 09:00\",\n" +
//                " \"content\":{\"field_one\":\"value_one\"}\n");
//        assertTrue(oEvent.isEmpty());
//    }
//    @Test
//    void testParseEventIfAFieldIsMissingInMessageEvent(){
//        var oEvent = Events.parseEvent("{\n" +
//                " \"service\":\"serv001\",\n" +
//                " \"queueName\":\"q.serv001\",\n" +
//                " \"zoneId\":\"Africa/Abidjan\",\n" +
//                " \"content\":\"{\\\"field_one\\\":\\\"value_one\\\"}\"\n" +
//                "}");
//        assertFalse(oEvent.isEmpty());
//        assertNull(oEvent.get().date());
//    }
//
//    @Test
//    void testParseEventIfMessageEventIsValid(){
//        var oEvent = Events.parseEvent("{\n" +
//                " \"service\":\"serv001\",\n" +
//                " \"queueName\":\"q.serv001\",\n" +
//                " \"zoneId\":\"Africa/Abidjan\",\n" +
//                " \"date\":\"2024-04-05 09:00\",\n" +
//                " \"content\":\"{\\\"field_one\\\":\\\"value_one\\\"}\"\n" +
//                "}\n");
//        assertTrue(oEvent.isPresent());
//
//        var event = oEvent.get();
//
//        assertAll("Verify event values",
//                ()->assertEquals("serv001",event.service()),
//                ()->assertEquals("q.serv001",event.queueName()),
//                ()->assertEquals(ZoneId.of("Africa/Abidjan"),ZoneId.of(event.zoneId())),
//                ()->assertEquals("{\"field_one\":\"value_one\"}",event.content()),
//                ()->assertEquals("2024-04-05 09:00",event.date()));
//
//    }
//
//    @Test
//    void testIsEventValidIfEventServiceIsNullOrEmpty(){
//        boolean isvalid =Events.isEventValid(new EventMessage("","q.service001",
//                "Africa/Abidjan","2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//        isvalid =Events.isEventValid(new EventMessage(null,"q.service001"
//                ,"Africa/Abidjan","2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//
//    }
//
//    @Test
//    void testIsEventValidIfEventZoneIdIsNullOrEmpty(){
//        boolean isvalid =Events.isEventValid(new EventMessage("serv001","q.service001",
//                "","2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//        isvalid =Events.isEventValid(new EventMessage("serv001","q.service001"
//                ,null,"2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//
//    }
//
//    @Test
//    void testIsEventValidIfEventQueueNameIsNullOrEmpty(){
//        boolean isvalid =Events.isEventValid(new EventMessage("serv001","",
//                "Africa/Abidjan","2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//        isvalid =Events.isEventValid(new EventMessage("serv001",null
//                ,"Africa/Abidjan","2024-04-05 14:00","some content"));
//        assertFalse(isvalid);
//
//    }
//
//    @Test
//    void testIsEventValidIfEventDateIsNullOrEmpty(){
//        boolean isvalid =Events.isEventValid(new EventMessage("serv001","q.service001",
//                "Africa/Abidjan","","some content"));
//        assertFalse(isvalid);
//        isvalid =Events.isEventValid(new EventMessage("serv001","q.service001"
//                ,"Africa/Abidjan",null,"some content"));
//        assertFalse(isvalid);
//    }
//
//    @Test
//    void testIsEventValidIfEventContentIsNullOrEmpty(){
//        boolean isvalid =Events.isEventValid(new EventMessage("serv001","q.service001",
//                "Africa/Abidjan","2024-04-05 14:00",null));
//        assertFalse(isvalid);
//        isvalid =Events.isEventValid(new EventMessage("serv001","q.service001"
//                ,"Africa/Abidjan","2024-04-05 14:00",""));
//        assertFalse(isvalid);
//    }
//
//    @Test
//    void testIsEventValidIfEventIsValid(){
//        boolean isvalid =Events.isEventValid(new EventMessage("serv001","q.service001",
//                "Africa/Abidjan","2024-04-05 14:00","some content"));
//        assertTrue(isvalid);
//    }
}
