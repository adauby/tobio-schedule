package com.aadauby.schedyt.schedule.utils.page;

import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.utils.page.PageBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.assertEquals;
@Import({TestConfig.class})
public class PageBuilderTest {

    @Test
    void testBuildIfPageIsLessThenZero(){
        var page= PageBuilder.build(-10,10);
        assertEquals(PageBuilder.DEFAULT_PAGE,page.getPageNumber());
        assertEquals(10,page.getPageSize());
    }
    @Test
    void testBuildIfSizeIsLessThanZero(){
        var page= PageBuilder.build(20,-100);
        assertEquals(20,page.getPageNumber());
        assertEquals(PageBuilder.DEFAULT_SIZE,page.getPageSize());
    }

    @Test
    void testBuildIfPageAndSizeAreLessThanZero(){
        var page = PageBuilder.build(-5,-50);
        assertEquals(PageBuilder.DEFAULT_PAGE,page.getPageNumber());
        assertEquals(PageBuilder.DEFAULT_SIZE,page.getPageSize());
    }

    @Test
    void testBuildIfPageAndSizeAreGreaterThanZero(){
        var page = PageBuilder.build(2,50);
        assertEquals(2,page.getPageNumber());
        assertEquals(50,page.getPageSize());
    }
}
