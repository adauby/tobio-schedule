package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.json.JsonUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.constants.Values.DURATION_FOREVER;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_FORMATTER;
import static org.junit.jupiter.api.Assertions.*;

public class EventScheduleTest {
    private final String dumbId = "a41d39ef10778f58f65eb";
    private final String dummyEvent = "dummy event";


    @Test
    void testScheduleOnceIfTriggerOnIsEmpty(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, null
                , 10, 30,dummyEvent );
        Assertions.assertFalse(sched.scheduleOnce());
    }

    @Test
    void testScheduleOnceIfTriggerOnIsNotEmpty(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:00:00", DATE_TIME_FORMATTER)
                , 10, 30,dummyEvent );
        assertTrue(sched.scheduleOnce());
    }

    @Test
    void testIsForeverIfDurationIsGreaterThanZero(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:00:00", DATE_TIME_FORMATTER)
                , 10, 30,dummyEvent );
        assertFalse(sched.isForever());
    }

    @Test
    void testIsForeverIfDurationEqualsZero(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:00:00", DATE_TIME_FORMATTER)
                , 10, DURATION_FOREVER,dummyEvent );
        assertTrue(sched.isForever());
    }

    @Test
    void testIsMonthlyIfFrequencyIsLessThanThirtyDays(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 29, DURATION_FOREVER,dummyEvent );
        assertFalse(sched.isMonthly());

    }
    @Test
    void testIsMonthlyIfFrequencyEqualsThirtyDays(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 30, DURATION_FOREVER,dummyEvent );
        assertTrue(sched.isMonthly());
    }
    @Test
    void testIsMonthlyIfFrequencyEqualsSixtyDays(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 60, DURATION_FOREVER,dummyEvent );
        assertTrue(sched.isMonthly());
    }

    @Test
    void testIsMonthlyIfFrequencyEqualsSixtySevenDays(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 67, DURATION_FOREVER,dummyEvent );
        assertFalse(sched.isMonthly());
    }

    @Test
    void testObtainTotalMonthsIfFrequencyEqualsNintyDays(){
        var sched = new EventSchedule(dumbId, DEFAULT_ZONE_ID, LocalDateTime.parse("2025-02-10 07:10:00", DATE_TIME_FORMATTER)
                , 90, DURATION_FOREVER,dummyEvent );

        assertEquals(3, sched.frequencyInMonths());
    }

    @Test
    void testBuildEventScheduleFromScheduleDTO(){
        Map<String, String>event = new HashMap<>();
        event.put("content","event data");
        var appInfo = new ApplicationInfo("4ab12056ef69","Evt name");
        var dto = new ScheduleData((long)4,"Recall", "Asia/Tokyo","2024-05-29 16:00:00 JST"
                ,"2080-06-01 16:00:00 JST",0,0,"",event, appInfo,"recallKey");

        var builtSchedule = EventSchedule.build(dto, JsonUtilities::fromMapToJsonString);
        assertEquals(4, builtSchedule.getId());
        assertEquals("Recall", builtSchedule.getEventName());
        assertEquals("Asia/Tokyo", builtSchedule.getZoneId());
        assertEquals(0, builtSchedule.getFrequency());
        assertEquals(0, builtSchedule.getDuration());
        assertEquals("recallKey", builtSchedule.getRoutingKey());
        assertEquals("4ab12056ef69", builtSchedule.getApplicationId());
        assertEquals("Evt name", builtSchedule.getApplication().getName());
        assertEquals("{\"content\":\"event data\"}", builtSchedule.getEvent());
        //assertEquals(ZonedDateTime.of(2024,5,29,16,0,0,0, ZoneId.of("Asia/Tokyo")),builtSchedule.getCreatedAt());
    }

}
