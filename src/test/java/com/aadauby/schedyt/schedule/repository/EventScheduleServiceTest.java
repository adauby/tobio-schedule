package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.application.repository.Application;
import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.List;

import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;


@ExtendWith(MockitoExtension.class)
public class EventScheduleServiceTest {

    @Mock
    EventScheduleRepository eventScheduleRepository;

    @Mock
    ApplicationRepository applicationRepository;


    EventScheduleService eventScheduleService ;

    @BeforeEach
    public void setUp(){
        eventScheduleService = new EventScheduleService(eventScheduleRepository,new EventScheduleMapper());
    }

    @Test
    void shouldReturnListOfScheduleData(){

        List<EventSchedule> pageFound = Arrays.asList(new EventSchedule((long)1, new Application("a7362be63e89ce",""), DEFAULT_ZONE_ID, null,1,30,"{\"content\":\"event data\"}", CodeGenerator.generateStringId())
                ,new EventSchedule((long)2,new Application("a7362be63e89ce",""), DEFAULT_ZONE_ID, null,1,30,"{\"content\":\"event data\"}", CodeGenerator.generateStringId())
                ,new EventSchedule((long)3,new Application("a7362be63e89ce",""), DEFAULT_ZONE_ID, null,3,30,"{\"content\":\"event data\"}", CodeGenerator.generateStringId())
                ,new EventSchedule((long)4,new Application("a7362be63e89ce",""), DEFAULT_ZONE_ID, null,5,45,"{\"content\":\"event data\"}", CodeGenerator.generateStringId())
                ,new EventSchedule((long)5,new Application("a7362be63e89ce",""), DEFAULT_ZONE_ID, null,10,360,"{\"content\":\"event data\"}", CodeGenerator.generateStringId()));
        var requestPage = PageRequest.of(2,50);
        BDDMockito.given(eventScheduleRepository.findAll(requestPage))
                .willReturn(new PageImpl<>(pageFound, requestPage, pageFound.size()));

        var page = eventScheduleService.findAll(requestPage);
        assertEquals(2,page.getNumber());
        assertEquals(50, page.getSize());
        assertEquals(5, page.getContent().size());
        assertInstanceOf(ScheduleData.class,page.getContent().get(0));

    }
}
