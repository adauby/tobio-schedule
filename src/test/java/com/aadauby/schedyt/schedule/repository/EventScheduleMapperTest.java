package com.aadauby.schedyt.schedule.repository;

import com.aadauby.schedyt.utils.constants.Values;
import com.aadauby.schedyt.utils.date.DateUtilities;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.date.DateUtilities.DATE_TIME_FORMATTER;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class EventScheduleMapperTest {

    private final EventScheduleMapper mapper = new EventScheduleMapper();

    @Test
    void testToScheduleInfoIfTriggerOnIsSet(){
        //String applicationId, String zoneId, String triggerOn
        //            , int frequency, int duration, String event,String jobKey
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, LocalDateTime.parse("2100-05-10 11:30:00", DATE_TIME_FORMATTER)
                , 0, 0, "{\"content\":\"event data\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)33);
        eventSchedule.setEventName("EventName");
        var info = mapper.toScheduleInfo(eventSchedule);
        Map<String, String>eventMap = new HashMap<>();
        eventMap.put("content","event data");
        assertEquals(33, info.scheduleId());
        assertEquals("EventName", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( Values.NOT_APPLICABLE, info.frequency());
        assertEquals( Values.NOT_APPLICABLE, info.period());
        assertEquals( "Never triggered", info.triggeredLastAt());
        assertEquals( "2100-05-10 11:30:00 GMT", info.nextTrigger());
       // assertEquals( "2100-05-10 11:30:00", info.nextTrigger());
        assertEquals( eventMap, info.event());

    }
    @Test
    void testToScheduleInfoIfTriggerOnAndEventAlreadyTriggered(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", "Asia/Tokyo", LocalDateTime.parse("2024-01-10 11:30:00",DATE_TIME_FORMATTER)
                , 0, 0, "{\"content\":\"event data\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)33);
        var triggeredLastAt = ZonedDateTime.of(2024,1,10,11,30,0,0, ZoneId.of("Asia/Tokyo"));
        eventSchedule.setTriggeredLastAt(triggeredLastAt
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        eventSchedule.setEventName("Name");
        var triggerDate = eventSchedule.getTriggerDate();
        var formattedTriggerDate = eventSchedule.getFormattedTriggerDate();
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(33, info.scheduleId());
        assertEquals("Name", info.eventName());
        assertEquals( "Asia/Tokyo", info.zoneId());
        assertEquals( Values.NOT_APPLICABLE, info.frequency());
        assertEquals( Values.NOT_APPLICABLE, info.period());
        assertEquals( "2024-01-10 11:30:00 JST", info.triggeredLastAt());
        assertEquals( "Finished", info.nextTrigger());
    }

    @Test
    void testToScheduleInfoIfScheduleIsPeriodicAndNeverTriggeredYet(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, null
                , 3, 60, "{\"content\":\"event data\"}", "b78e630fe1a94cd0");

        eventSchedule.setId((long)33);
        eventSchedule.setEventName("scheduler");
        var expectedDate = eventSchedule.getCreatedAt().plusDays(eventSchedule.getFrequency())
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(33, info.scheduleId());
        assertEquals("scheduler", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( "Every 3 day(s)", info.frequency());
        assertEquals( "60 day(s)", info.period());
        assertEquals( "Never triggered", info.triggeredLastAt());
        assertEquals( expectedDate, info.nextTrigger());
    }

    @Test
    void testToScheduleInfoIfScheduleIsPeriodicAndTriggered(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, null
                , 3, 60, "{\"content\":\"event data\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)33);
        eventSchedule.setRoutingKey("r-key");
        eventSchedule.setEventName("Special event");
        var currently = ZonedDateTime.now(ZoneId.of(DEFAULT_ZONE_ID));
        var creationTime = currently.minusDays(5);
        eventSchedule.setCreatedAt(creationTime
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var last = currently.minusDays(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
        var triggeredLastAt = currently.minusDays(2);
        eventSchedule.setTriggeredLastAt(triggeredLastAt
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var expectedDate = currently.plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(33, info.scheduleId());
        assertEquals("Special event", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( "Every 3 day(s)", info.frequency());
        assertEquals( "60 day(s)", info.period());
        assertEquals( last, info.triggeredLastAt());
        assertEquals( expectedDate, info.nextTrigger());
        assertEquals( "r-key", info.routingKey());
    }

    @Test
    void testToScheduleInfoIfScheduleIsPeriodicAndFinished(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, null
                , 1, 3, "{\"content\":\"event data\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)38);
        eventSchedule.setRoutingKey("RoutingKey");

        var currently = ZonedDateTime.now(ZoneId.of(DEFAULT_ZONE_ID));
        var creationTime = currently.minusDays(4);
        eventSchedule.setCreatedAt(creationTime
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var last = currently.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
       eventSchedule.setTriggeredLastAt(currently
               .withZoneSameInstant(ZoneId.of("UTC"))
               .toLocalDateTime());
        eventSchedule.setEventName("Special event");
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(38, info.scheduleId());
        assertEquals("Special event", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( "Everyday", info.frequency());
        assertEquals( "3 day(s)", info.period());
        assertEquals( last, info.triggeredLastAt());
        assertEquals( "Finished", info.nextTrigger());
        assertEquals( "RoutingKey", info.routingKey());
    }


    @Test
    void testToScheduleInfoIfScheduleIsForeverAndNotTriggeredYet(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, null
                , 3, Values.DURATION_FOREVER, "{\"content\":\"super pizza\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)33);
        eventSchedule.setRoutingKey("pizza");
        eventSchedule.setEventName("Pizza delivery");
        var currently = ZonedDateTime.now(ZoneId.of(DEFAULT_ZONE_ID));
        var creationTime = currently.minusDays(1);
       eventSchedule.setCreatedAt(creationTime
               .withZoneSameInstant(ZoneId.of("UTC"))
               .toLocalDateTime());

        var expectedDate = currently.plusDays(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(33, info.scheduleId());
        assertEquals("Pizza delivery", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( "Every 3 day(s)", info.frequency());
        assertEquals( "Forever", info.period());
        assertEquals( "Never triggered", info.triggeredLastAt());
        assertEquals( expectedDate, info.nextTrigger());
        assertEquals( "pizza", info.routingKey());
    }

    @Test
    void testToScheduleInfoIfScheduleIsForeverAndTriggered(){
        var eventSchedule = new EventSchedule("4ad62e0fc487", Values.DEFAULT_ZONE_ID, null
                , 3, Values.DURATION_FOREVER, "{\"content\":\"Happy Birthday\"}", "b78e630fe1a94cd0");
        eventSchedule.setId((long)33);
        eventSchedule.setRoutingKey("party.birthday");
        eventSchedule.setEventName("My friend Birthday");
        var currently = ZonedDateTime.now(ZoneId.of(DEFAULT_ZONE_ID));
        var creationTime = currently.minusDays(5);
        eventSchedule.setCreatedAt(creationTime
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var last = currently.minusDays(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
       var triggeredLast = currently.minusDays(2);
        eventSchedule.setTriggeredLastAt(triggeredLast
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime());
        var expectedDate = currently.plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
        var info = mapper.toScheduleInfo(eventSchedule);
        assertEquals(33, info.scheduleId());
        assertEquals("My friend Birthday", info.eventName());
        assertEquals( Values.DEFAULT_ZONE_ID, info.zoneId());
        assertEquals( "Every 3 day(s)", info.frequency());
        assertEquals( "Forever", info.period());
        assertEquals( last, info.triggeredLastAt());
        assertEquals( expectedDate, info.nextTrigger());
        assertEquals( "party.birthday", info.routingKey());
    }



}
