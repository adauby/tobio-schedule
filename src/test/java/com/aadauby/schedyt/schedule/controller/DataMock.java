package com.aadauby.schedyt.schedule.controller;

import com.aadauby.schedyt.application.repository.ApplicationData;
import com.aadauby.schedyt.schedule.repository.ApplicationInfo;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import com.aadauby.schedyt.schedule.repository.ScheduleInfo;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.date.DateUtilities.convertDatetimeToStringFormat;

public class DataMock {
    public static final int NO_ELEMENT = 0;
    private static final AtomicLong idGen = new AtomicLong(1);
    public static Page<ScheduleData> mockListOfSchedule(String appId, int count, Pageable page, boolean scheduleOne) {
        List<ScheduleData> listOfInfo = new ArrayList<>();
        String triggerOn = null;
        String createdAt = "2024-04-10 15:15:41 GMT";
        int duration = 60;
        int frequency = 10;
        if(scheduleOne)
            triggerOn = "2024-05-15 15:00:00 GMT";


        for(var i=0; i<count;i++) {
            Map<String,String>jsonMap = new HashMap<>();
            jsonMap.put("message","Côte d'Ivoire vs Guinnea Bissau on Saturday, January 13, 2024");
            listOfInfo.add(new ScheduleData(idGen.getAndIncrement()
                    , "AFCON 2023 " + i, DEFAULT_ZONE_ID, createdAt, triggerOn, frequency, duration, null
                    ,jsonMap, new ApplicationInfo(appId, "app test")
                    , "football.afcon"));
        }
        return PageableExecutionUtils.getPage(listOfInfo,page, listOfInfo::size);
    }

    public static Page<ScheduleInfo> generateListOfScheduleInfo(int count, Pageable page) {

        List<ScheduleInfo> listOfInfo = new ArrayList<>();
        LocalDateTime next= LocalDateTime.now().plusDays(3);
        for(var i=0; i<count;i++) {
            Map<String,String>jsonMap = new HashMap<>();
            jsonMap.put("message","Côte d'Ivoire vs Guinnea Bissau on Saturday, January 13, 2024");
            listOfInfo.add(new ScheduleInfo((long) i + 1, "MyEvent", jsonMap, "2024-03-25 15:00:00 GMT", DEFAULT_ZONE_ID, "N/A",
                    "N/A", "Not yet", convertDatetimeToStringFormat(next), "r-key"));
        }
        return PageableExecutionUtils.getPage(listOfInfo,page, listOfInfo::size);
    }

    public static Page<ApplicationData> generateListOfApplicationData(int count, Pageable page) {

        List<ApplicationData> listOfApps = new ArrayList<>();
        for(var i=0; i<count;i++) {
            Map<String,String>jsonMap = new HashMap<>();
            listOfApps.add(new ApplicationData(CodeGenerator.generateStringId(),"APP"+i,"description "+i,"exh"+1,"Africa/Abidjan","2024-10-20 16:00:00 GMT"));
        }
        return PageableExecutionUtils.getPage(listOfApps,page, listOfApps::size);
    }
}
