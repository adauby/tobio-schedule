package com.aadauby.schedyt.schedule.controller;

import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.schedule.calendar.scheduler.NextTrigger;
import com.aadauby.schedyt.schedule.calendar.scheduler.ScheduleCalendar;
import com.aadauby.schedyt.schedule.calendar.scheduler.ScheduleState;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.configuration.SchedytConfig;
import com.aadauby.schedyt.schedule.repository.*;
import com.aadauby.schedyt.utils.exception.DataNotFoundException;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import static com.aadauby.schedyt.schedule.controller.DataMock.NO_ELEMENT;
import static com.aadauby.schedyt.utils.constants.Codes.CODE_SCHEDULE_NOT_FOUND;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_SCHEDULE;
import static com.aadauby.schedyt.utils.constants.Keys.*;
import static com.aadauby.schedyt.utils.constants.Messages.dataNotFoundForId;
import static com.aadauby.schedyt.utils.constants.Paths.*;
import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@WebMvcTest({ScheduleController.class, SchedytConfig.class})
public class ScheduleControllerTest {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @MockBean
    ApplicationRepository applicationRepository;

    @MockBean
    private EventScheduleService eventScheduleService;

    @MockBean
    private BiFunction<Integer, Integer, Pageable>pageBuilder;

    @MockBean
    private SchedytScheduler eventScheduler;


    @BeforeEach
    public void setup(RestDocumentationContextProvider provider){
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                        .apply(MockMvcRestDocumentation.documentationConfiguration(provider)
                                .uris()
                                .withScheme("http")
                                .withPort(8080)
                                .withHost("localhost"))

                .build();
    }


    @Test
    void testFindByIdIfIdNotFound() throws Exception {
        long id = 8000;
        BDDMockito.when(eventScheduleService.findById(id))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_SCHEDULE, String.valueOf(id)), CODE_SCHEDULE_NOT_FOUND));
        mockMvc.perform(get(SCHEDULES+ ID, id))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("Schedule not found for id 8000")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_SCHEDULE_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(SCHEDULES.concat(ID.replace(
                                "{id}", "8000")))))
                .andDo(document("sched-by-id-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                        fieldWithPath("timestamp").description("timestamp")
                        , fieldWithPath("message").description("Error message")
                        , fieldWithPath("errorCode").description("Error code")
                        , fieldWithPath("path").description("URI")
                )));
    }

    @Test
    void testFindByIdIfIdExists() throws Exception {
        long id = 8000;
        var appInfo = new ApplicationInfo("2b8954720360de58f695ca5","Notifier");
        Map<String, String> event  = new HashMap<>();
        event.put("message","notification data");
        BDDMockito.when(eventScheduleService.findById(id))
                .thenReturn(new ScheduleData(7000L,"conference"
                        ,DEFAULT_ZONE_ID,"2024-05-05 16:00:00 GMT",null
                        ,10, 90,"2024-05-15 16:00:00 GMT",event,appInfo,"schedule"));
        mockMvc.perform(RestDocumentationRequestBuilders.get(SCHEDULES+ ID, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.scheduleId",Matchers.is(7000)))
                .andExpect(jsonPath("$.eventName", Matchers.is("conference")))
                .andExpect(jsonPath("$.zoneId", Matchers.is(DEFAULT_ZONE_ID)))
                .andExpect(jsonPath("$.createdAt", Matchers.is("2024-05-05 16:00:00 GMT")))
                .andExpect(jsonPath("$.triggerDate", Matchers.nullValue()))
                .andExpect(jsonPath("$.triggerFirstAt", Matchers.is("2024-05-15 16:00:00 GMT")))
                .andExpect(jsonPath("$.routingKey", Matchers.is("schedule")))
                .andExpect(jsonPath("$.frequency", Matchers.is(10)))
                .andExpect(jsonPath("$.duration", Matchers.is(90)))
                .andExpect(jsonPath("$.application.id", Matchers.is("2b8954720360de58f695ca5")))
                .andExpect(jsonPath("$.application.name", Matchers.is("Notifier")))
                .andExpect(jsonPath("$.event", Matchers.is(event)))
                .andDo(document("sched-by-id-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Schedule id"))))
                .andDo(document("sched-by-id-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                        fieldWithPath("scheduleId").description("Schedule event identifier")
                        ,fieldWithPath("eventName").description("Event name")
                        ,fieldWithPath("zoneId").description("Zone identifier")
                        ,fieldWithPath("createdAt").description("Creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        ,fieldWithPath("triggerDate").description("Trigger date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        ,fieldWithPath("routingKey").description("Routing key")
                        ,fieldWithPath("frequency").description("Interval of time between two occurrences set.")
                        ,fieldWithPath("duration").description("period of time the event will be triggered set")
                        ,fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        ,fieldWithPath("event.*").description("Event data")
                        ,fieldWithPath("application.id").description("Application identifier")
                        ,fieldWithPath("application.name").description("Application name")
                        ,fieldWithPath("routingKey").description("Routing key")
                        //,fieldWithPath("cancelled").description("Indicate if schedule is cancelled or not")
                )));
    }
    @Test
    void shouldReturnNoContent() throws Exception{
        int size= 10, page=0;
        var pageRequest = PageRequest.of(page,size);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        BDDMockito.given(eventScheduleService.findAll(pageRequest))
                .willReturn(DataMock.mockListOfSchedule(CodeGenerator.generateStringId()
                        , NO_ELEMENT,pageRequest, true));

        mockMvc.perform(get(SCHEDULES)
                .param("page","0")
                .param("size","10")
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

    }

    @Test
    void shouldReturnTwoElements() throws Exception{
        int size= 10, page=0;
        var pageRequest = PageRequest.of(page,size);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        BDDMockito.given(eventScheduleService.findAll(pageRequest))
                .willReturn(DataMock.mockListOfSchedule(CodeGenerator.generateStringId(),2,pageRequest, true));

        mockMvc.perform(get(SCHEDULES)
                        .param("page","0")
                        .param("size","10")
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(header().string(H_PAGE,"0"))
                .andExpect(header().string(H_HAS_NEXT_PAGE,"false"))
                .andExpect(header().string(H_TOTAL_PAGES,"1"))
                .andExpect(header().string(H_TOTAL_ELEMENTS, "2"))
                .andDo(document("get-all-schedules",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        queryParameters(
                        parameterWithName("page").description("Page number. Page number start from 0 (default value). " +
                                "if a negative value is passed the default value will be considered")
                        ,parameterWithName("size").description("Number of elements. The default value is 10 and the max value is 50. " +
                                "If the the value passed is less then zero the default value will be considered. If the value passed is greater " +
                                "than the max value then the max value will be considered  "))))
                .andDo(document("get-all-schedules",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseHeaders(
                        headerWithName(H_PAGE).description("current page")
                        ,headerWithName(H_HAS_NEXT_PAGE).description("Indicate the existence of a next page or not")
                        ,headerWithName(H_TOTAL_PAGES).description("The total amount of pages")
                        ,headerWithName(H_TOTAL_ELEMENTS).description("the total amount of elements"))))
                .andDo(document("get-all-schedules",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                        fieldWithPath("[].scheduleId").description("Schedule event identifier")
                        ,fieldWithPath("[].eventName").description("Event name")
                        ,fieldWithPath("[].zoneId").description("Time zone identifier. Examples: 'Africa/Abidjan', 'Asia/Tokyo'.")
                        ,fieldWithPath("[].createdAt").description("Creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        ,fieldWithPath("[].triggerDate").description("Trigger date")
                        ,fieldWithPath("[].frequency").description("Interval of time between two occurrences set.")
                        ,fieldWithPath("[].duration").description("period of time the event will be triggered set")
                        ,fieldWithPath("[].triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        ,fieldWithPath("[].event.*").description("Event data")
                        ,fieldWithPath("[].application.id").description("Application identifier")
                        ,fieldWithPath("[].application.name").description("Application name")
                        ,fieldWithPath("[].routingKey").description("Routing key")
                        //, fieldWithPath("[].cancelled").description("Indicate if schedule is cancelled or not")

                )));
    }

    @Test
    void shouldReturnOk() throws Exception{
        long scheduleId = 200;
        Map<String, String> event = new HashMap<>();
        event.put("message","this is a new event");
        var calendar = new ScheduleCalendar("event name",scheduleId, DEFAULT_ZONE_ID
                ,event, ScheduleState.ACTIVE,
                List.of(new NextTrigger("2030-01-02 10:00:00 GMT"), new NextTrigger("2031-01-02 10:00:00 GMT")));
        BDDMockito.when(eventScheduler.getCalendar(scheduleId))
                .thenReturn(calendar);
        var path = SCHEDULES.concat(NEXT_SCHEDULE_OCCURRENCES);

        mockMvc.perform(RestDocumentationRequestBuilders.get(path,scheduleId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.eventName",Matchers.is("event name")))
                .andExpect(jsonPath("$.scheduleId", Matchers.is(200)))
                .andExpect(jsonPath("$.zoneId", Matchers.is(DEFAULT_ZONE_ID)))
                .andExpect(jsonPath("$.event.message", Matchers.is("this is a new event")))
                .andExpect(jsonPath("$.state", Matchers.is(ScheduleState.ACTIVE.toString())))
                .andExpect(jsonPath("$.upcoming", Matchers.hasSize(2)))

                .andDo(document("get-calendar-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                        parameterWithName("id").description("schedule id"))))
                .andDo(document("get-calendar-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                        fieldWithPath("eventName").description("event name"),
                        fieldWithPath("scheduleId").description("Schedule Identifier"),
                        fieldWithPath("zoneId").description("Zone Identifier"),
                        fieldWithPath("event.*").description("Event data"),
                        fieldWithPath("state").description("Schedule state: ACTIVE, TERMINATED"),
                        fieldWithPath("upcoming.[].*").description("List of upcoming trigger dates.\nThe date format is yyyy-MM-dd HH:mm:ss z.")
                )));
    }
    @Test
    void shouldReturnScheduleNotFound() throws Exception{
        long wrongScheduleId = 207;
        Map<String, String> event = new HashMap<>();
        event.put("message","this is a new event");

        BDDMockito.when(eventScheduler.getCalendar(wrongScheduleId))
                .thenThrow(new DataNotFoundException(dataNotFoundForId("Schedule"
                        , String.valueOf(wrongScheduleId)), CODE_SCHEDULE_NOT_FOUND));
        var path = SCHEDULES.concat(NEXT_SCHEDULE_OCCURRENCES);

        mockMvc.perform(RestDocumentationRequestBuilders.get(path,wrongScheduleId))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("Schedule not found for id 207")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_SCHEDULE_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(path.replace("{id}", "207"))))

                .andDo(document("get-calendar-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                        parameterWithName("id").description("schedule id"))))
                .andDo(document("get-calendar-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                        fieldWithPath("timestamp").description("timestamp")
                        , fieldWithPath("message").description("Error message")
                        , fieldWithPath("errorCode").description("Error code")
                        , fieldWithPath("path").description("URI")
                )));
    }





}