package com.aadauby.schedyt.schedule.controller;

import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.configuration.SchedytConfig;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.schedule.repository.ApplicationInfo;
import com.aadauby.schedyt.schedule.repository.EventScheduleService;
import com.aadauby.schedyt.schedule.repository.ScheduleData;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static com.aadauby.schedyt.utils.constants.Paths.ID;
import static com.aadauby.schedyt.utils.constants.Paths.SCHEDULES;
import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@WebMvcTest({ScheduleController.class, SchedytConfig.class})
@TestPropertySource(properties = "spring.profiles.active=default,test-prod-http-basic")
@AutoConfigureMockMvc(addFilters = true)
public class ScheduleControllerHttpBasicTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @MockBean
    ApplicationRepository applicationRepository;

    @MockBean
    private EventScheduleService eventScheduleService;

    @MockBean
    private BiFunction<Integer, Integer, Pageable> pageBuilder;

    @MockBean
    private SchedytScheduler eventScheduler;


    @BeforeEach
    public void setup(RestDocumentationContextProvider provider){
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(MockMvcRestDocumentation.documentationConfiguration(provider)
                        .uris()
                        .withScheme("http")
                        .withPort(8080)
                        .withHost("localhost"))

                .build();
    }

    @Test
    void shouldReturnOneSchedule() throws Exception {
        long id = 8000;
        var appInfo = new ApplicationInfo("2b8954720360de58f695ca5","Notifier");
        Map<String, String> event  = new HashMap<>();
        event.put("message","notification data");
        BDDMockito.when(eventScheduleService.findById(id))
                .thenReturn(new ScheduleData(7000L,"conference"
                        ,DEFAULT_ZONE_ID,"2024-05-29 16:00:00",""
                        ,10, 90,"",event,appInfo,"schedule"));
        mockMvc.perform(RestDocumentationRequestBuilders.get(SCHEDULES+ ID, id)
                        .header("Authorization","Basic QXBwTmFtZTpwYXNzd29yZA=="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.scheduleId", Matchers.is(7000)))
                .andExpect(jsonPath("$.eventName", Matchers.is("conference")))
                .andExpect(jsonPath("$.zoneId", Matchers.is(DEFAULT_ZONE_ID)))
                .andExpect(jsonPath("$.createdAt", Matchers.is("2024-05-29 16:00:00")))
                .andExpect(jsonPath("$.triggerDate", Matchers.is("")))
                .andExpect(jsonPath("$.routingKey", Matchers.is("schedule")))
                .andExpect(jsonPath("$.frequency", Matchers.is(10)))
                .andExpect(jsonPath("$.duration", Matchers.is(90)))
                .andExpect(jsonPath("$.application.id", Matchers.is("2b8954720360de58f695ca5")))
                .andExpect(jsonPath("$.application.name", Matchers.is("Notifier")))
                .andExpect(jsonPath("$.event", Matchers.is(event)))
                .andDo(document("http-basic-sched-by-id-ok"
                        ,requestHeaders(
                                headerWithName("Authorization").description("Basic authorization")
                        ),
                        pathParameters(
                                parameterWithName("id").description("Schedule id"))))
                .andDo(document("http-basic-sched-by-id-ok", responseFields(
                        fieldWithPath("scheduleId").description("Schedule event identifier")
                        ,fieldWithPath("eventName").description("Event name")
                        ,fieldWithPath("zoneId").description("Zone identifier")
                        ,fieldWithPath("createdAt").description("Creation date")
                        ,fieldWithPath("triggerDate").description("Trigger date")
                        ,fieldWithPath("routingKey").description("Routing key")
                        ,fieldWithPath("frequency").description("Interval of time between two occurrences set.")
                        ,fieldWithPath("duration").description("period of time the event will be triggered set")
                        ,fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time. If this value is not set, the trigger date will be calculated as follows: current date and time + frequency")
                        ,fieldWithPath("event.*").description("Event data")
                        ,fieldWithPath("application.id").description("Application identifier")
                        ,fieldWithPath("application.name").description("Application name")
                        ,fieldWithPath("routingKey").description("Routing key")
                        //,fieldWithPath("cancelled").description("Indicate if schedule is cancelled or not")
                )));
    }
}
