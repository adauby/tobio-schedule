package com.aadauby.schedyt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
public class ControllerTest<T> {

    @Autowired
    protected WebApplicationContext context;
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;
    protected JacksonTester<T> requestJsonWriter;


    @BeforeEach
    public void setup(RestDocumentationContextProvider provider){
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(MockMvcRestDocumentation.documentationConfiguration(provider)
                        .uris()
                        .withScheme("http")
                        .withPort(8080)
                        .withHost("localhost"))

                .build();

        JacksonTester.initFields(this, objectMapper);
    }

}
