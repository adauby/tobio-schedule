package com.aadauby.schedyt.application.api.key.controller;


import com.aadauby.schedyt.api.key.controller.ApiKeyController;
import com.aadauby.schedyt.api.key.repository.ApiKey;
import com.aadauby.schedyt.api.key.repository.ApiKeyService;
import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.utils.exception.AuthException;
import com.aadauby.schedyt.utils.exception.DataNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static com.aadauby.schedyt.utils.constants.Codes.CODE_APPLICATION_NOT_FOUND;
import static com.aadauby.schedyt.utils.constants.Codes.CODE_INVALID_SECRET;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_APP;
import static com.aadauby.schedyt.utils.constants.Messages.INVALID_SECRET;
import static com.aadauby.schedyt.utils.constants.Messages.dataNotFoundForId;
import static com.aadauby.schedyt.utils.constants.Paths.API_KEYS;
import static com.aadauby.schedyt.utils.constants.Paths.APPLICATIONS;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@Profile({"default","prod-api-key"})
@WebMvcTest({ApiKeyController.class})
@TestPropertySource(properties = "spring.profiles.active=default,test-prod-api-key")
@AutoConfigureMockMvc(addFilters = false)
@AutoConfigureRestDocs
public class ApiKeyControllerTest {


//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private ApiKeyService apiKeyService;
//
//    @Autowired
//    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ApiKeyService apiKeyService;

    @MockBean
    private ApplicationRepository applicationRepository;

    private final String APP_SECRET_HEADER = "X-APP-SECRET";
    private static final String TEST_APP_ID = "b11ec3fdf26348dabd6ce768a86f03f1";
    private static final String TEST_APP_NAME = "TestApp";
    private static final String TEST_SECRET = "secret123";
    private static final String TEST_API_KEY = "0d904f03-1f91-4b85-86e2-0f82251ad2b0";


    @Test
    void shouldGetApplicationKey() throws Exception {
        ApiKey apiKey = new ApiKey(TEST_APP_ID, TEST_APP_NAME, TEST_API_KEY, "2024-01-01T00:00:00");
        when(apiKeyService.findApiKey(TEST_APP_ID, TEST_SECRET)).thenReturn(apiKey);

        mockMvc.perform(get(APPLICATIONS.concat(API_KEYS), TEST_APP_ID)
                        .header(APP_SECRET_HEADER, TEST_SECRET)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.applicationId").value(TEST_APP_ID))
                .andExpect(jsonPath("$.applicationName").value(TEST_APP_NAME))
                .andExpect(jsonPath("$.apiKey").value(TEST_API_KEY))
                .andExpect(jsonPath("$.updatedAt").value("2024-01-01T00:00:00"))
                .andDo(document("get-application-key",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application ID")
                        ),
                        requestHeaders(
                                headerWithName("X-APP-SECRET").description("Application secret")
                        ),
                        responseFields(
                                fieldWithPath("applicationId").description("The application ID")
                                , fieldWithPath("applicationName").description("The application name")
                                , fieldWithPath("apiKey").description("The API key for the application")
                                , fieldWithPath("updatedAt").description("The date the key has been generated")

                        )));

    }

    @Test
    void shouldGenerateNewApiKey() throws Exception {
        ApiKey apiKey = new ApiKey(TEST_APP_ID, "TestApp", TEST_API_KEY, "2024-01-01T00:00:00");
        when(apiKeyService.generateApiKey(TEST_APP_ID, TEST_SECRET)).thenReturn(apiKey);

        mockMvc.perform(post(APPLICATIONS.concat(API_KEYS), TEST_APP_ID)
                        .header(APP_SECRET_HEADER, TEST_SECRET)
                        .param("id", TEST_APP_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.applicationId").value(TEST_APP_ID))
                .andExpect(jsonPath("$.applicationName").value(TEST_APP_NAME))
                .andExpect(jsonPath("$.apiKey").value(TEST_API_KEY))
                .andExpect(jsonPath("$.updatedAt").value("2024-01-01T00:00:00"))
                .andDo(document("post-application-key",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application ID")
                        ),
                        requestHeaders(
                                headerWithName("X-APP-SECRET").description("Application secret")
                        ),
                        responseFields(
                                fieldWithPath("applicationId").description("The application ID")
                                , fieldWithPath("applicationName").description("The application name")
                                , fieldWithPath("apiKey").description("The API key for the application")
                                , fieldWithPath("updatedAt").description("The date the key has been generated")

                        )));


    }

    @Test
    void shouldReturn404WhenApplicationNotFound() throws Exception {
        // Given
        when(apiKeyService.findApiKey(TEST_APP_ID, TEST_SECRET))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, TEST_APP_ID)
                        , CODE_APPLICATION_NOT_FOUND));

        // When & Then
        mockMvc.perform(get(APPLICATIONS.concat(API_KEYS), TEST_APP_ID)
                        .header(APP_SECRET_HEADER, TEST_SECRET))
                .andExpect(status().isNotFound())
                .andDo(document("get-key-application-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application ID")
                        ),
                        requestHeaders(
                                headerWithName("X-APP-SECRET").description("Application secret")
                        ),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("cause message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("path called"))));
    }

    @Test
    void shouldReturn401WhenSecretIsInvalid() throws Exception {
        // Given
        when(apiKeyService.findApiKey(TEST_APP_ID, TEST_SECRET))
                .thenThrow(new AuthException(INVALID_SECRET, CODE_INVALID_SECRET));

        // When & Then
        mockMvc.perform(get(APPLICATIONS.concat(API_KEYS), TEST_APP_ID)
                        .header(APP_SECRET_HEADER, TEST_SECRET))
                .andExpect(status().isUnauthorized())
                .andDo(document("get-key-unauthorized",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application ID")
                        ),
                        requestHeaders(
                                headerWithName("X-APP-SECRET").description("Application secret")
                        ),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("cause message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("path called"))));
    }

}
