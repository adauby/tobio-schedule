package com.aadauby.schedyt.application.controller;

import com.aadauby.schedyt.ControllerTest;
import com.aadauby.schedyt.api.key.repository.ApiKeyService;
import com.aadauby.schedyt.application.repository.*;
import com.aadauby.schedyt.configuration.SchedytConfig;
import com.aadauby.schedyt.configuration.TestConfig;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.schedule.controller.DataMock;
import com.aadauby.schedyt.schedule.controller.ScheduleRequest;
import com.aadauby.schedyt.schedule.repository.*;
import com.aadauby.schedyt.utils.constants.Constants;
import com.aadauby.schedyt.utils.constants.Messages;
import com.aadauby.schedyt.utils.constants.Paths;
import com.aadauby.schedyt.utils.date.DateUtilities;
import com.aadauby.schedyt.utils.exception.*;
import com.aadauby.schedyt.utils.generator.CodeGenerator;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static com.aadauby.schedyt.schedule.controller.DataMock.NO_ELEMENT;
import static com.aadauby.schedyt.utils.constants.Codes.*;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_APP;
import static com.aadauby.schedyt.utils.constants.Constants.ENTITY_SCHEDULE;
import static com.aadauby.schedyt.utils.constants.Keys.*;
import static com.aadauby.schedyt.utils.constants.Messages.*;
import static com.aadauby.schedyt.utils.constants.Paths.*;
import static com.aadauby.schedyt.utils.constants.Values.DEFAULT_ZONE_ID;
import static com.aadauby.schedyt.utils.date.DateUtilities.*;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest({ApplicationController.class, SchedytConfig.class, TestConfig.class})
public class ApplicationControllerTest extends ControllerTest<ApplicationRequest> {

    @MockBean // Replaces the CommandLineRunner bean with a mock
    private CommandLineRunner commandLineRunner;
    @MockBean
    ApplicationRepository applicationRepository;

    @MockBean
    EventScheduleRepository eventScheduleRepository;


    @MockBean
    ApplicationService appService;
    @MockBean
    ApiKeyService apiKeyService;

    @MockBean
    EventScheduleService scheduleService;

    @MockBean
    SchedytScheduler eventScheduler;

    protected JacksonTester<ScheduleRequest> scheduleRequestJsonWriter;
    protected JacksonTester<RegisterRequest> registerRequestJsonWriter;
    protected JacksonTester<PasswordRequest> passwordRequestJsonWriter;

    @BeforeEach
    void setSecondWriter() {
        JacksonTester.initFields(this, objectMapper);
    }

    @MockBean
    private BiFunction<Integer, Integer, Pageable> pageBuilder;

    @Test
    void testRegisterIfRequestBodyIsValid() throws Exception {
        var appli = new ApplicationData("app001", "new service", "service description", "message.direct", "Africa/Abidjan", "2024-12-01 12:00:00 GMT");
        var appRequest = new RegisterRequest("new service", "service description", "message.direct", "MySecret", "Africa/Abidjan");
        var request = registerRequestJsonWriter.write(appRequest).getJson();
        BDDMockito.given(appService.register(ArgumentMatchers.any(Application.class))).willReturn(appli);

        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(request))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.is("app001")))
                .andExpect(jsonPath("$.name", Matchers.is("new service")))
                .andExpect(jsonPath("description", Matchers.is("service description")))
                .andExpect(jsonPath("$.exchangeName", Matchers.is("message.direct")))
                .andExpect(jsonPath("$.zoneId", Matchers.is("Africa/Abidjan")))
                .andExpect(jsonPath("$.createdAt", Matchers.is("2024-12-01 12:00:00 GMT")))
                .andDo(document("application-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestFields(
                                fieldWithPath("name").description("Service name")
                                , fieldWithPath("description").description("Service description. This is optional").optional()
                                , fieldWithPath("exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                        "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                        "events to the correct queues based on routing keys.")
                                , fieldWithPath("secret").description("Application password")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone")
                        )))
                .andDo(document("application-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("id").description("Service identifier")
                                , fieldWithPath("name").description("Service name")
                                , fieldWithPath("description").description("Service description. This is optional")
                                , fieldWithPath("exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                        "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                        "events to the correct queues based on routing keys.")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone")
                                , fieldWithPath("createdAt").description("Application creation date. The format is yyyy-MM-dd HH:mm:ss z")
                        )));
    }

    @Test
    void testRegisterIfNameIsEmpty() throws Exception {
        var appRequest = new RegisterRequest("", "", "message.direct", "MySecret","Africa/Abidjan");
        var request = registerRequestJsonWriter.write(appRequest).getJson();


        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("name must not be blank")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FIELD)))
                .andExpect(jsonPath("$.path", Matchers.is(APPLICATIONS)))
                .andDo(document("application-bad-request",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("cause message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("path called")
                        )));
    }

    @Test
    void testRegisterIfExchangeNameIsEmpty() throws Exception {
        var appRequest = new RegisterRequest("app name", "", "", "MySecret"
                ,"Africa/Abidjan");
        var request = registerRequestJsonWriter.write(appRequest).getJson();


        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("exchangeName must not be blank")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FIELD)))
                .andExpect(jsonPath("$.path", Matchers.is(APPLICATIONS)));
    }

    @Test
    void testGetSchedulesByApplicationIfEverythingOk() throws Exception {
        var appId = "785a56213be4f08236de";
        int size = 10, page = 0;
        var pageRequest = PageRequest.of(page, size);
        Page<ScheduleInfo> schedules = DataMock.generateListOfScheduleInfo(2, pageRequest);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        BDDMockito.given(appService.findAllSchedules(appId, pageRequest))
                .willReturn(schedules);
        mockMvc.perform(get(APPLICATIONS + APPLICATION_SCHEDULES, appId)
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(header().string(H_PAGE, "0"))
                .andExpect(header().string(H_HAS_NEXT_PAGE, "false"))
                .andExpect(header().string(H_TOTAL_PAGES, "1"))
                .andExpect(header().string(H_TOTAL_ELEMENTS, "2"))
                .andDo(document("sched-by-appli-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("application_id").description("Application identifier"))))
                .andDo(document("sched-by-appli-ok", queryParameters(
                        parameterWithName("page").description("Page number. Page number start from 0 (default value). " +
                                "if a negative value is passed the default value will be considered")
                        , parameterWithName("size").description("Number of elements. The default value is 10 and the max value is 50. " +
                                "If the the value passed is less then zero the default value will be considered. If the value passed is greater " +
                                "than the max value then the max value will be considered  "))))
                .andDo(document("sched-by-appli-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("applicationId").description("Application identifier")
                                , fieldWithPath("schedules.[].scheduleId").description("Schedule event identifier")
                                , fieldWithPath("schedules.[].eventName").description("Event name")
                                , fieldWithPath("schedules.[].event.*").description("Event data (from application owner)")
                                , fieldWithPath("schedules.[].createdAt").description("Creation date. The format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("schedules.[].zoneId").description("Unique identifier for a time zone. Examples: 'Africa/Abidjan', 'Asia/Tokyo'.")
                                , fieldWithPath("schedules.[].frequency").description("Interval of time between two occurrences set.")
                                , fieldWithPath("schedules.[].period").description("period of time the event will be triggered set")
                                , fieldWithPath("schedules.[].triggeredLastAt").description("Last time the event has been triggered. The format yyyy-MM-dd hh:mm:ss z")
                                , fieldWithPath("schedules.[].nextTrigger").description("Next time the event will be trigger. The format yyyy-MM-dd hh:mm:ss z")
                                , fieldWithPath("schedules.[].routingKey").description("Routing key. It determines the target queue(s) for messages in a direct exchange.")

                        )));
    }

    @Test
    void testGetSchedulesByApplicationIfApplicationIdIsNotFound() throws Exception {
        var appId = "785a56213be4f08236de";
        int size = 10, page = 0;
        var pageRequest = PageRequest.of(page, size);
        Page<ScheduleInfo> schedules = DataMock.generateListOfScheduleInfo(10, pageRequest);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        BDDMockito.given(appService.findAllSchedules(appId, pageRequest))
                .willThrow(new DataNotFoundException(dataNotFoundForId("Application", appId), CODE_APPLICATION_NOT_FOUND));
        mockMvc.perform(get(APPLICATIONS + APPLICATION_SCHEDULES, appId)
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId("Application", appId))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers.is(APPLICATIONS + APPLICATION_SCHEDULES.replace(Paths.APPLICATION_ID, "/" + appId))))
                .andDo(document("sched-by-appli-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("application_id").description("Application identifier"))))
                .andDo(document("sched-by-appli-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("Timestamp")
                                , fieldWithPath("message").description("Cause of exception")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));


    }

    @Test
    void testGetSchedulesByApplicationIfApplicationHasNoSchedule() throws Exception {
        var appId = "785a56213be4f08236de";
        int size = 10, page = 0;
        var pageRequest = PageRequest.of(page, size);
        Page<ScheduleInfo> schedules = DataMock.generateListOfScheduleInfo(NO_ELEMENT, pageRequest);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        BDDMockito.given(appService.findAllSchedules(appId, pageRequest))
                .willReturn(schedules);
        mockMvc.perform(get(APPLICATIONS + APPLICATION_SCHEDULES, appId)
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isNoContent());

    }

    @Test
    void testScheduleEventIfRequestIsValid() throws Exception {
        String appId = "45a3efa5236cd635";
        var appli = new Application(appId, "Food delivery", "no desc", "message.topic");
        Map<String, String> eventJson = new HashMap<>();
        eventJson.put("message", "The conference will occurred on 2045-04-25 at 16:00:00");
        ScheduleRequest request = new ScheduleRequest("Conference", "Africa/Abidjan", "2045-04-25 16:00:00"
                , 10, 30, null, eventJson, "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        var sched = new EventSchedule((long) 60125897, appli
                , "Africa/Abidjan", LocalDateTime.parse("2024-04-25 16:00:00",DATE_TIME_FORMATTER), 0, 0, "{\"content\":\"The conference will occurred on 2045-04-25 at 16:00:00\"}"
                , CodeGenerator.generateStringId());
        sched.setEventName(request.eventName());
        var appData = new ApplicationData(appId, "Food delivery", "no desc"
                , "message.topic", "Africa/Abidjan","2024-10-20 at 16:00:00");
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("content", "The conference will occurred on 2045-04-25 at 16:00:00");
        when(appService.findById(appId))
                .thenReturn(appData);

        BDDMockito.given(eventScheduler.schedule(ArgumentMatchers.any(EventSchedule.class)))
                .willReturn(sched);

        when(appService.toScheduleData(ArgumentMatchers.any(EventSchedule.class)))
                .thenReturn(new ScheduleData(sched.getId(), sched.getEventName(), sched.getZoneId(), "2024-03-25 16:00:00 GMT"
                        , "2024-04-25 16:00:00 GMT", 0, 0, null
                        , jsonMap, new ApplicationInfo(appId, "Food delivery")
                        , "r-key"
                ));

        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), appId)
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody).accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.scheduleId", Matchers.is(60125897)))
                .andExpect(jsonPath("$.eventName", Matchers.is("Conference")))
                .andExpect(jsonPath("$.zoneId", Matchers.is("Africa/Abidjan")))
                .andExpect(jsonPath("$.triggerDate", Matchers.is("2024-04-25 16:00:00 GMT")))
                .andExpect(jsonPath("$.frequency", Matchers.is(0)))
                .andExpect(jsonPath("$.duration", Matchers.is(0)))
                .andExpect(jsonPath("$.event", Matchers.is(jsonMap)))
                .andExpect(jsonPath("$.application.id", Matchers.is("45a3efa5236cd635")))
                .andExpect(jsonPath("$.application.name", Matchers.is("Food delivery")))
                //.andExpect(jsonPath("$.cancelled", Matchers.is(false)))
                .andDo(document("event-schedule-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestFields(
                                fieldWithPath("eventName").description("Event name")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone").optional()
                                , fieldWithPath("triggerDate").description("The date and time when the event is scheduled to trigger. If this value is set, the 'frequency', 'duration' and 'triggerFirstAt' parameters will be ignored.\nThe format is yyyy-MM-dd HH:mm:ss")
                                , fieldWithPath("frequency").description("Interval of day between two occurrences")
                                , fieldWithPath("duration").description("Period of time the event will be triggered (in days):\n" +
                                        "\n" +
                                        "    * The maximum allowed value is 360 days.\n" +
                                        "    * If set to 0, the event will repeat indefinitely.")
//                        ,fieldWithPath("event").description("Event data in JSON format")
                                , fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time. If this value is not set, the first trigger date will be calculated as follows: current date and time + frequency.\nThe format is yyyy-MM-dd HH:mm")
                                , fieldWithPath("event.*").description("Event Fields from application")
                                , fieldWithPath("routingKey").description("Routing key. It determines the target queue(s) for messages in a direct exchange.")
                        )))
                .andDo(document("event-schedule-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("scheduleId").description("schedule event identifier")
                                , fieldWithPath("eventName").description("event name")
                                , fieldWithPath("zoneId").description("Time zone identifier")
                                , fieldWithPath("createdAt").description("Creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("triggerDate").description("The date and time when the event is scheduled to trigger.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("frequency").description("Interval of day between two occurrences ")
                                , fieldWithPath("duration").description("Period of time the event will be triggered (in days):\n" +
                                        "\n" +
                                        "    * The maximum allowed value is 360 days.\n" +
                                        "    * If set to 0, the event will repeat indefinitely.")
                                , fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("event.*").description("Event Fields from application")
                                , fieldWithPath("application.id").description("Application identifier")
                                , fieldWithPath("application.name").description("Application name")
                                , fieldWithPath("routingKey").description("Routing key. It determines the target queue(s) for messages in a direct exchange.")
                                // , fieldWithPath("cancelled").description("Indicate if schedule is cancelled or not")
                        )));
    }

    @Test
    void testScheduleEventIfRequestObjectHasEmptyZoneId() throws Exception {
        var appId = "45a3efa5236cd635";
        Map<String, String> eventJson = new HashMap<>();
        eventJson.put("message", "The conference will occurred on 2045-04-25 at 16:00:00");
        ScheduleRequest request = new ScheduleRequest("", DEFAULT_ZONE_ID
                , "2024-04-25 16:00:00", 10, 30
                , null, eventJson, "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), appId)
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("eventName must not be blank")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FIELD)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd635")))))
                .andDo(document("event-schedule-bad-req",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));

    }

    @Test
    void testScheduleEventIfRequestObjectHasEmptyEvent() throws Exception {

        ScheduleRequest request = new ScheduleRequest("Africa/Abidjan", "2024-04-25 16:00"
                , "", 10, 30
                , null, new HashMap<>(), "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), "45a3efa5236cd635")
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is("event must not be empty")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FIELD)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd635")))));

    }

    @Test
    void testScheduleEventIfRequestObjectHasNullEventAndNullZoneId() throws Exception {
        //var appId = "45a3efa5236cd635";

        ScheduleRequest request = new ScheduleRequest("Google I/O", null
                , "2024-04-25 16:00:00", 10, 30, null, null, "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), "45a3efa5236cd635")
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.containsString("must not be")))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FIELD)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd635")))));

    }

    @Test
    void testScheduleEventIfRequestObjectHasInvalidFrequency() throws Exception {
        var appId = "45a3efa5236cd635";
        Map<String, String> eventJson = new HashMap<>();
        eventJson.put("message", "The conference will occurred on 2045-04-25 at 16:00:00");
        ScheduleRequest request = new ScheduleRequest("Google I/O", DEFAULT_ZONE_ID
                , "", -10, 30, null, eventJson, "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        BDDMockito.given(appService.findById(appId))
                .willReturn(new ApplicationData(appId, "Food delivery", "no desc"
                        , "message.direct", "Africa/Abidjan","2024-10-20 at 16:00:00"));
        BDDMockito.given(eventScheduler.schedule(ArgumentMatchers.any(EventSchedule.class)))
                .willThrow(new InvalidScheduleException(Messages.INVALID_FREQUENCY, CODE_INVALID_FREQUENCY));
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), appId)
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(Messages.INVALID_FREQUENCY)))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_FREQUENCY)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd635")))));

    }

    @Test
    void testScheduleEventIfTriggerOnIsInvalidFormatDate() throws Exception {
        var appId = "45a3efa5236cd635";
        Map<String, String> eventJson = new HashMap<>();
        eventJson.put("message", "The conference will occurred on 2045-04-25 at 16:00:00");
        ScheduleRequest request = new ScheduleRequest("Google I/O", DEFAULT_ZONE_ID
                , "2024-04-25", 10, 30, null, eventJson, "r-key");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        BDDMockito.given(appService.findById(appId))
                .willReturn(new ApplicationData(appId, "Food delivery", "no desc"
                        , "message.direct",DEFAULT_ZONE_ID, "2024-10-20 at 16:00:00 GMT"));

        BDDMockito.given(eventScheduler.schedule(ArgumentMatchers.any(EventSchedule.class)))
                .willThrow(new InvalidDataException(Messages.INVALID_DATETIME_FORMAT, CODE_INVALID_DATE_FORMAT));
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), appId)
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(Messages.INVALID_DATETIME_FORMAT)))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_DATE_FORMAT)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd635")))));


    }

    @Test
    void testScheduleApplicationIdDoesntExist() throws Exception {
        var appId = "45a3efa5236cd636";
        Map<String, String> eventJson = new HashMap<>();
        eventJson.put("message", "You will be delivered at 19 o'clock");
        ScheduleRequest request = new ScheduleRequest("Conference", DEFAULT_ZONE_ID
                , "2099-04-29 14:30:00", 10, 30, null, eventJson, "food.delivery");
        String requestBody = scheduleRequestJsonWriter.write(request).getJson();
        BDDMockito.given(appService.findById(appId))
                .willReturn(new ApplicationData(appId, "Food delivery", "no desc"
                        , "message.topic", "Africa/Abidjan","2024-10-20 at 16:00:00 GMT"));
        BDDMockito.given(eventScheduler.schedule(ArgumentMatchers.any(EventSchedule.class)))
                .willThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, appId), CODE_APPLICATION_NOT_FOUND));
        mockMvc.perform(MockMvcRequestBuilders.post(APPLICATIONS.concat(APPLICATION_SCHEDULES), appId)
                        .accept(APPLICATION_JSON_VALUE)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestBody))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, appId))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULES.replace(
                                APPLICATION_ID, "/45a3efa5236cd636")))));


    }


    @Test
    void testUpdateScheduleEventIfApplicationIdNotFound() throws Exception {
        String appId = "62ef3658c4d36ab784102fc6";
        long scheduleId = 10800;
        Map<String, String> event = new HashMap<>();
        event.put("content", "event data");
        var requestBody = new ScheduleRequest("Name", DEFAULT_ZONE_ID, ""
                , 5, 30, null, event, "routing.key");
        var requestPayload = scheduleRequestJsonWriter.write(requestBody).getJson();
        when(appService.findById(appId))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, appId), CODE_APPLICATION_NOT_FOUND));
        mockMvc.perform(put(APPLICATIONS.concat(APPLICATION_SCHEDULE), appId, scheduleId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, appId))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULE.replace(
                                        APPLICATION_ID, "/62ef3658c4d36ab784102fc6")
                                .replace("{schedule_id}", String.valueOf(scheduleId))))))
                .andDo(document("event-schedule-update-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));
    }

    @Test
    void testUpdateScheduleEventIfScheduleNotFound() throws Exception {
        String appId = "62ef3658c4d36ab784102fc7";
        var appli = new ApplicationData(appId, "MyApp", "any desc"
                , "app.key", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        long scheduleId = 10801;
        Map<String, String> event = new HashMap<>();
        event.put("content", "event data");
        var requestBody = new ScheduleRequest("Name", DEFAULT_ZONE_ID, ""
                , 5, 30, null, event, "routing.key");
        var requestPayload = scheduleRequestJsonWriter.write(requestBody).getJson();
        when(appService.findById(appId))
                .thenReturn(appli);
        when(appService.findApplicationSchedule(appId, scheduleId))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_SCHEDULE, String.valueOf(scheduleId))
                        , CODE_SCHEDULE_NOT_FOUND));
        mockMvc.perform(put(APPLICATIONS.concat(APPLICATION_SCHEDULE), appId, scheduleId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_SCHEDULE, String.valueOf(scheduleId)))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_SCHEDULE_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULE.replace(
                                        APPLICATION_ID, "/62ef3658c4d36ab784102fc7")
                                .replace("{schedule_id}", String.valueOf(scheduleId))))));
    }

    @Test
    void testUpdateScheduleEventIfEventNameIsUsedByAnotherSchedule() throws Exception {
        String appId = "62ef3658c4d36ab784102fc7";
        var appliDto = new ApplicationData(appId, "MyApp", "any desc"
                , "app.key","Abidjan/Africa", "2024-10-20 16:00:00 GMT");
        var appli = new Application(appId, appliDto.name(), appliDto.description(), appliDto.exchangeName(), new ArrayList<>());
        long scheduleId = 10801;
        Map<String, String> event = new HashMap<>();
        event.put("content", "event data");
        var requestBody = new ScheduleRequest("already used name", DEFAULT_ZONE_ID, ""
                , 5, 30, null, event, "routing.key");
        var requestPayload = scheduleRequestJsonWriter.write(requestBody).getJson();
        var scheduleToUpdate = new EventSchedule(appli, DEFAULT_ZONE_ID, "old name",null, 5, 30, "{\"content\":\"event data\"}");
        scheduleToUpdate.setId(scheduleId);
        when(appService.findById(appId))
                .thenReturn(appliDto);
        when(appService.findApplicationSchedule(appId, scheduleId))
                .thenReturn(scheduleToUpdate);
        scheduleToUpdate.setEventName(requestBody.eventName());
        when(scheduleService.verifyEventNameIsNotUsed(scheduleToUpdate.getEventName(), scheduleId))
                .thenThrow(new DuplicatedDataException(duplicatedData("already used name", ENTITY_SCHEDULE), CODE_DUPLICATE_SCHEDULE));
        mockMvc.perform(put(APPLICATIONS.concat(APPLICATION_SCHEDULE), appId, scheduleId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(duplicatedData("already used name", ENTITY_SCHEDULE))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_DUPLICATE_SCHEDULE)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULE.replace(
                                        APPLICATION_ID, "/62ef3658c4d36ab784102fc7")
                                .replace("{schedule_id}", String.valueOf(scheduleId))))));
    }

    @Test
    void testUpdateScheduleEventIfRequestIsValid() throws Exception {
        String appId = "62ef3658c4ae36ab784102fc77";
        var appliDto = new ApplicationData(appId, "MyApp", "any desc"
                , "app.key", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        var appli = new Application(appId, appliDto.name(), appliDto.description(), appliDto.exchangeName(), new ArrayList<>());
        long scheduleId = 220011;
        Map<String, String> event = new HashMap<>();
        event.put("content", "new event data");
        var requestBody = new ScheduleRequest("new name", "Africa/Dakar", ""
                , 7, 40, "2025-01-01 11:00", event, "event");
        var requestPayload = scheduleRequestJsonWriter.write(requestBody).getJson();
        var scheduleToUpdate = new EventSchedule(appli, DEFAULT_ZONE_ID, "old name", null, 5, 30, "{\"content\":\"old data\"}");
        scheduleToUpdate.setEventName(requestBody.eventName());
        scheduleToUpdate.setRoutingKey("old.routing.key");
        scheduleToUpdate.setId(scheduleId);
        scheduleToUpdate.setCreatedAt(ZonedDateTime.parse("2024-10-21 08:00:00 GMT", DATE_TIME_FORMATTER_WITH_ZONE)
                .withZoneSameInstant(ZoneId.of(DEFAULT_ZONE_ID))
                .toLocalDateTime());
        //new EventSchedule(appli, "Africa/Dakar", "new name", null, 7, 40, "{\"content\":\"new event data\"}");

        var updated = new ScheduleData(scheduleId, "new name", "Africa/Dakar"
                , scheduleToUpdate.getFormattedCreatedAt()
                , "", 7, 40, "2025-01-01 11:00:00 GMT", event, new ApplicationInfo(appliDto.id(), appliDto.name())
                , "event");

        when(appService.findById(appId))
                .thenReturn(appliDto);
        when(appService.findApplicationSchedule(appId, scheduleId))
                .thenReturn(scheduleToUpdate);
        BDDMockito.given(eventScheduler.updateSchedule(scheduleToUpdate)).willReturn(updated);

//        when(eventScheduler.updateSchedule(scheduleToUpdate))
//                .thenReturn(updated);
        mockMvc.perform(put(APPLICATIONS.concat(APPLICATION_SCHEDULE), appId, scheduleId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.scheduleId", Matchers.is(220011)))
                .andExpect(jsonPath("$.eventName", Matchers.is("new name")))
                .andExpect(jsonPath("$.zoneId", Matchers.is("Africa/Dakar")))
                .andExpect(jsonPath("$.triggerDate", Matchers.is("")))
                .andExpect(jsonPath("$.triggerFirstAt", Matchers.is("2025-01-01 11:00:00 GMT")))
                .andExpect(jsonPath("$.frequency", Matchers.is(7)))
                .andExpect(jsonPath("$.duration", Matchers.is(40)))
                .andExpect(jsonPath("$.event", Matchers.is(event)))
                .andExpect(jsonPath("$.application.id", Matchers.is(appId)))
                .andExpect(jsonPath("$.application.name", Matchers.is("MyApp")))
                .andExpect(jsonPath("$.routingKey", Matchers.is("event")))
                // .andExpect(jsonPath("$.cancelled", Matchers.is(false)))
                .andDo(document("event-schedule-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestFields(
                                fieldWithPath("eventName").description("Event name")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone")
                                , fieldWithPath("triggerDate").description("The date and time when the event is scheduled to trigger. If this value is set, the 'frequency', 'duration' and 'triggerFirstAt' parameters will be ignored.\nThe format is yyyy-MM-dd HH:mm:ss")
                                , fieldWithPath("frequency").description("Interval of day between two occurrences")
                                , fieldWithPath("duration").description("Period of time the event will be triggered (in days):\n" +
                                        "\n" +
                                        "    * The maximum allowed value is 360 days.\n" +
                                        "    * If set to 0, the event will repeat indefinitely.")
                                , fieldWithPath("event.*").description("Event Fields from application")
                                , fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time. If this value is not set, the first trigger date will be calculated as follows: current date and time + frequency. The format is yyyy-MM-dd HH:mm")
                                , fieldWithPath("routingKey").description("Routing key. It determines the target queue(s) for messages in a direct exchange.")
                        )))
                .andDo(document("event-schedule-created",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("scheduleId").description("schedule event identifier")
                                , fieldWithPath("eventName").description("event name")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone")
                                , fieldWithPath("createdAt").description("Creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("triggerDate").description("The date and time when the event is scheduled to trigger.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("frequency").description("Interval of day between two occurrences ")
                                , fieldWithPath("duration").description("Period of time the event will be triggered (in days):\n" +
                                        "\n" +
                                        "    * The maximum allowed value is 360 days.\n" +
                                        "    * If set to 0, the event will repeat indefinitely.")

                                , fieldWithPath("triggerFirstAt").description("The date and time when the event is scheduled to trigger for the first time.\nThe format is yyyy-MM-dd HH:mm:ss z")
                                , fieldWithPath("event.*").description("Event Fields from application")
                                , fieldWithPath("application.id").description("Application identifier")
                                , fieldWithPath("application.name").description("Application name")
                                , fieldWithPath("routingKey").description("Routing key. It determines the target queue(s) for messages in a direct exchange.")
                                //, fieldWithPath("cancelled").description("Indicate if schedule is cancelled or not")
                        )));
    }

    @Test
    void testUpdatePassword() throws Exception {
        String id = "a458e52f36a02136";
        var appData = new ApplicationData(id, "App", "description"
                , "message.direct", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        var requestBody = new PasswordRequest("currentSecret", "app_new_secret");
        var requestPayload = passwordRequestJsonWriter.write(requestBody).getJson();
        when(appService.updatePassword(id, "currentSecret", "app_new_secret"))
                .thenReturn(appData);

        mockMvc.perform(put(APPLICATIONS.concat(APP_PASSWORD), id)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestPayload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(id)))
                .andExpect(jsonPath("$.name", Matchers.is("App")))
                .andExpect(jsonPath("$.description", Matchers.is("description")))
                .andExpect(jsonPath("$.createdAt", Matchers.is("2024-10-20 16:00:00 GMT")))
                .andDo(document("change-password-ok", pathParameters(
                        //String id, String name, String description,String exchangeName
                        parameterWithName("id").description("Application identifier"))))
                .andDo(document("change-password-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestFields(
                                fieldWithPath("currentSecret").description("Current secret (secret to be changed)"),
                                fieldWithPath("newSecret").description("New secret")
                        )));
    }

    @Test
    void testUpdatePasswordIfCurrentPasswordIsIncorrect() throws Exception {
        String id = "a458e52f36a02136";

        var requestBody = new PasswordRequest( "wrong_currentSecret", "app_new_secret");
        var requestPayload = passwordRequestJsonWriter.write(requestBody).getJson();

        when(appService.updatePassword(id, "wrong_currentSecret", "app_new_secret"))
                .thenThrow(new AuthException(INVALID_SECRET, CODE_INVALID_SECRET));

        mockMvc.perform(put(APPLICATIONS.concat(APP_PASSWORD), id)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestPayload))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(INVALID_SECRET)))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_INVALID_SECRET)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APP_PASSWORD.replace(
                                "{id}", id)))))
                .andDo(document("change-password-401", responseFields(
                        fieldWithPath("timestamp").description("timestamp")
                        , fieldWithPath("message").description("Error message")
                        , fieldWithPath("errorCode").description("Error code")
                        , fieldWithPath("path").description("URI")
                )));
    }

    @Test
    void testUpdatePasswordIfApplicationNotFound() throws Exception {
        String wrongId = "a458e52f36a02136";

        var requestBody = new PasswordRequest( "currentSecret", "app_new_secret");
        var requestPayload = passwordRequestJsonWriter.write(requestBody).getJson();

        when(appService.updatePassword(wrongId, "currentSecret", "app_new_secret"))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, wrongId)
                        , CODE_APPLICATION_NOT_FOUND));

        mockMvc.perform(put(APPLICATIONS.concat(APP_PASSWORD), wrongId)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(requestPayload))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, String.valueOf(wrongId)))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APP_PASSWORD.replace(
                                "{id}", wrongId)))))
                .andDo(document("change-password-404", responseFields(
                        fieldWithPath("timestamp").description("timestamp")
                        , fieldWithPath("message").description("Error message")
                        , fieldWithPath("errorCode").description("Error code")
                        , fieldWithPath("path").description("URI")
                )));
    }

    @Test
    void testFindAnApplication() throws Exception {
        String id = "a458e52f36a02136";
        var appData = new ApplicationData(id, "found service", "description"
                , "message.direct", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        when(appService.findById(id)).thenReturn(appData);

        mockMvc.perform(get(APPLICATIONS + ID, id)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(id)))
                .andExpect(jsonPath("$.name", Matchers.is("found service")))
                .andExpect(jsonPath("$.description", Matchers.is("description")))
                .andExpect(jsonPath("$.exchangeName", Matchers.is("message.direct")))
                .andExpect(jsonPath("$.zoneId", Matchers.is("Africa/Abidjan")))
                .andDo(document("find-app-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(parameterWithName("id").description("Application identifier"))))
                .andDo(document("find-app-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("id").description("Application identifier"),
                                fieldWithPath("name").description("Application name"),
                                fieldWithPath("description").description("Application description"),
                                fieldWithPath("exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                        "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                        "events to the correct queues based on routing keys."),
                                fieldWithPath("zoneId").description("Unique identifier for a time zone"),
                                fieldWithPath("createdAt").description("Application creation date. The format is yyyy-MM-dd HH:mm:ss z")
                        )));
    }

    @Test
    void testApplicationNotFound() throws Exception {
        String wrongId = "a458e52f36a02136f7";

        when(appService.findById(wrongId)).thenThrow(
                new DataNotFoundException(dataNotFoundForId(ENTITY_APP, wrongId), CODE_APPLICATION_NOT_FOUND));

        mockMvc.perform(get(APPLICATIONS + ID, wrongId)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, String.valueOf(wrongId)))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(ID.replace(
                                "{id}", wrongId)))))
                .andDo(document("find-app-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));
    }

    @Test
    void testFindFirstPageOfAllApplications() throws Exception {
        int page = 0;
        int size = 5;
        Pageable pageRequest = PageRequest.of(page, size);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        when(appService.findAll(pageRequest))
                .thenReturn(DataMock.generateListOfApplicationData(2, pageRequest));

        mockMvc.perform(get(APPLICATIONS)
                        .param("size", "5")
                        .param("page", "0")
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(header().string(H_PAGE, "0"))
                .andExpect(header().string(H_HAS_NEXT_PAGE, "false"))
                .andExpect(header().string(H_TOTAL_PAGES, "1"))
                .andExpect(header().string(H_TOTAL_ELEMENTS, "2"))
                .andDo(document("all-apps-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        queryParameters(
                                parameterWithName("page").description("Page number. Page number start from 0 (default value). " +
                                        "if a negative value is passed the default value will be considered")
                                , parameterWithName("size").description("Number of elements. The default value is 10 and the max value is 50. " +
                                        "If the the value passed is less then zero the default value will be considered. If the value passed is greater " +
                                        "than the max value then the max value will be considered  "))))
                .andDo(document("all-apps-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseHeaders(
                                headerWithName(H_PAGE).description("current page"),
                                headerWithName(H_HAS_NEXT_PAGE).description("Next page"),
                                headerWithName(H_TOTAL_PAGES).description("Total number of pages"),
                                headerWithName(H_TOTAL_ELEMENTS).description("total number of elements")
                        )))
                .andDo(document("all-apps-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("[].id").description("Application identifier"),
                                fieldWithPath("[].name").description("Application name"),
                                fieldWithPath("[].description").description("Application description"),
                                fieldWithPath("[].exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                        "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                        "events to the correct queues based on routing keys."),
                                fieldWithPath("[].zoneId").description("Unique identifier for a time zone"),
                                fieldWithPath("[].createdAt").description("Application creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                        )));
    }

    @Test
    void testFindAllApplicationsWhenNoOneExists() throws Exception {
        int page = 0;
        int size = 10;
        Pageable pageRequest = PageRequest.of(page, size);
        BDDMockito.given(pageBuilder.apply(page, size))
                .willReturn(PageRequest.of(page, size));
        when(appService.findAll(pageRequest))
                .thenReturn(DataMock.generateListOfApplicationData(0, pageRequest));
        mockMvc.perform(get(APPLICATIONS)
                        .param("size", "10")
                        .param("page", "0")
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void testCancelScheduleIfItExists() throws Exception {
        Long scheduleId = 10023L;
        String appId = "a458e52f36a02136";
        Map<String, String> jsonMap = new HashMap<>();
        jsonMap.put("content", "event data");

        var appli = new Application(appId, "appName", "Desc Desc", "app.exch", new ArrayList<>());
        var concernedApp = new ApplicationData(appId, "appName", "Desc Desc"
                , "app.exch","Africa/Abidjan", "2024-10-20 16:00:00 GMT");
        var sched = new EventSchedule(appli, DEFAULT_ZONE_ID, "event Del", null, 1, 0, "{\"content\":\"event data\"}");
        sched.setId(scheduleId);
        var scheduleData = new ScheduleData(scheduleId, sched.getEventName(), DEFAULT_ZONE_ID
                , "2024-09-06 16:00:00", "2024-09-07 16:00:00", 1, 0
                , "", jsonMap, new ApplicationInfo(appId, "Food delivery")
                , "r-key");
        when(appService.findById(appId)).thenReturn(concernedApp);
        when(appService.findApplicationSchedule(appId, scheduleId))
                .thenReturn(sched);
        String schedulePath = APPLICATIONS.concat(APPLICATION_SCHEDULE);
        mockMvc.perform(RestDocumentationRequestBuilders.delete(schedulePath, appId, scheduleId))
                .andExpect(status().isNoContent())
                .andDo(document("schedule-cancellation-success",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("application_id").description("Application identifier")
                                , parameterWithName("schedule_id").description("Schedule identifier"))));
    }

    @Test
    void testCancelScheduleIfApplicationIdDoesntExist() throws Exception {
        Long scheduleId = 10023L;
        String wrongId = "a458e52f36a02136";

        when(appService.findById(wrongId))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, wrongId), CODE_APPLICATION_NOT_FOUND));

        String schedulePath = APPLICATIONS.concat(APPLICATION_SCHEDULE);
        mockMvc.perform(RestDocumentationRequestBuilders.delete(schedulePath, wrongId, scheduleId))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, wrongId))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULE.replace(
                                "{application_id}", wrongId).replace("{schedule_id}"
                                , String.valueOf(scheduleId))))))
                .andDo(document("schedule-cancellation-app-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(parameterWithName("application_id").description("Application identifier")
                                , parameterWithName("schedule_id").description("Schedule identifier"))))
                .andDo(document("schedule-cancellation-app-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));
    }

    @Test
    void testCancelScheduleIfScheduleIdDoesntExist() throws Exception {
        Long wrongScheduleId = 10023L;
        String appId = "a458e52f36a02136";
        var appli = new Application(appId, "appName", "Desc Desc", "app.exch", new ArrayList<>());
        var concernedApp = new ApplicationData(appId, "appName", "Desc Desc"
                , "app.exch", "Africa/Abidjan","2024-10-20  16:00:00 GMT");

        when(appService.findById(appId)).thenReturn(concernedApp);
        when(appService.findApplicationSchedule(appId, wrongScheduleId))
                .thenThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_SCHEDULE
                        , String.valueOf(wrongScheduleId)), CODE_SCHEDULE_NOT_FOUND));

        String schedulePath = APPLICATIONS.concat(APPLICATION_SCHEDULE);
        mockMvc.perform(RestDocumentationRequestBuilders.delete(schedulePath, appId, wrongScheduleId))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_SCHEDULE, String.valueOf(wrongScheduleId)))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_SCHEDULE_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(APPLICATION_SCHEDULE.replace(
                                "{application_id}", appId).replace("{schedule_id}"
                                , String.valueOf(wrongScheduleId))))))
                .andDo(document("schedule-cancellation-sched-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(parameterWithName("application_id").description("Application identifier")
                                , parameterWithName("schedule_id").description("Schedule identifier"))))
                .andDo(document("schedule-cancellation-sched-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI")
                        )));
    }

    @Test
    void testUpdateApplicationIfApplicationExists() throws Exception {
        String appId = "62ef3658c4d36ab784102fc7";
        var appliUpdated = new ApplicationData(appId, "newName", "desc update"
                , "app.key", "Asia/Tokyo","2024-10-20 16:00:00 JST");
        var appRequest = new ApplicationRequest("newName", "desc update"
                , "app.key","Asia/Tokyo");
        var requestPayload = requestJsonWriter.write(appRequest).getJson();
        BDDMockito.given(appService.update(ArgumentMatchers.any(Application.class)))
                .willReturn(appliUpdated);
        mockMvc.perform(put(APPLICATIONS.concat(ID), appId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(appId)))
                .andExpect(jsonPath("$.name", Matchers.is("newName")))
                .andExpect(jsonPath("$.description", Matchers.is("desc update")))
                .andExpect(jsonPath("$.exchangeName", Matchers.is("app.key")))
                .andExpect(jsonPath("$.zoneId", Matchers.is("Asia/Tokyo")))
                .andDo(document("appli-update-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application identifier"))))
                .andDo(document("appli-update-ok", requestFields(
                        fieldWithPath("name").description("Service name")
                        , fieldWithPath("description").description("Application description. This is optional").optional()
                        , fieldWithPath("exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                "events to the correct queues based on routing keys.")
                        , fieldWithPath("zoneId").description("Unique identifier for a time zone")

                )))
                .andDo(document("appli-update-ok",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("id").description("Application identifier")
                                , fieldWithPath("name").description("Application name")
                                , fieldWithPath("description").description("Application description")
                                , fieldWithPath("exchangeName").description("RabbitMQ exchange name. An exchange is a RabbitMQ component responsible for routing messages to queues." +
                                        "Your application uses this name (example: `schedyt.direct`) exchange to route scheduled " +
                                        "events to the correct queues based on routing keys.")
                                , fieldWithPath("zoneId").description("Unique identifier for a time zone")
                                , fieldWithPath("createdAt").description("Application creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")

                        )));

    }

    @Test
    void testUpdateApplicationIfApplicationNotFound() throws Exception {
        String wrongAppId = "62ef3658c4d36ab784102fc7";
        var appliUpdated = new ApplicationData(wrongAppId, "newName", "desc update"
                , "app.key", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        var appRequest = new ApplicationRequest("newName", "desc update"
                , "app.key","Africa/Abidjan");
        var requestPayload = requestJsonWriter.write(appRequest).getJson();
        BDDMockito.given(appService.update(ArgumentMatchers.any(Application.class)))
                .willThrow(new DataNotFoundException(dataNotFoundForId(ENTITY_APP, wrongAppId)
                        , CODE_APPLICATION_NOT_FOUND));
        mockMvc.perform(put(APPLICATIONS.concat(ID), wrongAppId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(dataNotFoundForId(ENTITY_APP, wrongAppId))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_APPLICATION_NOT_FOUND)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(ID.replace("{id}", wrongAppId)))))
                .andDo(document("appli-update-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application identifier"))))
                .andDo(document("appli-update-not-found",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI"))));

    }

    @Test
    void testUpdateApplicationIfNewNameAlreadyExists() throws Exception {
        String appId = "62ef3658c4d36ab784102fc7";
        var appliUpdated = new ApplicationData(appId, "wrongName", "desc update"
                , "app.key", "Africa/Abidjan","2024-10-20 16:00:00 GMT");
        var appRequest = new ApplicationRequest("wrongName", "desc update"
                , "app.key","Africa/Abidjan");
        var requestPayload = requestJsonWriter.write(appRequest).getJson();
        BDDMockito.given(appService.update(ArgumentMatchers.any(Application.class)))
                .willThrow(new DuplicatedDataException(duplicatedData("wrongName"
                        , Constants.ENTITY_SCHEDULE), CODE_DUPLICATE_APPLICATION_NAME));
        mockMvc.perform(put(APPLICATIONS.concat(ID), appId)
                        .content(requestPayload)
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", Matchers.matchesPattern("(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}).*")))
                .andExpect(jsonPath("$.message", Matchers.is(duplicatedData("wrongName", Constants.ENTITY_SCHEDULE))))
                .andExpect(jsonPath("$.errorCode", Matchers.is(CODE_DUPLICATE_APPLICATION_NAME)))
                .andExpect(jsonPath("$.path", Matchers
                        .is(APPLICATIONS.concat(ID.replace("{id}", appId)))))
                .andDo(document("appli-update-duplicate",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("id").description("Application identifier"))))
                .andDo(document("appli-update-duplicate",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(
                                fieldWithPath("timestamp").description("timestamp")
                                , fieldWithPath("message").description("Error message")
                                , fieldWithPath("errorCode").description("Error code")
                                , fieldWithPath("path").description("URI"))));

    }


}
