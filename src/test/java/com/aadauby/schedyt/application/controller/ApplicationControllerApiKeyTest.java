package com.aadauby.schedyt.application.controller;

import com.aadauby.schedyt.ControllerTest;
import com.aadauby.schedyt.api.key.repository.ApiKeyService;
import com.aadauby.schedyt.application.repository.ApplicationData;
import com.aadauby.schedyt.application.repository.ApplicationRepository;
import com.aadauby.schedyt.application.repository.ApplicationService;
import com.aadauby.schedyt.configuration.SchedytConfig;
import com.aadauby.schedyt.schedule.calendar.scheduler.SchedytScheduler;
import com.aadauby.schedyt.schedule.controller.ScheduleRequest;
import com.aadauby.schedyt.schedule.repository.EventScheduleRepository;
import com.aadauby.schedyt.schedule.repository.EventScheduleService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.TestPropertySource;

import java.util.function.BiFunction;

import static com.aadauby.schedyt.utils.constants.Paths.APPLICATIONS;
import static com.aadauby.schedyt.utils.constants.Paths.ID;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({ApplicationController.class, SchedytConfig.class})

@TestPropertySource(properties = "spring.profiles.active=default,test-prod-api-key")
@AutoConfigureMockMvc(addFilters = true)
public class ApplicationControllerApiKeyTest extends ControllerTest<RegisterRequest> {
    private static final String API_KEY_HEADER = "X-API-KEY";
    private static final String TEST_API_KEY = "0d904f03-1f91-4b85-86e2-0f82251ad2b0";

    @MockBean
    ApplicationRepository applicationRepository;

    @MockBean
    EventScheduleRepository eventScheduleRepository;


    @MockBean
    ApplicationService appService;
    @MockBean
    ApiKeyService apiKeyService;

    @MockBean
    EventScheduleService scheduleService;

    @MockBean
    SchedytScheduler eventScheduler;

    protected JacksonTester<ScheduleRequest> scheduleRequestJsonWriter;

    @BeforeEach
    void setSecondWriter() {
        JacksonTester.initFields(this, objectMapper);
    }

    @MockBean
    private BiFunction<Integer, Integer, Pageable> pageBuilder;

    @Test
    void shouldReturnApplication() throws Exception {
        String id = "a458e52f36a02136";
        var appData = new ApplicationData(id, "found service", "description"
                , "message.direct","Africa/Abidjan", "2024-10-20 16:00:00 GMT");
        BDDMockito.when(appService.findById(id)).thenReturn(appData);

        mockMvc.perform(RestDocumentationRequestBuilders.get(APPLICATIONS + ID, id)
                        .contentType(APPLICATION_JSON_VALUE)
                        .header(API_KEY_HEADER, TEST_API_KEY))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(id)))
                .andExpect(jsonPath("$.name", Matchers.is("found service")))
                .andExpect(jsonPath("$.description", Matchers.is("description")))
                .andExpect(jsonPath("$.exchangeName", Matchers.is("message.direct")))
                .andDo(document("api-key-find-app-ok",
                        requestHeaders(
                                headerWithName("X-API-KEY").description("Api key header")
                        )
                        , pathParameters(
                                parameterWithName("id").description("Application identifier")
                        )))
                .andDo(document("api-key-find-app-ok", responseFields(
                        fieldWithPath("id").description("Application identifier"),
                        fieldWithPath("name").description("Application name"),
                        fieldWithPath("description").description("Application description"),
                        fieldWithPath("exchangeName").description("Rabbit exchange name"),
                        fieldWithPath("zoneId").description("Unique identifier for a time zone"),
                        fieldWithPath("createdAt").description("Application creation date.\nThe format is yyyy-MM-dd HH:mm:ss z")
                )));
    }

}
