# SCHEDYT
Schedyt is a simple service for scheduling events.
## Overview
Schedyt is a simple service for scheduling events. An event can be a task, a note, a message, or other types of activities.
Events can be scheduled:
* **Once**: For a single occurrence.
* **At a fixed rate**: Repeated at regular intervals.
* **Over a period**: Within a defined time range.
* **Indefinitely**: For ongoing, never-ending repetitions.

Additionally, Schedyt supports RabbitMQ Direct Exchange, enabling precise message routing to specific queues based on routing keys. This feature ensures efficient communication and integration with other services.

![architecture](images/architecture.png)
## How it works
![how it works](images/how_it_works.png)

## Build with
* Spring boot (version 2.7)
* Maven  (version 3.8)
* Docker
* Quartz

## Get the project
### Requirements
* java >=17
* Git
* MariaDB >=10.4 
* RabbitMQ
* Docker

### Clone from GitLab
Execute the command below:
```shell
git clone https://gitlab.com/adauby/schedyt.git
```

### Running all tests

```shell
$ ./mvnw verify
```
#### Running unit tests only

```shell
$ ./mvnw test
```

#### Running integration tests only

```shell
$ ./mvnw verify -Dskip.ut=true
```
### Build package
```shell
$ ./mvnw package
```
### Run in docker environment
```shell
$ docker compose -f dev-compose-api-key.yaml up -d
```

### Get APIs Documentation
APIs are documented with '[Spring REST Docs](https://docs.spring.io/spring-restdocs/docs/2.0.8.RELEASE/reference/html5/)'.
The API documentation is generated when [building package](#build-package) and located at ```target/generated-docs/``` . The path is ```target/generated-docs/api-docs.html```
Once deployed locally APIs documentation is available at http://localhost:8080/api-docs.html

## Environment variables

| VARIABLE            | DESCRIPTION                                               |
|---------------------|-----------------------------------------------------------|
| DB_HOST             | Database host                                             |
| SCHEDYT_DB_USER     | Database username                                         |
| SCHEDYT_DB_PASSWORD | Database user password                                    |
| BROKER_HOST         | Message broker host                                       |
| BROKER_USER         | Message broker username                                   |
| BROKER_PASSWORD     | Message broker user password                              |
| BROKER_PORT         | Message broker port                                       |
| ADMIN_SECRET        | Application admin password. The default value is Pas5w.rd |

## Application Profiles
Schedyt supports multiple Spring profiles to cater to different use cases and environments. Each profile configures the application differently, enabling flexible deployment and feature activation.
###  Available Profiles
**1. quick-start**:

- **Purpose**: For quick setup and local testing.
- **Features**:
    - No security.
- **Usage:**
    - Ideal for development or demo purposes.

**2. prod-http-basic:**

* **Purpose**: Production environment with HTTP Basic Authentication.
* **Features**:
    * Secured with HTTP Basic Authentication.
    * Focused on simplicity for environments that require basic security.

**3. prod-api-key**:

* **Purpose**: Production environment with API key-based security.
* **Features**:
    * Secured with API keys.
    * Supports client-specific authentication.
    * Ideal for applications integrating with external services or APIs.

### How to Activate Profiles
Set JAVA_OPTS environment<br>
Example: Using HTTP Basic authentication in compose configuration file

```yml
...
  schedyt-app:
    container_name: schedyt-app
    image: 4dauby/schedyt:latest

    restart: on-failure
    environment:
      JAVA_OPTS: -Dspring.profiles.active=prod-api-key
...
```
### Configuration Files
Each profile has its own configuration file, which overrides the default settings in `application.yml`:

| **Profile**       | **Configuration File**            |
| ----------------- | --------------------------------- |
| `quick-start`     | `application-quick-start.yml`     |
| `prod-http-basic` | `application-prod-http-basic.yml` |
| `prod-api-key`    | `application-prod-api-key.yml`    |
## Authors
* Assi A. Mason

# License
This project is licensed under Apache License Version 2.0. See [here](LICENSE).