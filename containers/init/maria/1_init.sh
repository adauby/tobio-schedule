mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "DELETE FROM mysql.user WHERE User=''"
mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"

mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "CREATE DATABASE schedyt_db"
mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "CREATE USER '$SCHEDY_DB_USER'@'localhost' IDENTIFIED BY 'k@geSecre7'"
mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "CREATE USER '$SCHEDY_DB_USER'@'%' IDENTIFIED BY '$SCHEDY_DB_PASSWORD'"

mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "GRANT ALL PRIVILEGES ON schedyt_db.* TO '$SCHEDY_DB_USER'@'localhost'"
mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "GRANT ALL PRIVILEGES ON schedyt_db.* TO '$SCHEDY_DB_USER'@'%'"
mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "FLUSH PRIVILEGES"