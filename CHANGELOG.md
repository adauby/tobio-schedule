# CHANGELOG – Schedyt
## [1.0.2] - (2025-02-17)
### Bug fixes:
- Handle Exception caused by invalid Zone Id

## [1.0.1] - (2025-02-14)
### Features:
- Event scheduling with support for various types (one-time, fixed-rate, periodic, indefinite).
- Time zone support for precise scheduling.
- Secure API access with API Key or HTTP Basic Authentication.
- Role management with **CLIENT** and **ADMIN** roles.
- Default **ADMIN** application created at first launch (profile: prod-api-key or prod-http-basic) with customizable password via `ADMIN_SECRET`.
- Dockerized deployment with **MariaDB** and **RabbitMQ**.
- Integrated RabbitMQ Direct Exchange for efficient message routing.
- Automatic provisioning of default `schedyt` admin application with initial password.

### Security:
- API secured using **API keys** and **HTTP Basic Authentication**.
- Role-based access control with ADMIN and CLIENT roles.
- Environment-based admin password management.
