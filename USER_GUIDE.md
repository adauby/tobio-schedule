# Overview
Schedyt is a simple service for scheduling events. An event can be a task, a note, a message, or other types of activities.
Events can be scheduled:
* **Once**: For a single occurrence.
* **At a fixed rate**: Repeated at regular intervals.
* **Over a period**: Within a defined time range.
* **Indefinitely**: For ongoing, never-ending repetitions.

Additionally, Schedyt supports RabbitMQ Direct Exchange, enabling precise message routing to specific queues based on routing keys. This feature ensures efficient communication and integration with other services.
![architecture](https://gitlab.com/adauby/schedyt/-/raw/main/images/architecture_x1.png?ref_type=heads&inline=false)

# How it works
![architecture](https://gitlab.com/adauby/schedyt/-/raw/main/images/how_it_works.png?ref_type=heads&inline=false)

# Getting started
## Requirements

To run Schedyt, ensure you have the following tools installed:

- **Docker**
- **Postman** (Optional for API testing)

## Running Schedyt Containers

Schedyt requires **MariaDB** and **RabbitMQ** to operate. Follow these steps to set up and run Schedyt:

1. Create a docker network
   ```sh 
   docker network create schedyt-network
   ```
2. Run mariaDB
   ```sh 
   docker run -d --network schedyt-network --hostname schedyt-db --name schedyt-db --env MYSQL_ROOT_PASSWORD=ro0tPas5wr.d --env MYSQL_DATABASE=schedyt_db mariadb:10.4.4
   ```
3. Run rabbitMQ
   ```sh 
   docker run -d --network schedyt-network --hostname schedyt-broker --name schedyt-broker -p 5672:5672 -p 15672:15672  --env RABBITMQ_DEFAULT_USER=schedyt --env RABBITMQ_DEFAULT_PASS=Pas5wr.d rabbitmq:3.12.4-management-alpine
   ```
4. Run schedyt
   ```sh
   docker run -d --network schedyt-network --name schedyt-app --env JAVA_OPTS=-Dspring.profiles.active=quick-start -p 8080:8080 4dauby/schedyt:latest
   ```
   Then schedyt is listening on port 8080
## Schedule an event
Now schedyt is running follow the steps below to schedule an event.

**To schedule for a short time (*example: One minute*) set the value of the field ```"triggerDate"```**

### Scheduling with postman
1. Download a postman collection [here](schedyt.postman_collection.json)
2. Import collection in Postman.
3. From collection imported in Postman:
   1. create an application with request ```Register Application [Public]```
   2. schedule an event with request ```Schedule Event  [Client]```
### Scheduling with curl
1. Create an application
   ```shell
    curl --location 'http://localhost:8080/v1/applications' \
    --header 'Content-Type: application/json' \
    --data '{
     "name":"foo",
     "description":"foo description",
     "exchangeName":"foo.direct",
     "secret": "myPass",
     "zoneId":"Asia/Abidjan"
    }'
   ```
2. Schedule an event
   Replace *application_id* by the application id you just created. It looks like ```"id": "9779548b335c470096c6bc6863348af9"``` in the response of the request above.

  ```sh
   curl --location 'http://localhost:8080/v1/applications/application_id/schedules' \
   --header 'Content-Type: application/json' \
   --data '{
    "eventName":"foo bar",
    "triggerDate":"",
    "triggerFirstAt":"",
    "zoneId":"Africa/Abidjan",
    "frequency":1,
    "duration":10,
    "event":{
        "content":"new event successfully scheduled"    
        },    
    "routingKey":"event"
   }'
  ```
3. View event
   display schedyt logs with this command:
```sh
docker logs schedyt-app
```
When the event is triggered on the set date, you'll see a line containing the following text:
```text
[QUICK START][INFO] Event received: {"content":"foo event triggered successfully"}
```
# Container user
Schedyt is not run as root. this container has a Non-Root User called *kageyama* and his directory is `/home/kageyama`
# Logging
Schedyt uses two log files to record activities:
- **`access.log`**: Logs all HTTP requests.
-  **`schedyt.log`**: Logs all application-related events and messages.

Both log files are stored in the directory: `/home/kageyama/logs`

***Example**: Mapping Logs to a Host Directory*
```sh
docker run -d --network schedyt-network --name schedyt-app --env JAVA_OPTS=-Dspring.profiles.active=quick-start \ 
-v /host/directory:/home/kageyama/logs \
-p 8080:8080 schedyt:latest 
```

> Ensure your host directory `/host/directory` has the correct permissions for Docker to write files:<br/>
> `sudo chmod -R 777 /host/directory`
# Deploy via docker compose

```yaml
services:

  schedyt-db:
    container_name: schedyt-db
    image: mariadb:10.4.4
    environment:
      MYSQL_ROOT_PASSWORD: Pas5wr.d
      MYSQL_USER: dbUser
      MYSQL_PASSWORD: Pas5wr.d
      MYSQL_DATABASE: schedyt_db
    ports:
      - "3307:3306"

  schedyt-broker:
    container_name: schedyt-broker
    image: rabbitmq:3.12.4-management-alpine
    environment:
      RABBITMQ_DEFAULT_USER: brokerUser
      RABBITMQ_DEFAULT_PASS: Pas5wr.d

    ports:
      - "5672:5672"
      - "15672:15672"


  schedyt-app:
    container_name: schedyt-app
    image: schedyt:latest

    restart: on-failure
    environment:
      JAVA_OPTS: -Dspring.profiles.active=prod-api-key
      DB_HOST: schedyt-db
      SCHEDY_DB_USER: dbUser
      SCHEDY_DB_PASSWORD: Pas5wr.d
      BROKER_HOST: schedyt-broker
      BROKER_USER: brokerUser
      BROKER_PASSWORD: Pas5wr.d
      BROKER_PORT: 5672

    ports:
      - "8080:8080"
    depends_on:
      - schedyt-db
      - schedyt-broker

networks:
  default:
    name: schedyt-net
```

# Environment variables

| VARIABLE            | DESCRIPTION                                               |
| ------------------- | --------------------------------------------------------- |
| DB_HOST             | Database host                                             |
| SCHEDYT_DB_USER     | Database username                                         |
| SCHEDYT_DB_PASSWORD | Database user password                                    |
| BROKER_HOST         | Message broker host                                       |
| BROKER_USER         | Message broker username                                   |
| BROKER_PASSWORD     | Message broker user password                              |
| BROKER_PORT         | Message broker port                                       |
| ADMIN_SECRET        | Application admin password. The default value is Pas5w.rd |

# Accessing the API Documentation
Once the application is deployed, the API documentation is available at:
* Production URL: http://my-domain/api-docs.html
* Local Environment: http://localhost:8080/api-docs.html
* [Download here](https://gitlab.com/adauby/schedyt/-/raw/main/api-docs.html?ref_type=heads&inline=false)

This documentation provides detailed information about all available APIs, including endpoints, request parameters, and response formats.

# Application Profiles
Schedyt supports multiple Spring profiles to cater to different use cases and environments. Each profile configures the application differently, enabling flexible deployment and feature activation.
##  Available Profiles
**1. quick-start**:

- **Purpose**: For quick setup and local testing.
- **Features**:
   - No security.
- **Usage:**
   - Ideal for development or demo purposes.

**2. prod-http-basic:**

* **Purpose**: Production environment with HTTP Basic Authentication.
* **Features**:
   * Secured with HTTP Basic Authentication.
   * Focused on simplicity for environments that require basic security.

**3. prod-api-key**:

* **Purpose**: Production environment with API key-based security.
* **Features**:
   * Secured with API keys.
   * Supports client-specific authentication.
   * Ideal for applications integrating with external services or APIs.
## How to Activate Profiles
Set JAVA_OPTS environment<br>
Example: Using HTTP Basic authentication in compose configuration file

```yml
...
  schedyt-app:
    container_name: schedyt-app
    image: 4dauby/schedyt:latest

    restart: on-failure
    environment:
      JAVA_OPTS: -Dspring.profiles.active=prod-api-key
...
```
# Roles in Schedyt

Schedyt defines two primary roles for applications:

**CLIENT**:

- Applications with the CLIENT role can schedule and manage their own events.

**ADMIN**:

- Applications with the ADMIN role have full access, including managing other applications and their events.
- When Schedyt starts for the first time with the profile `prod-api-key` or `prod-http-basic`, an application with the ADMIN role is automatically created.
- The default ADMIN application is named `schedyt` with the password `Pas5w.rd`.
- To change the default ADMIN password, set the environment variable `ADMIN_SECRET` to your desired password during deployment.


